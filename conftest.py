import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.firefox.options import Options as OptionsFirefox
from selenium.webdriver.chrome.service import Service


def pytest_addoption(parser):
    parser.addoption('--browser_name', action='store', default=None,
                     help="Choose browser: chrome or firefox or safari or yandex")
    parser.addoption('--language', action='store', default=None,
                     help="Choose language: ec or fr")


# pytest -v -s --tb=line --language=ru --browser_name=chrome test_main_page.py --alluredir test

@pytest.fixture(scope="class")  # function
def browser(request):
    options = Options()
    options.add_argument("user-agent=YandexDirect")
    options.add_argument("--disable-blink-features=AutomationControlled")
    options.add_argument("--headless=new")
    options.add_experimental_option('prefs', {'intl.accept_languages': 'user_language'})
    browser_name = request.config.getoption("browser_name")
    options_firefox = OptionsFirefox()
    options_firefox.add_argument("user-agent=YandexDirect")
    options_firefox.set_preference("intl.accept_languages", 'user_language')
    service = Service(r'/Users/denisprohorenko/Desktop/work/yandex/yandexdriver')
    browser = None
    if browser_name == "chrome":
        print("\nstart chrome browser for test..")
        browser = webdriver.Remote (command_executor = "http://selenium__standalone-chrome:4444/wd/hub", options = options)
        browser.maximize_window()

    elif browser_name == "firefox":
        print("\nstart firefox browser for test..")
        browser = webdriver.Firefox(options=options_firefox)
        browser.maximize_window()
    elif browser_name == 'yandex':
        print("\nstart chrome browser for test..")
        browser = webdriver.Chrome(service=service, options=options)
        browser.maximize_window()
    else:
        raise pytest.UsageError("--browser_name should be chrome or firefox")
    yield browser
    print("\nquit browser..")
    browser.quit()
