from .pages.main_page import *
# from .pages.main_page import CatalogPage
import pytest
import allure


@allure.epic('Главная страница')
class TestMainPage:
    @pytest.mark.city_tests
    class TestCityPopup:
        @allure.feature('Хедер')
        @allure.title('Поиск кнопки город')
        @pytest.mark.smoke
        def test_should_be_city_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_city_button()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Попап выбора города открывается кликом в кнопку город')
        @pytest.mark.smoke
        def test_popup_city_should_open(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_city_popup()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Фон страницы блюрится при открытии попапа город')
        @pytest.mark.regression
        def test_background_should_be_blurred_city(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.background_should_be_blurred_city()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В попапе есть заголовок')
        @pytest.mark.regression
        def test_should_be_title(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_city()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В попапе есть поле ввода')
        @pytest.mark.regression
        def test_should_be_input_field(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_input_field()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В поле ввода есть плейсхолдер')
        @pytest.mark.regression
        def test_should_be_placeholder(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.city_should_be_placeholder()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Плейс холдер уменьшается и перемещается при фокусе на поле')
        @pytest.mark.regression
        def test_laceholder_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.city_placeholder_focus()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Цвет рамки поля ввода меняется при фокусе')
        @pytest.mark.regression
        def test_frame_should_change_color(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.city_frame_should_change_color()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Плейсхолдер поля город, смещается в центр и увеличивается, при снятии фокуса с пустого поля')
        @pytest.mark.regression
        def test_city_placeholder_defocus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.city_placeholder_defocus()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Рамка поля ввода города, не подсвечивается при снятии фокуса с поля')
        @pytest.mark.regression
        def test_city_field_is_not_highlighted_when_focus_is_removed(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.city_field_is_not_highlighted_when_focus_is_removed()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В попапе есть список доступных городов')
        @pytest.mark.regression
        def test_there_is_a_list_of_available_cities(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_a_list_of_available_cities()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Список городов скроллится')
        @pytest.mark.regression
        def test_the_list_of_cities_scrolls(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_list_of_cities_scrolls()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Цвет текста с названием города, меняет цвет при фокусе')
        @pytest.mark.regression
        def test_text_color_inlist_of_cities_changes_on_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.text_color_inlist_of_cities_changes_on_focus()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Кликом в город из списка, попап закрывается, город меняется на выбранный')
        @pytest.mark.smoke
        def test_the_city_is_changed_by_clicking_on_any_city_from_the_list(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.the_city_is_changed_by_clicking_on_any_city_from_the_list()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Из поля можно удалить введенный текст')
        @pytest.mark.regression
        def test_text_can_be_removed_from_the_field(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_city_popup()
            page.text_can_be_removed_from_the_field()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В поле можно ввести текст')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['Уфа', 'Ufa', '1234', '!@#$'])  # pytest.param('Уфа', marks=pytest.mark.xfail)
        def test_can_enter_text_in_the_field(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.can_enter_text_in_the_field(text)

        # @allure.feature('Попапы')
        # @allure.story('Попап "Выбор города"')
        # @allure.title('Из поля можно удалить введенный текст')
        # @pytest.mark.regression
        # def test_text_can_be_removed_from_the_field(self, browser):
        #     link = 'https://stroylandiya.ru/'
        #     page = MainPage(browser, link)
        #     page.text_can_be_removed_from_the_field()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Должны появиться подсказки')
        @pytest.mark.parametrize('text',
                                 ['Уфа', 'Оренбург', 'Челябинск'])  # pytest.param('Уфа', marks=pytest.mark.xfail)
        @pytest.mark.regression
        def test_hints_with_cities_should_appear(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.hints_with_cities_should_appear(text)

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Подсказки должны быть релевантны')
        @pytest.mark.parametrize('text', ['Уф', 'Ор', 'Че', 'Ба'])  # pytest.param('Уфа', marks=pytest.mark.xfail)
        @pytest.mark.regression
        def test_hints_must_be_relevant(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.hints_must_be_relevant(text)

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Цвет текста подсказки должен меняться')
        @pytest.mark.regression
        def test_focus_on_tooltip_change_text_color(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.focus_on_tooltip_change_text_color()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Список подсказок скроллится')
        @pytest.mark.regression
        def test_the_hint_list_scrolls(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_hint_list_scrolls()

        # @allure.feature('Попапы')
        # @allure.story('Попап "Выбор города"')
        # @allure.title('Клик в любой город из подсказок, закрывает попап, город соответствует выбранному')
        # @pytest.mark.smoke
        # def test_when_click_on_the_hint_the_popup_closes_the_city_corresponds_to_the_selected_one(self, browser):
        #     link = 'https://stroylandiya.ru/'
        #     page = MainPage(browser, link)
        #     page.go_to_city_popup()
        #     page.when_click_on_the_hint_the_popup_closes_the_city_corresponds_to_the_selected_one()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('В попапе есть кнопка закрытия "Х"')
        @pytest.mark.regression
        def test_should_be_close_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_city_popup()
            page.should_be_close_button()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Попап закрывается кликом в кнопку "Х"')
        @pytest.mark.regression
        def test_popup_should_close_click_in_x(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.popup_should_close_click_in_x()

        @allure.feature('Попапы')
        @allure.story('Попап "Выбор города"')
        @allure.title('Попап закрывается кликом за пределами окна')
        @pytest.mark.smoke
        def test_popup_should_be_closed_by_clicking_outside_the_window(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            # page.go_to_city_popup()
            page.popup_should_be_closed_by_clicking_outside_the_window()

    @pytest.mark.shops_tests
    class TestShopsButton:
        @allure.feature('Хедер')
        @allure.title('Кнопка магазины')
        @pytest.mark.smoke
        def test_should_be_shops_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_shops_button()

        @allure.feature('Хедер')
        @allure.title('Клик в кнопку магазины ведет на страницу /shops')
        @pytest.mark.smoke
        def test_clicking_on_the_shops_button_leads_to_the_shops_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_the_shops_button_leads_to_the_shops_page()

    @pytest.mark.auth_tests
    class TestStatusAndAuth:
        @allure.feature('Хедер')
        @allure.title('На странице есть кнопка статус заказа')
        @pytest.mark.smoke
        def test_should_be_status_order_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_status_order_button()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Клик в кнопку статус заказа открывает попап авторизации')
        @pytest.mark.smoke
        def test_go_to_popur_authorization(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_popur_authorization()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В попапе авторизации есть заголовок')
        @pytest.mark.regression
        def test_should_be_title_auth(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Фон страницы блюрится при открытии попапа авторизации')
        @pytest.mark.regression
        def test_background_should_be_blurred_auth(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.background_should_be_blurred_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В попапе есть кнопка "получить код"')
        @pytest.mark.regression
        def test_should_be_button_get_code(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_button_get_code()

        # @allure.feature('Попапы')
        # @allure.story('Попап "Авторизация"')
        # @allure.title('Кнопка получить код не активна при незаполненных полях')
        # @pytest.mark.smoke
        # def test_button_get_code_is_disabled(self, browser):
        #     link = 'https://stroylandiya.ru/'
        #     page = MainPage(browser, link)
        #     page.button_get_code_is_disabled()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В попапе есть кнопка войти по Email')
        @pytest.mark.smoke
        def test_should_be_button_login_by_email(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_button_login_by_email()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('"Войти по Email" активна')
        @pytest.mark.regression
        def test_button_login_by_email_is_enabled(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_login_by_email_is_enabled()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('в попапе есть кнопка закрыть "Х"')
        @pytest.mark.regression
        def test_should_be_close_button_auth(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_close_button_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Попап закрывается кликом в кнопку "Х"')
        @pytest.mark.regression
        def test_popup_should_close_click_in_x_auth(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.popup_should_close_click_in_x_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Попап закрывается кликом за пределами окна')
        @pytest.mark.regression
        def test_popup_auth_should_be_closed_by_clicking_outside_the_window(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.popup_auth_should_be_closed_by_clicking_outside_the_window()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('в попапе есть поле ввода номера телефона')
        @pytest.mark.regression
        def test_should_be_a_field_for_entering_phone_number(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_popur_authorization()
            page.should_be_a_field_for_entering_phone_number()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В поле ввода номера телефона есть плейсхолдер')
        @pytest.mark.regression
        def test_auth_should_be_placeholder(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_should_be_placeholder()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Плейсхолдер меняет размер и положение при фокусе')
        @pytest.mark.regression
        def test_auth_placeholder_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_placeholder_focus()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Рамка поля ввода номера телефона, меняет цвет при фокусе')
        @pytest.mark.regression
        def test_auth_frame_should_change_color(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.auth_frame_should_change_color()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Текст плейсхолдера увеличивается и смещается в центр поля, при снятии фокуса с поля')
        @pytest.mark.regression
        def test_auth_placeholder_defocus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_placeholder_defocus()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Рамка поля ввода номера телефона, не подсвечивается при снятии фокуса')
        @pytest.mark.regression
        def test_auth_field_is_not_highlighted_when_focus_is_removed(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_field_is_not_highlighted_when_focus_is_removed()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('При вводе любого символа, автоматически подставляется +7')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['7', '8', '+', 'f', 'ф'])
        def test_when_enter_any_number_plus_seven_is_automatically_substituted(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.when_enter_any_number_plus_seven_is_automatically_substituted(text)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Второй символ после + меняется на 7')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['8', pytest.param('ф', marks=pytest.mark.xfail),
                                          pytest.param('f', marks=pytest.mark.xfail)])
        def test_second_character_after_the_plus_is_changed_to_seven(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.second_character_after_the_plus_is_changed_to_seven(text)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('12 символов позитив')
        @pytest.mark.regression
        @pytest.mark.parametrize('num', ['1234567890',
                                         # '12345678901',
                                         # '+71234567890',
                                         # '81234567890',
                                         # '71234567890',
                                         # '7(123)4567890',
                                         # '+7(123)4567890',
                                         # '7(123)456-78-90',
                                         # '+7(123)456-78-90',
                                         # '7123456-78-90',
                                         # '+7123456-78-90',
                                         # '7123 456 78 90',
                                         '+7123 456 78 90'])
        def test_the_field_accepts_exactly_12_characters_only_numbers_positive(self, browser, num):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.the_field_accepts_exactly_12_characters_only_numbers_positive(num)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('12 символов негатив')
        @pytest.mark.regression
        @pytest.mark.parametrize('num', ['7123456789',
                                         # '7(123)456789',
                                         # '7123 456 78 9',
                                         # '7123456-78-9',
                                         # '7(123)456-78-9',
                                         # 'кукуруза',
                                         # 'corn',
                                         # '!@#$%^&*()',
                                         pytest.param('712345678901', marks=pytest.mark.xfail)])
        def test_the_field_accepts_exactly_12_characters_only_numbers_negative(self, browser, num):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.the_field_accepts_exactly_12_characters_only_numbers_negative(num)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('в попапе есть блок яндекс капчи')
        @pytest.mark.regression
        @pytest.mark.xfail
        def test_auth_should_be_yandex_captcha(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.auth_should_be_yandex_captcha()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В блоке капчи есть чек-бокс')
        @pytest.mark.regression
        @pytest.mark.xfail
        def test_auth_should_be_captcha_checkbox(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_should_be_captcha_checkbox()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Чек бокс можно отметить')
        @pytest.mark.regression
        @pytest.mark.xfail
        def test_auth_can_click_on_the_captcha_checkbox(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_can_click_on_the_captcha_checkbox()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('С чекбокса нельзя снять отметку')
        @pytest.mark.regression
        @pytest.mark.xfail
        def test_auth_can_not_uncheck_the_checkbox(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.auth_can_not_uncheck_the_checkbox()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Кнопка получить код активна')
        @pytest.mark.regression
        def test_button_get_code_is_enabled(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_get_code_is_enabled()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('При повторном открытии попапа, чекбокс не отмечен')
        @pytest.mark.regression
        @pytest.mark.xfail
        def test_when_reopening_the_popup_the_checkbox_is_not_checked(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.popup_should_close_click_in_x_auth()
            page.go_to_popur_authorization()
            page.when_reopening_the_popup_the_checkbox_is_not_checked()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Появляется ошибка при клике получить код, при незаполненном номере')
        @pytest.mark.regression
        def test_when_click_get_code_without_phone_number_an_error_appears(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.when_click_get_code_without_phone_number_an_error_appears()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Открывается окно ввода кода')
        @pytest.mark.regression
        def test_when_click_get_the_code_popup_opens_enter_the_code(self, browser, num=9080000909):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.when_click_get_the_code_popup_opens_enter_the_code(num)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В окне ввода кода есть заголовок')
        @pytest.mark.regression
        def test_should_be_title_enter_code(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_enter_code()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Есть поле ввода кода')
        @pytest.mark.regression
        def test_should_be_code_entry_field(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_code_entry_field()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Есть таймер на повторный запрос смс')
        @pytest.mark.regression
        def test_should_be_timer(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_timer()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Есть ранее введенный номер телефона')
        @pytest.mark.regression
        def test_should_be_phone_number_auth(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.close_gift()
            page.should_be_phone_number_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Есть кнопка войти по Email')
        @pytest.mark.regression
        def test_should_be_button_email_login(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.close_gift()
            page.should_be_button_email_login()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Поле код принимает ровно 4 символа')
        @pytest.mark.regression
        @pytest.mark.parametrize('code', ['123',
                                          '12345'])
        def test_code_entry_field_accepts_four_characters(self, browser, code):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.code_entry_field_accepts_four_characters(code)

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Возврат к вводу номера кликом в кнопку "Изменить"')
        @pytest.mark.regression
        def test_clicking_on_the_change_button_returns_to_entering_the_number(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_the_change_button_returns_to_entering_the_number()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Кликом в кнопку "Войти по Email" открывается попап с полем Email')
        @pytest.mark.regression
        def test_clicking_on_the_login_by_email_button_opens_an_popup_with_an_email_input_field(self, browser,
                                                                                                num=9080000909):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.when_click_get_the_code_popup_opens_enter_the_code(num)
            page.clicking_on_the_login_by_email_button_opens_an_popup_with_an_email_input_field()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('У нового пользователя есть чекбокс согласия на обработку')
        @pytest.mark.regression
        def test_new_user_has_the_consent_checkbox(self, browser, num=9999999909):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_popur_authorization()
            page.when_click_get_the_code_popup_opens_enter_the_code(num)
            page.new_user_has_the_consent_checkbox()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Клик в кнопку "Согласие" открывает соглашение на обработку данных')
        @pytest.mark.regression
        def test_clicking_on_the_consent_opens_the_data_processing_agreement(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_the_consent_opens_the_data_processing_agreement()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Должна быть кнопка "Назад"')
        @pytest.mark.regression
        def test_should_be_back_link(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_back_link()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Должна быть кнопка "Закрыть"')
        @pytest.mark.regression
        def test_should_be_close_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_close_button_auth()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Клик в кнопку "Назад" возвращает в попап ввода кода')
        @pytest.mark.regression
        def test_back_button_returns_to_the_code_entry_popup(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_returns_to_the_code_entry_popup()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Клик в "Политика" открывает политику обработки данных')
        @pytest.mark.regression
        def test_click_in_policy_opens_the_data_processing_policy(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_in_policy_opens_the_data_processing_policy()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В окне политики обработки данных есть кнопка назад')
        @pytest.mark.regression
        def test_policy_should_be_back_link(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.policy_should_be_back_link()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Клик в кнопку назад возвращает в попап ввода кода')
        @pytest.mark.regression
        def test_policy_back_button_returns_to_the_code_entry_popup(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.policy_back_button_returns_to_the_code_entry_popup()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('В попапе есть кнопка "Зарегистрироваться"')
        @pytest.mark.regression
        def test_should_be_registration_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_registration_button()

        @allure.feature('Попапы')
        @allure.story('Попап "Авторизация"')
        @allure.title('Ошибка при клике в чекбокс с пустым полем ввода кода')
        @pytest.mark.regression
        def test_error_when_clicking_in_the_checkbox_without_code(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.error_when_clicking_in_the_checkbox_without_code()

        # !!!!!!!!!!!!!!!!ПРОДОЛЖЕНИЕ АВТОРИЗАЦИИ!!!!!!!!!!!
    @pytest.mark.phone_tests
    class TestPhoneNumber:
        @allure.feature('Хедер')
        @allure.title('В хедере есть номер телефона')
        @pytest.mark.smoke
        def test_should_be_phone_number(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_phone_number()

    @pytest.mark.logo_tests
    class TestLogo:
        @allure.feature('Хедер')
        @allure.title('В хедере есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_logo()

        @allure.feature('Хедер')
        @allure.title('Клик в логотип открывает главную страницу')
        @pytest.mark.smoke
        def test_click_on_the_logo_to_open_the_home_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_on_the_logo_to_open_the_home_page(link)

    @pytest.mark.catalog_button_tests
    class TestCatalogButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка каталог')
        @allure.title('На странице есть кнопка "Каталог"')
        @pytest.mark.smoke
        def test_should_be_catalog_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_catalog_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка каталог')
        @allure.title('Клик в кнопку "Каталог" открывает окно каталога')
        @pytest.mark.smoke
        def test_when_you_click_on_the_catalog_button_a_menu_with_catalog_sections_opens(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_catalog_popup()

        @allure.feature('Хедер')
        @allure.story('Кнопка каталог')
        @allure.title('Клик в кнопку "Каталог" меняет иконку бургера на крестик')
        @pytest.mark.regression
        def test_burger_changes_to_a_cross(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.burger_changes_to_a_cross()

        @allure.feature('Хедер')
        @allure.story('Кнопка каталог')
        @allure.title('Повторный клик в кнопку "Каталог" закрывает окно каталога')
        @pytest.mark.regression
        def test_clicking_the_catalog_button_again_closes_the_catalog_window(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_catalog_button_again_closes_the_catalog_window()

        @allure.feature('Хедер')
        @allure.story('Кнопка каталог')
        @allure.title('Кнопка каталог подсвечивается при фокусе на нее')
        @pytest.mark.regression
        def test_catalog_button_is_highlighted_when_focused(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.catalog_button_is_highlighted_when_focused()

    @pytest.mark.search_input_tests
    class TestSearch:

        @allure.feature('Хедер')
        @allure.title('На странице есть поле поиска')
        @pytest.mark.smoke
        def test_should_be_search_field(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_search_field()

        @allure.feature('Хедер')
        @allure.title('Рамка и иконка поиска подсвечиваются при фокусе на поле')
        @pytest.mark.regression
        def test_search_frame_is_highlighted_when_focused(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.search_frame_is_highlighted_when_focused()

        @allure.feature('Хедер')
        @allure.title('В поле поиска должен быть плейсхолдер "Найти дрель"')
        @pytest.mark.regression
        def test_should_be_placeholder(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_placeholder()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('При клике в поле открывается попап поиска')
        @pytest.mark.smoke
        def test_go_to_search_popup(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_search_popup()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('В окне поиска есть блоки "Часто ищут" и "Популярные товары"(холодный пользователь)')
        @pytest.mark.regression
        def test_there_are_sections_in_popup_often_searched_and_popular_products(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_are_sections_in_popup_often_searched_and_popular_products()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('В блоке "Популярные товары" есть товары')
        @pytest.mark.regression
        def test_must_be_product(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.must_be_product()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('У товаров есть название')
        @pytest.mark.regression
        def test_should_be_item_title(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_item_title()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('У товаров есть цена')
        @pytest.mark.regression
        def test_should_be_item_price(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_item_price()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('У товаров есть фото')
        @pytest.mark.regression
        def test_should_be_photo(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_photo()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('У товаров есть кнопка добавления в корзину')
        @pytest.mark.regression
        def test_should_be_add_to_cart_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_add_to_cart_button()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Кнопка добавления в корзину подсвечивается при фокусе')
        @pytest.mark.regression
        def test_button_changes_color_on_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_changes_color_on_focus()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Кнопка добавления в корзину меняется на "-","+" и счетчик товара')
        @pytest.mark.regression
        def test_click_the_add_to_cart_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.click_the_add_to_cart_button()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Отображение счетчика товаров в корзине(в хедере)')
        @pytest.mark.regression
        def test_counter_on_cart_icon(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.counter_on_cart_icon()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Название и цена товара в корзине, соответствуют данным из попапа поиска')
        @pytest.mark.regression
        def test_added_product_is_displayed_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.added_product_is_displayed_on_the_shopping_cart_page()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('При повторном открытии поиска, добавленный в корзину товар, сохранил кнопки "+" "-" и счетчик')
        @pytest.mark.regression
        def test_quantity_change_buttons_not_closing(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.quantity_change_buttons_not_closing()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "+" добавляет 1 товар')
        @pytest.mark.regression
        def test_click_on_the_plus_button_adds_one_item(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_on_the_plus_button_adds_one_item()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "-" отнимает 1 товар')
        @pytest.mark.regression
        def test_clicking_the_minus_button_takes_away_one_product(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_takes_away_one_product()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "-" закрывает кнопки изменения количества товара')
        @pytest.mark.regression
        def test_clicking_the_minus_button_removes_the_product(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_removes_the_product()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Удаленный товар в блоке "популярные товары" удаляется со страницы корзина')
        @pytest.mark.regression
        def test_product_is_not_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.product_is_not_on_the_shopping_cart_page()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Кнопки + и - не закрываются при добавлении второго наименования товара в корзину')
        @pytest.mark.regression
        def test_plus_and_minus_buttons_do_not_disappear_when_adding_new_product_to_cart(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.plus_and_minus_buttons_do_not_disappear_when_adding_new_product_to_cart()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в товар в разделе популярные товары, открывает релевантную карточку товара')
        @pytest.mark.regression
        def test_clicking_on_product_in_popular_products_section_leads_to_product_card(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_product_in_popular_products_section_leads_to_product_card()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Количество добавленных в поиске товаров в корзину, равно количеству в карточке товара')
        @pytest.mark.regression
        def test_quantity_of_added_product_in_product_card_corresponds_to_quantity_in_search(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.quantity_of_added_product_in_product_card_corresponds_to_quantity_in_search()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('У теплого пользователя есть история поиска')
        @pytest.mark.regression
        def test_should_be_previously_you_searched(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_previously_you_searched()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в запрос истории поиска открывает релевантную поисковую выдачу')
        @pytest.mark.regression
        def test_click_to_search_history(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_to_search_history()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в в пункт раздела "часто ищут" открывает релевантную поисковую выдачу')
        @pytest.mark.regression
        def test_click_to_frequently_searched_item(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.click_to_frequently_searched_item()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Цвет текста истории поиска, меняется при фокусе на него')
        @pytest.mark.regression
        def test_search_history_item_changes_color_when_focused(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.search_history_item_changes_color_when_focused()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Все пункты истории поиска имеют кнопку удалить "Х"')
        @pytest.mark.regression
        def test_each_search_history_item_has_a_delete_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.each_search_history_item_has_a_delete_button()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "Х" удаляет запрос из истории')
        @pytest.mark.regression
        def test_clicking_on_the_delete_button_removes_the_query_from_the_search_history(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_the_delete_button_removes_the_query_from_the_search_history()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Блок "Ранее вы искали" пропадает при удалении всех запросов')
        @pytest.mark.regression
        def test_if_delete_all_requests_the_section_is_not_displayed_in_popup(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.if_delete_all_requests_the_section_is_not_displayed_in_popup()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('В поле поиска можно ввести текст')
        @pytest.mark.smoke
        @pytest.mark.parametrize('text', ['ламинат', 'laminate', 'ЛАМИНАТ', '123', '!@#', 'ванна акриловая'])
        def test_can_enter_text_in_the_search_field(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.can_enter_text_in_the_search_field(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Выдача в блоке "Часто ищут" релевантна запросу')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['ламинат', 'плитка'])
        def test_often_searches_change_with_search_query(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.often_searches_change_with_search_query(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Выдача в блоке "Популярные товары" релевантна запросу')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['Ламинат', 'Плитка'])
        def test_popular_products_change_with_search_query(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.popular_products_change_with_search_query(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('При вводе запроса в поиск, добавляется блок "Категории"')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['Ламинат', 'Плитка'])
        def test_category_block_appears(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.category_block_appears(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Выдача в блоке "Категории релевантна запросу')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['Ламинат', 'Плитка'])
        def test_output_in_category_block_is_relevant_to_query(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.output_in_category_block_is_relevant_to_query(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('При вводе запроса в поисковую строку, появляются кнопки "Удалить" и "Найти"')
        @pytest.mark.regression
        def test_close_and_find_buttons_appear_in_the_search_bar(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.close_and_find_buttons_appear_in_the_search_bar()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Кнопка "Найти" подсвечивается при фокусе на нее')
        @pytest.mark.regression
        def test_find_button_is_highlighted_when_focused(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.find_button_is_highlighted_when_focused()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Поле поиска очищается кликом в кнопку "Х"')
        @pytest.mark.regression
        def test_field_is_cleared_by_clicking_on_x_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.field_is_cleared_by_clicking_on_x_button()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "Найти", открывает релевантную запросу выдачу')
        @pytest.mark.smoke
        @pytest.mark.parametrize('text', ['Ламинат', 'Плитка'])
        def test_clicking_on_find_button_opens_search_results(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.clicking_on_find_button_opens_search_results(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в кнопку "Все результаты", открывает релевантную запросу выдачу')
        @pytest.mark.regression
        def test_clicking_on_button_all_results_opens_search_results(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.clicking_on_button_all_results_opens_search_results()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Клик в любую категорию открывает результаты поиска')
        @pytest.mark.regression
        def test_clicking_on_any_category_opens_search_results(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.clicking_on_any_category_opens_search_results()

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Поиск работает при запросе на неверной раскладке')
        @pytest.mark.regression
        @pytest.mark.parametrize('text', ['Kfvbyfn', 'kfvbyfn'])
        def test_processed_requests_with_a_typo(self, browser, text):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.processed_requests_with_a_typo(text)

        @allure.feature('Попапы')
        @allure.story('Попап поиска')
        @allure.title('Попап поиска закрывается кликом за его пределами')
        @pytest.mark.smoke
        def test_popup_is_closed_by_clicking_outside_it(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_search_popup()
            page.popup_is_closed_by_clicking_outside_it()

    @pytest.mark.actions_button_tests
    class TestActionsButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка "Акции"')
        @allure.title('На странице есть кнопка "Акции"')
        @pytest.mark.smoke
        def test_should_be_actions_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_actions_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка "Акции"')
        @allure.title('Клик в кнопку "Акции" открывает страницу "/actions/"')
        @pytest.mark.smoke
        def test_click_in_button_actions_open_actions_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_in_button_actions_open_actions_page()

    @pytest.mark.enter_button_tests
    class TestEnterButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка "Войти"')
        @allure.title('В хедере есть кнопка "Войти"')
        @pytest.mark.smoke
        def test_should_be_enter_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_enter_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка "Войти"')
        @allure.title('Клик в кнопку "Войти" открывает попап авторизации')
        @pytest.mark.smoke
        def test_click_in_button_enter_open_auth_popup(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_in_button_enter_open_auth_popup()

    @pytest.mark.compare_button_tests
    class TestCompareButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка "Сравнение"')
        @allure.title('В хедере есть кнопка "Сравнение"')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_compare_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка "Сравнение"')
        @allure.title('Клик в кнопку сравнение открывает страницу "Сравнение товаров"')
        @pytest.mark.smoke
        def test_clicking_on_button_opens_the_comparison_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_button_compare_opens_the_comparison_page()

    @pytest.mark.favourites_button_tests
    class TestFavouritesButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка "Избранное"')
        @allure.title('На странице есть кнопка "Избранное"')
        @pytest.mark.smoke
        def test_should_be_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_favourites_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка "Избранное"')
        @allure.title('Клик в кнопку избранное открывает страницу "Отложенные товары"')
        @pytest.mark.smoke
        def test_clicking_on_button_favourites_opens_page_waiting_products(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_button_favourites_opens_page_waiting_products()

    @pytest.mark.basket_button_tests
    class TestBasketButton:

        @allure.feature('Хедер')
        @allure.story('Кнопка "Корзина"')
        @allure.title('В хедере есть кнопка "Корзина"')
        @pytest.mark.smoke
        def test_should_be_basket_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_basket_button()

        @allure.feature('Хедер')
        @allure.story('Кнопка "Корзина"')
        @allure.title('Клик в кнопку "Корзина" открывает страницу "Корзина"')
        @pytest.mark.smoke
        def test_clicking_on_button_basket_opens_basket_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_button_basket_opens_basket_page()

    @pytest.mark.header_banner_tests
    class TestHeaderBanner:

        @allure.feature('Хедер')
        @allure.story('Баннер над хедером')
        @allure.title('Над хедером есть баннер')
        @pytest.mark.smoke
        def test_should_be_header_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_header_banner()

        @allure.feature('Хедер')
        @allure.story('Баннер над хедером')
        @allure.title('Клик в баннер над хедером открывает страниц акции из баннера')
        @pytest.mark.smoke
        def test_click_in_header_banner_open_action_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_in_header_banner_open_action_page()

    @pytest.mark.main_banner_tests
    class TestMainBanner:

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('На странице есть главный баннер')
        @pytest.mark.smoke
        def test_should_be_main_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_main_banner()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('У баннера есть блок управления слайдами')
        @pytest.mark.smoke
        def test_should_be_main_banner_navigation(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_main_banner_navigation()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('В блоке управления слайдами есть кнопка переключения слайда назад')
        @pytest.mark.smoke
        def test_should_be_prev_button_navigation(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_prev_button_navigation()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('В блоке управления слайдами есть кнопка переключения слайда вперед')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_next_button_navigation()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('В блоке управления слайдами отображается номер текущего слайда')
        @pytest.mark.smoke
        def test_should_be_current_value_main_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_current_value_main_banner()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('В блоке управления слайдами отображается общее количество слайдов')
        @pytest.mark.smoke
        def test_should_be_total_value_main_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_total_value_main_banner()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('Слайды можно переключить вперед')
        @pytest.mark.smoke
        def test_slides_can_be_switched_forward(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_can_be_switched_forward()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('Слайды можно переключить назад')
        @pytest.mark.smoke
        def test_slides_can_be_switched_back(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_can_be_switched_back()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('Количество слайдов в счетчике равно количеству слайдов')
        @pytest.mark.smoke
        def test_number_of_slides_is_equal_to_number_in_counter(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.number_of_slides_is_equal_to_number_in_counter()

        @allure.feature('Тело')
        @allure.story('Главный баннер(верхний)')
        @allure.title('Слайды должны быть кликабельны')
        @pytest.mark.smoke
        def test_slides_in_the_main_banner_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_in_the_main_banner_are_clickable()

    @pytest.mark.items_banner_tests
    class TestItemsBanner:

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Должен быть блок "Товары дня"')
        @pytest.mark.smoke
        def test_should_be_goods_of_the_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_goods_of_the_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('В блоке есть слайды с товарами')
        @pytest.mark.smoke
        def test_should_be_item_of_day_slides(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_item_of_day_slides()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У блока есть кнопка переключения к следующему слайду')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_item_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_next_button_navigation_item_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('На первом слайде не отображается кнопка переключения слайдов к предыдущему')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Слайды переключаются кликом в кнопку "следующий"')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_next(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_next()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Кнопка переключения слайдов к предыдущему появляется на втором слайде')
        @pytest.mark.smoke
        def test_back_button_appears_on_the_second_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_appears_on_the_second_slide()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Слайды переключаются кликом в кнопку "предыдущий"')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_prev(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_prev()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('При возврате к первому слайду, кнопка переключения назад, пропадает')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('На последнем слайде, нет кнопки переключения к следующему')
        @pytest.mark.regression
        def test_next_button_missing(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_missing()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Кнопка переключения слайдов к следующему появляется')
        @pytest.mark.regression
        def test_next_button_appears(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_appears()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Слайды кликабельны')
        @pytest.mark.smoke
        def test_slides_products_of_the_day_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_products_of_the_day_are_clickable()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('В блоке есть заголовок')
        @pytest.mark.regression
        def test_should_be_block_title(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_block_title()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть таймер окончания акции')
        @pytest.mark.regression
        def test_should_be_items_timer(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_items_timer()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть шильдик акции/скидки')
        @pytest.mark.regression
        def test_should_be_a_badge(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_a_badge()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть фотография')
        @pytest.mark.smoke
        def test_should_be_photo_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_photo_items()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть название товара')
        @pytest.mark.smoke
        def test_should_be_name_item(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_name_item()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть цена товара')
        @pytest.mark.smoke
        def test_should_be_price(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_price()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('У слайдов есть кнопка добавления в корзину')
        @pytest.mark.smoke
        def test_should_be_add_cart_btn(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_add_cart_btn()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Кнопка добавить в корзину подсвечивается при фокусе')
        @pytest.mark.regression
        def test_button_add_cart_lights_up_when_focused(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_add_cart_lights_up_when_focused()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Появляются кнопки управления количеством товара, после клика в кнопку добавления в корзину')
        @pytest.mark.smoke
        def test_click_the_add_to_cart_button_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.click_the_add_to_cart_button_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('В иконке корзины в хедере появился счетчик товаров')
        @pytest.mark.smoke
        def test_counter_on_cart_icon_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.counter_on_cart_icon_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Название и цена товара в корзине, соответствуют данным из слайда')
        @pytest.mark.smoke
        def test_added_product_is_displayed_on_the_shopping_cart_page_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.added_product_is_displayed_on_the_shopping_cart_page_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Кнопки "+" и "-" не закрываются при обновлении страницы')
        @pytest.mark.smoke
        def test_quantity_change_buttons_not_closing_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.quantity_change_buttons_not_closing_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Клик в кнопку + добавляет 1ед товара в счетчик')
        @pytest.mark.smoke
        def test_click_on_the_plus_button_adds_one_item_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_on_the_plus_button_adds_one_item_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Клик в кнопку "-" уменьшает количество товара в счетчике на 1')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_takes_away_one_product_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_takes_away_one_product_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Цена и название товара в карточке товара, релевантны слайду товары дня')
        @pytest.mark.smoke
        def test_clicking_on_product_in_items_of_day_section_leads_to_product_card(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_product_in_items_of_day_section_leads_to_product_card()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Количество добавленного в корзину товара, соответствует количеству в карточке товара')
        @pytest.mark.smoke
        def test_quantity_of_added_product_in_product_card_corresponds_to_quantity_in_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.quantity_of_added_product_in_product_card_corresponds_to_quantity_in_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Клик в кнопку "-" закрывает кнопки управления количеством товара')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_removes_the_product_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_removes_the_product_items_of_day()

        @allure.feature('Тело')
        @allure.story('Товары дня')
        @allure.title('Товар удалился со страницы корзины')
        @pytest.mark.smoke
        def test_product_is_not_on_the_shopping_cart_page_items_of_day(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.product_is_not_on_the_shopping_cart_page_items_of_day()

    @pytest.mark.popular_categories_tests
    class TestPopularCategories:

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Есть блок популярные категории')
        @pytest.mark.smoke
        def test_should_be_popular_categories(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_popular_categories()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('В блоке есть слайды')
        @pytest.mark.smoke
        def test_should_be_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('У всех слайдов есть картина и заголовок')
        @pytest.mark.smoke
        def test_should_be_title_and_img_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_and_img_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Заголовки слайдов меняют цвет при фокусе')
        @pytest.mark.smoke
        def test_slide_titles_change_color_on_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slide_titles_change_color_on_focus()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('В блоке есть кнопка переключения слайдов к следующему')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_next_button_navigation_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('На первом экране слайдов, нет кнопки переключения слайдов назад')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Слайды листаются кликом в кнопку к следующим')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_next_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_next_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('На втором экране слайдов, появляется кнопка переключения слайдов к предыдущим')
        @pytest.mark.smoke
        def test_back_button_appears_on_the_second_slide_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_appears_on_the_second_slide_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Слайды переключаются на первый экран')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_prev_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_prev_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Кнопка переключения слайдов к следующим, пропадает на последнем экране')
        @pytest.mark.smoke
        def test_next_button_missing_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_missing_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Кнопка переключения слайдов к следующим, появляется на первом экране')
        @pytest.mark.smoke
        def test_next_button_appears_popular_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_appears_popular_items()

        @allure.feature('Тело')
        @allure.story('Популярные категории')
        @allure.title('Слайды популярные категории кликабельны')
        @pytest.mark.smoke
        def test_slides_popular_categories_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_popular_categories_are_clickable()

    @pytest.mark.hits_sales_tests
    class TestHitsSales:

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('На странице есть блок "Хиты продаж"')
        @pytest.mark.smoke
        def test_should_be_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('В блоке "Хиты продаж" должны быть плитки товаров')
        @pytest.mark.smoke
        def test_should_be_hits_sales_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_hits_sales_items()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть фотографии')
        @pytest.mark.smoke
        def test_should_be_photo_items_sales_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_photo_items_sales_items()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть название')
        @pytest.mark.smoke
        def test_should_be_name_item_sales_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_name_item_sales_items()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть цена')
        @pytest.mark.smoke
        def test_should_be_price_sales_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_price_sales_items()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть кнопка добавить в корзину')
        @pytest.mark.smoke
        def test_should_be_add_cart_btn_sales_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_add_cart_btn_sales_items()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть кнопка добавить в избранное')
        @pytest.mark.smoke
        def test_should_be_add_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_add_favourites_button()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть оценка/текст "Нет оценки"')
        @pytest.mark.smoke
        def test_should_be_reviews_rating(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_reviews_rating()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('У товаров есть блок комментариев')
        @pytest.mark.smoke
        def test_should_be_comments_block(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_comments_block()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Иконка отзывы кликабельна, открывает таб отзывы в карточке товара')
        @pytest.mark.smoke
        def test_comment_button_is_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.comment_button_is_clickable()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Плитки товаров кликабельны')
        @pytest.mark.smoke
        def test_slides_hits_sales_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.slides_hits_sales_are_clickable()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Есть кнопка переключения слайдов вперед')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_next_button_navigation_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('На первом экране слайдов нет кнопки переключения назад')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Слайды переключаются кликом в кнопку переключения вперед')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_next_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_next_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('На втором экране слайдов появляется кнопка переключения назад')
        @pytest.mark.smoke
        def test_back_button_appears_on_the_second_slide_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_appears_on_the_second_slide_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Слайды переключаются кликом в кнопку назад')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_prev_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_prev_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('На последнем экране слайдов, кнопка вперед пропадает')
        @pytest.mark.smoke
        def test_next_button_missing_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_missing_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Кнопка вперед появляется при переходе с последнего экрана слайдов назад')
        @pytest.mark.smoke
        def test_next_button_appears_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_appears_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Кнопка добавления в корзину подсвечивается при фокусе')
        @pytest.mark.regression
        def test_button_add_cart_lights_up_when_focused_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_add_cart_lights_up_when_focused_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Появляются кнопки управления количеством товара, после клика в кнопку добавления в корзину')
        @pytest.mark.smoke
        def test_click_the_add_to_cart_button_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_the_add_to_cart_button_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('В иконке корзины в хедере появился счетчик')
        @pytest.mark.smoke
        def test_counter_on_cart_icon_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.counter_on_cart_icon_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Название и цена товара в корзине, соответствуют данным из слайда')
        @pytest.mark.smoke
        def test_added_product_is_displayed_on_the_shopping_cart_page_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.added_product_is_displayed_on_the_shopping_cart_page_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Кнопки управления количеством не закрываются при обновлении страницы')
        @pytest.mark.smoke
        def test_quantity_change_buttons_not_closing_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.quantity_change_buttons_not_closing_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Клик в кнопку "+" добавляет в счетчик 1ед.')
        @pytest.mark.smoke
        def test_click_on_the_plus_button_adds_one_item_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_on_the_plus_button_adds_one_item_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Клик в кнопку "-" уменьшает счетчик товара 1ед.')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_takes_away_one_product_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_takes_away_one_product_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Цена и название товара в карточке товара, релевантны плитке хиты продаж')
        @pytest.mark.smoke
        def test_clicking_on_product_in_hits_sales_section_leads_to_product_card(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_on_product_in_hits_sales_section_leads_to_product_card()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Количество добавленного в корзину товара, соответствует количеству в карточке товара')
        @pytest.mark.smoke
        def test_quantity_of_added_product_in_product_card_corresponds_to_quantity_in_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.quantity_of_added_product_in_product_card_corresponds_to_quantity_in_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Клик в кнопку "-" закрывает кнопки управления количеством товара')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_removes_the_product_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_minus_button_removes_the_product_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Товар удалился со страницы корзины')
        @pytest.mark.smoke
        def test_product_is_not_on_the_shopping_cart_page_hits_sales(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.product_is_not_on_the_shopping_cart_page_hits_sales()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Фото в плитках переключаются движением курсора')
        @pytest.mark.smoke
        @pytest.mark.xfail
        def test_photos_in_tiles_switch(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.photos_in_tiles_switch()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('В плитках где фотографий больше 1, есть точки переключения фотографий')
        @pytest.mark.smoke
        @pytest.mark.xfail
        def test_should_be_photo_switching_points(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_photo_switching_points()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Количество точек равно количеству фото')
        @pytest.mark.smoke
        @pytest.mark.xfail
        def test_number_dots_is_equal_to_number_of_photos(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.number_dots_is_equal_to_number_of_photos()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Должна быть кнопка "Смотреть все"')
        @pytest.mark.smoke
        def test_should_be_hits_all_btn(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_hits_all_btn()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Текст кнопки подсвечивается при фокусе на него')
        @pytest.mark.smoke
        def test_button_text_is_highlighted_on_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.button_text_is_highlighted_on_focus()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Клик в кнопку "Смотреть все" открывает страницу "Хиты"')
        @pytest.mark.smoke
        def test_click_to_open_hits_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.click_to_open_hits_page()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Счетчик товаров в избранном, отображается')
        @pytest.mark.smoke
        def test_should_be_added_to_favorites(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_added_to_favorites()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Товар отображается на странице "Отложенные товары"')
        @pytest.mark.smoke
        def test_should_be_on_the_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_on_the_wishlist_page()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Кнопка добавления в избранное, выбрана после обновления сраницы')
        @pytest.mark.smoke
        def test_not_deleted_after_page_refresh(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.not_deleted_after_page_refresh()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Счетчик товаров в избранном, пропадает')
        @pytest.mark.smoke
        def test_should_be_removed_from_favorites(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_removed_from_favorites()

        @allure.feature('Тело')
        @allure.story('Хиты продаж')
        @allure.title('Товар не удален со страницы "Отложенные товары"')
        @pytest.mark.smoke
        def test_should_be_removed_from_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_removed_from_wishlist_page()

    @pytest.mark.popular_brands
    class TestPopularBrands:

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('На странице есть блок "Популярные бренды"')
        @pytest.mark.smoke
        def test_should_be_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('В блоке есть слайды брендов')
        @pytest.mark.smoke
        def test_should_be_popular_brand_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_popular_brand_items()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('У брендов есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_logo_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('У брендов есть название')
        @pytest.mark.smoke
        def test_should_be_title_items_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_items_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Цвет текста меняется при фокусе на плитку бренда')
        @pytest.mark.smoke
        def test_text_color_changes_on_focus(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.text_color_changes_on_focus()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Есть кнопка все бренды')
        @pytest.mark.smoke
        def test_should_be_all_brands_btn(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_all_brands_btn()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Открывается страница бренды, кликом в кнопку "Все бренды"')
        @pytest.mark.smoke
        def test_brand_page_opens(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.brand_page_opens()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Страница бренда релевантна слайду')
        @pytest.mark.smoke
        def test_brand_page_is_relevant_to_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.brand_page_is_relevant_to_slide()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Есть кнопка переключения слайдов')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_next_button_navigation_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('На первом экране слайдов нет кнопки переключения слайдов назад')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Слайды переключаются вперед')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_next_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_next_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('На втором экране слайдов, появилась внопка переключения назад')
        @pytest.mark.smoke
        def test_back_button_appears_on_the_second_slide_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_appears_on_the_second_slide_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Слайды переключаются назад')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_prev_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_prev_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Кнопка переключения слайдов вперед, пропадает на последнем экране')
        @pytest.mark.smoke
        def test_next_button_missing_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_missing_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Кнопка переключения слайдов вперед, появляется')
        @pytest.mark.smoke
        def test_next_button_appears_popular_brand(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_appears_popular_brand()

        @allure.feature('Тело')
        @allure.story('Популярные бренды')
        @allure.title('Плитки брендов кликабельны')
        @pytest.mark.smoke
        def test_slides_popular_brads_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_popular_brads_are_clickable()

    @pytest.mark.lower_banner
    class TestLowerBanner:

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Должен быть нижний баннер')
        @pytest.mark.smoke
        def test_should_be_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Должен быть блок управления слайдами')
        @pytest.mark.smoke
        def test_should_be_lower_banner_navigation(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_lower_banner_navigation()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Должна быть кнопка переключения слайдов назад')
        @pytest.mark.smoke
        def test_should_be_prev_button_navigation_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_prev_button_navigation_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Должна быть кнопка переключения слайдов вперед')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_next_button_navigation_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Есть номер текущего слайда')
        @pytest.mark.smoke
        def test_should_be_current_value_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_current_value_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Есть общее количество слайдов')
        @pytest.mark.smoke
        def test_should_be_total_value_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_total_value_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Слайд можно переключить к следующему')
        @pytest.mark.smoke
        def test_slides_can_be_switched_forward_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_can_be_switched_forward_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Слайд можно переключить к предыдущему')
        @pytest.mark.smoke
        def test_slides_can_be_switched_back_lower_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_can_be_switched_back_lower_banner()

        @allure.feature('Тело')
        @allure.story('Нижний баннер')
        @allure.title('Слайды кликабельны')
        @pytest.mark.smoke
        def test_slides_in_the_lower_banner_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.slides_in_the_lower_banner_are_clickable()

    @pytest.mark.expert_advices
    class TestExpertAdvices:

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('На странице есть блок "Советы эксперта"')
        @pytest.mark.smoke
        def test_should_be_block_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_block_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('В блоке "Советы эксперта" есть слайды')
        @pytest.mark.smoke
        def test_should_be_expert_advices_items(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_expert_advices_items()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('У слайдов есть фотографии')
        @pytest.mark.smoke
        def test_should_be_photo_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_photo_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('У слайдов есть категория')
        @pytest.mark.smoke
        def test_should_be_category_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_category_slide()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('У слайдов есть заголовок')
        @pytest.mark.smoke
        def test_should_be_title_slide(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_title_slide()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('Есть кнопка "Смотреть все"')
        @pytest.mark.smoke
        def test_should_be_all_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_all_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('открывается страница "блог"')
        @pytest.mark.smoke
        def test_blog_page_opens(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.blog_page_opens()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('Слайды в блоке кликабельны, страницы релевантны')
        @pytest.mark.smoke
        def test_slides_expert_advices_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.slides_expert_advices_are_clickable()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('В блоке есть кнопка переключения слайдов к следующим')
        @pytest.mark.smoke
        def test_should_be_next_button_navigation_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_next_button_navigation_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('На первом экране слайдов, нет кнопки переключения назад')
        @pytest.mark.smoke
        def test_there_is_no_back_button_on_the_first_slide_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.there_is_no_back_button_on_the_first_slide_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('Слайды переключаются вперед')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_next_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_next_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('На втором экране слайдов, появляется кнопка переключения назад')
        @pytest.mark.smoke
        def test_back_button_appears_on_the_second_slide_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.back_button_appears_on_the_second_slide_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('слайды переключаются назад')
        @pytest.mark.smoke
        def test_the_slide_switches_to_the_prev_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.the_slide_switches_to_the_prev_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('На последнем экране, кнопка переключения вперед, пропадает')
        @pytest.mark.smoke
        def test_next_button_missing_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_missing_expert_advices()

        @allure.feature('Тело')
        @allure.story('Советы эксперта')
        @allure.title('Кнопка переключения слайдов появляется')
        @pytest.mark.smoke
        def test_next_button_appears_expert_advices(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.next_button_appears_expert_advices()


@allure.epic('Каталог')
@pytest.mark.catalog_tests
class TestCatalog:

    @pytest.mark.catalog_popup
    class TestCatalogPopup:
        @allure.feature('Попап каталога')
        @allure.title('В левой части попапа есть блок с категориями 1го уровня')
        @pytest.mark.smoke
        def test_on_the_left_side_of_the_first_level_category(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_catalog_popup()
            catalog_page = CatalogPage(browser, link)
            catalog_page.on_the_left_side_of_the_first_level_category()

        @allure.feature('Попап каталога')
        @allure.title('В центральной части попапа есть категории 2го уровня')
        @pytest.mark.smoke
        def test_should_be_second_level_categories(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.should_be_second_level_categories()

        @allure.feature('Попап каталога')
        @allure.title('В центральной части попапа есть категории 3го уровня')
        @pytest.mark.smoke
        def test_should_be_third_level_categories(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.should_be_third_level_categories()

        @allure.feature('Попап каталога')
        @allure.title('При открытии каталога выбрана первая категория')
        @pytest.mark.smoke
        def test_when_opening_window_top_category_is_selected(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.when_opening_window_top_category_is_selected()

        @allure.feature('Попап каталога')
        @allure.title('Категории выбираются наведением курсора')
        @pytest.mark.smoke
        def test_categories_are_selected_by_hovering_over(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.categories_are_selected_by_hovering_over()

        @allure.feature('Попап каталога')
        @allure.title('Каталог закрывается повторным кликом в кнопку "Каталог"')
        @pytest.mark.smoke
        def test_clicking_the_catalog_button_again_closes_the_catalog_window(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.clicking_the_catalog_button_again_closes_the_catalog_window()

        @allure.feature('Попап каталога')
        @allure.title('Клик в каталог первого уровня открывает страницу каталога первого уровня')
        @pytest.mark.smoke
        def test_click_in_category_of_first_lvl_opens_the_directory_of_first_lvl(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.go_to_catalog_popup()
            page_catalog = CatalogPage(browser, link)
            page_catalog.go_to_directory_of_first_lvl()

        @allure.feature('Попап каталога')
        @allure.title('Клик в каталог второго уровня открывает страницу каталога второго уровня')
        @pytest.mark.smoke
        def test_click_in_category_of_second_lvl_opens_the_directory_of_second_lvl(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_catalog_popup()
            catalog_page = CatalogPage(browser, link)
            catalog_page.go_to_directory_of_second_lvl()

        @allure.feature('Попап каталога')
        @allure.title('Клик в каталог третьего уровня открывает листинг товаров')
        @pytest.mark.smoke
        def test_go_to_directory_of_listing_products(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_catalog_popup()
            catalog_page = CatalogPage(browser, link)
            catalog_page.go_to_directory_of_listing_products()

    @pytest.mark.catalog_first_lvl
    class TestFirstLvlCatalog:

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('Поиск кнопки город')
        @pytest.mark.smoke
        def test_should_be_city_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.open()
            page.should_be_city_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('Кнопка магазины')
        @pytest.mark.smoke
        def test_should_be_shops_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_shops_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка статус заказа')
        @pytest.mark.smoke
        def test_should_be_status_order_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_status_order_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть номер телефона')
        @pytest.mark.smoke
        def test_should_be_phone_number(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_phone_number()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_logo()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Каталог"')
        @pytest.mark.smoke
        def test_should_be_catalog_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_catalog_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть поле поиска')
        @pytest.mark.smoke
        def test_should_be_search_field(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_search_field()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Войти"')
        @pytest.mark.smoke
        def test_should_be_enter_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_enter_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Сравнение"')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_compare_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Избранное"')
        @pytest.mark.smoke
        def test_should_be_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_favourites_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Корзина"')
        @pytest.mark.smoke
        def test_should_be_basket_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_basket_button()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Хедер')
        @allure.title('Над хедером есть баннер')
        @pytest.mark.smoke
        def test_should_be_header_banner(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_header_banner()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('На странице есть заголовок')
        @pytest.mark.smoke
        def test_should_be_catalog_title(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_catalog_title()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('На странице есть категории второго уровня')
        @pytest.mark.smoke
        def test_should_be_second_lvl_categories(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_second_lvl_categories()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('На странице есть категории третьего уровня')
        @pytest.mark.smoke
        def test_should_be_third_lvl_categories(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_third_lvl_categories()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('У всех категорий есть фотография')
        @pytest.mark.smoke
        def test_should_be_category_photo(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_category_photo()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('В группе отображается не больше 8 категорий')
        @pytest.mark.smoke
        def test_no_more_than_eight_categories(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.no_more_than_eight_categories()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('Есть кнопка "Показать все"')
        @pytest.mark.smoke
        def test_should_be_show_more_link(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_show_more_link()

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('Клик в кнопку "Показать все" открывает релевантный каталог 2 уровня')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num', [1, 2, 3])
        def test_clicking_on_show_all_opens_second_lvl_directory(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.clicking_on_show_all_opens_second_lvl_directory(num)

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('Клик в заголовок группы, открывает релевантный каталог 2 уровня')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num', [1, 2, 3])
        def test_opens_second_lvl_directory(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.opens_second_lvl_directory(num)

        @allure.feature('Страница каталога первого уровня')
        @allure.story('Тело')
        @allure.title('Клик в категорию 3 уровня, открывает релевантный листинг')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num', [1, 2, 3])
        def test_opens_third_lvl_directory(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.opens_third_lvl_directory(num)

    @pytest.mark.catalog_second_lvl
    class TestSecondLvlCatalog:

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('Поиск кнопки город')
        @pytest.mark.smoke
        def test_should_be_city_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.open()
            catalog_page = CatalogPage(browser, link)
            catalog_page.go_to_second_lvl_catalog()
            page.should_be_city_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('Кнопка магазины')
        @pytest.mark.smoke
        def test_should_be_shops_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_shops_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка статус заказа')
        @pytest.mark.smoke
        def test_should_be_status_order_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_status_order_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть номер телефона')
        @pytest.mark.smoke
        def test_should_be_phone_number(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_phone_number()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_logo()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Каталог"')
        @pytest.mark.smoke
        def test_should_be_catalog_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_catalog_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть поле поиска')
        @pytest.mark.smoke
        def test_should_be_search_field(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_search_field()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Войти"')
        @pytest.mark.smoke
        def test_should_be_enter_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_enter_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Сравнение"')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_compare_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Избранное"')
        @pytest.mark.smoke
        def test_should_be_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_favourites_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Корзина"')
        @pytest.mark.smoke
        def test_should_be_basket_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_basket_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Хедер')
        @allure.title('Над хедером есть баннер')
        @pytest.mark.smoke
        def test_should_be_header_banner(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_header_banner()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('На странице есть заголовок')
        @pytest.mark.smoke
        def test_should_be_listing_title(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_second_lvl_catalog_title()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('На странице есть кнопка назад')
        @pytest.mark.smoke
        def test_should_be_back_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_back_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Клик в кнопку назад, открывает страницу каталога')
        @pytest.mark.smoke
        def test_returns_to_catalog_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.returns_to_catalog_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должен быть блок с категориями')
        @pytest.mark.smoke
        def test_should_be_categories_block(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            catalog_page = CatalogPage(browser, link)
            catalog_page.go_to_second_lvl_catalog()
            page = CatalogPage(browser, link)
            page.should_be_categories_block()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должны быть плитки с категориями')
        @pytest.mark.smoke
        def test_should_be_categories(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_categories()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Может быть рекламный баннер')
        @pytest.mark.smoke
        @pytest.mark.xfail
        def test_could_be_a_banner_ad(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.could_be_a_banner_ad()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должны быть плитки товаров')
        @pytest.mark.smoke
        def test_should_be_items(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_items()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должны быть фотографии товаров')
        @pytest.mark.smoke
        def test_should_be_photo_items(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_photo_items()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должны быть названия товаров')
        @pytest.mark.smoke
        def test_should_be_title_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_title_item()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должны быть цены товаров')
        @pytest.mark.smoke
        def test_should_be_price_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_price_item()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должна быть кнопка добавления в корзину')
        @pytest.mark.smoke
        def test_should_be_add_cart_btn(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_add_cart_btn()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должна быть кнопка добавления в избранное')
        @pytest.mark.smoke
        def test_should_be_add_favourites_btn(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_add_favourites_btn()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должен быть блок оценки товара')
        @pytest.mark.smoke
        def test_should_be_reviews_rating_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_reviews_rating_item()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Должен быть блок отзывов')
        @pytest.mark.smoke
        def test_should_be_comments_block(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_comments_block()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Плитка товара кликабельна')
        @pytest.mark.smoke
        def test_tiles_are_clickable_and_relevant(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.tiles_are_clickable_and_relevant()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Фотографии в плитке переключаются')
        @pytest.mark.smoke
        def test_photos_in_tiles_switch(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.photos_in_tiles_switch()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('У плиток есть точки индикаторы фотографий')
        @pytest.mark.smoke
        def test_should_be_photo_switching_points(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_photo_switching_points()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Количество точек равно количеству фотографий')
        @pytest.mark.smoke
        def test_number_dots_is_equal_to_number_of_photos(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.number_dots_is_equal_to_number_of_photos()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('В листинге есть блок сортировки')
        @pytest.mark.smoke
        def test_should_be_sorted(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_sorted()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Список сортировки открывается кликом в поле')
        @pytest.mark.smoke
        def test_sort_list_expands(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_list_expands()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Сравнение вариантов сортировки')
        @pytest.mark.smoke
        def test_list_items_sorted(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_items_sorted()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Список сортировки сворачивается выбором значения')
        @pytest.mark.smoke
        def test_list_is_collapsed_by_selecting_a_value(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_is_collapsed_by_selecting_a_value()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Список сортировок сворачивается, кликом за пределами списка')
        @pytest.mark.smoke
        def test_list_is_collapsing(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_is_collapsing()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Сортировка по цене от сначала дешевые')
        @pytest.mark.smoke
        def test_sort_first_cheap(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.sort_first_cheap()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Сортировка по цене сначала дорогие')
        @pytest.mark.smoke
        def test_sort_first_expensive(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_first_expensive()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Сортировка по размеру скидки')
        @pytest.mark.smoke
        def test_sort_first_discount_size(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_first_discount_size()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопка добавления в корзину подсвечивается при фокусе')
        @pytest.mark.smoke
        def test_button_add_cart_lights_up_when_focused(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.button_add_cart_lights_up_when_focused()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопка добавления в корзину кликабельна')
        @pytest.mark.smoke
        def test_click_the_add_to_cart_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.click_the_add_to_cart_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Появляется счетчик товаров в иконке корзины')
        @pytest.mark.smoke
        def test_counter_on_cart_icon(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.counter_on_cart_icon()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Добавленный в корзину товар, отображается на странице корзины')
        @pytest.mark.smoke
        def test_added_product_is_displayed_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.added_product_is_displayed_on_the_shopping_cart_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопки "+" и "-" не закрываются при обновлении страницы')
        @pytest.mark.smoke
        def test_quantity_change_buttons_not_closing(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.quantity_change_buttons_not_closing()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кликом в кнопку "+" добавляет 1 товар в корзину')
        @pytest.mark.smoke
        def test_click_on_the_plus_button_adds_one_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.click_on_the_plus_button_adds_one_item()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кликом в кнопку "-" удаляет 1 товар из корзины')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_takes_away_one_product(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_the_minus_button_takes_away_one_product()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кликом в плитку, открывается релевантная карточка товара')
        @pytest.mark.smoke
        def test_clicking_on_product_in_slider_section_leads_to_product_card(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_on_product_in_slider_section_leads_to_product_card()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Количество добавленного товара в карточке товара соответствует количеству товара в плитке')
        @pytest.mark.smoke
        def test_quantity_of_added_product_in_product_card_corresponds_to_quantity_in_slider(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.quantity_of_added_product_in_product_card_corresponds_to_quantity_in_slider()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Повторный клик в кнопку "-" скрывает кнопки "+" и "-"')
        @pytest.mark.smoke
        def test_clicking_the_minus_button_removes_the_product(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_the_minus_button_removes_the_product()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар не отображается на странице корзины')
        @pytest.mark.smoke
        def test_product_is_not_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_not_on_the_shopping_cart_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар добавляется в избранное')
        @pytest.mark.xfail
        @pytest.mark.smoke
        def test_should_be_added_to_favorites(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_added_to_favorites()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар отображается на странице избранное')
        @pytest.mark.smoke
        def test_should_be_on_the_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_on_the_wishlist_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар не удаляется из избранного при обновлении страницы')
        @pytest.mark.xfail
        @pytest.mark.smoke
        def test_not_deleted_after_page_refresh(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.not_deleted_after_page_refresh()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар удаляется из избранного')
        @pytest.mark.smoke
        def test_should_be_removed_from_favorites(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_removed_from_favorites()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар не отображается на странице избранное')
        @pytest.mark.smoke
        def test_should_be_removed_from_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_removed_from_wishlist_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопка "К сравнению" есть в плитках товаров')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_compare_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопка "К сравнению" кликабельна, появляется счетчик товаров')
        @pytest.mark.smoke
        def test_click_on_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.click_on_compare_button()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Добавленный товар к сравнению, отображается на странице сравнение')
        @pytest.mark.smoke
        def test_product_is_displayed_on_the_comparison_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_displayed_on_the_comparison_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар удаляется из сравнения, кнопка серая, счетчик закрылся')
        @pytest.mark.smoke
        def test_product_is_removed_from_comparison(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_removed_from_comparison()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Товар удаляется со страницы сравнение')
        @pytest.mark.smoke
        def test_product_is_not_displayed_on_the_comparison_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_not_displayed_on_the_comparison_page()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('При фокусе на плитку товара, отображаются все характеристики')
        @pytest.mark.smoke
        def test_should_be_window_with_characteristics(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_window_with_characteristics()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Отображается не более 10 характеристик')
        @pytest.mark.smoke
        def test_maximum_10_characteristics(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.maximum_10_characteristics()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Характеристики релевантны характеристикам из карточки товара')
        @pytest.mark.smoke
        def test_characteristics_are_relevant_to_the_product_card(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.characteristics_are_relevant_to_the_product_card()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кликом в плитку, открывается релевантный листинг')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num', [1, 2, 3])
        def test_relevant_listing_opens(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.relevant_listing_opens(num)

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('В фильтрах отображаются все категории')
        @pytest.mark.smoke
        def test_should_be_categories_links(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.should_be_categories_links()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кликом в категорию открывается релевантный листинг')
        @pytest.mark.smoke
        def test_click_on_category_link(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.click_on_category_link()

        # @allure.feature('Страница каталога второго уровня')
        # @allure.story('Тело')
        # @allure.title('Для каждой категории отображается не больше 5 подкатегорий')
        # @pytest.mark.smoke
        # def test_no_more_than_five_subcategories_are_displayed(self, browser):
        #     link = 'https://stroylandiya.ru/catalog/'
        #     page = CatalogPage(browser, link)
        #     page.open()
        #     page.go_to_second_lvl_catalog()
        #     page.no_more_than_five_subcategories_are_displayed()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Кнопка "Смотреть все" есть только если подкатегорий больше 5')
        @pytest.mark.smoke
        def test_maybe_show_everything(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_second_lvl_catalog()
            page.maybe_show_everything()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Список подкатегорий разворачивается')
        @pytest.mark.smoke
        def test_expands_the_list_of_subcategories(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.expands_the_list_of_subcategories()

        @allure.feature('Страница каталога второго уровня')
        @allure.story('Тело')
        @allure.title('Список подкатегорий сворачивается')
        @pytest.mark.smoke
        def test_the_list_is_collapsing(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.the_list_is_collapsing()

    @pytest.mark.catalog_listing
    class TestListing:

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Поиск кнопки город')
        @pytest.mark.smoke
        def test_should_be_city_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.open()
            catalog_page = CatalogPage(browser, link)
            catalog_page.go_to_listing()
            page.should_be_city_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Кнопка магазины')
        @pytest.mark.smoke
        def test_should_be_shops_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_shops_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка статус заказа')
        @pytest.mark.smoke
        def test_should_be_status_order_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_status_order_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В хедере есть номер телефона')
        @pytest.mark.smoke
        def test_should_be_phone_number(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_phone_number()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В хедере есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_logo()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Каталог"')
        @pytest.mark.smoke
        def test_should_be_catalog_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_catalog_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть поле поиска')
        @pytest.mark.smoke
        def test_should_be_search_field(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_search_field()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Войти"')
        @pytest.mark.smoke
        def test_should_be_enter_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_enter_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Сравнение"')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_compare_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Избранное"')
        @pytest.mark.smoke
        def test_should_be_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_favourites_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Корзина"')
        @pytest.mark.smoke
        def test_should_be_basket_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_basket_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Над хедером есть баннер')
        @pytest.mark.smoke
        def test_should_be_header_banner(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = MainPage(browser, link)
            page.should_be_header_banner()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть заголовок')
        @pytest.mark.smoke
        def test_should_be_listing_title(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_listing_title()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('На странице есть хлебные крошки')
        @pytest.mark.smoke
        def test_should_be_bread_crumb(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_bread_crumb()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Хлебные крошки кликабельны')
        @pytest.mark.smoke
        def test_breadcrumbs_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.breadcrumbs_are_clickable()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('В листинге есть общее количество товаров')
        @pytest.mark.smoke
        def test_should_be_quantity(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_quantity()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Должна быть кнопка назад, с текстом выбранной категории')
        @pytest.mark.smoke
        def test_should_be_back_btn(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_back_btn()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Кнопка назад кликабельна')
        @pytest.mark.smoke
        def test_back_button_clickable(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.back_button_clickable()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Должна быть кнопка выбранной категории')
        @pytest.mark.smoke
        def test_should_be_category_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_category_button()

        @allure.feature('Листинг')
        @allure.story('Хедер')
        @allure.title('Кнопка категории кликабельна')
        @pytest.mark.smoke
        def test_category_button_is_clickable(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.category_button_is_clickable()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('На странице есть фильтры')
        @pytest.mark.smoke
        def test_should_be_filters(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_filters()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('По умолчанию, все фильтры отключены')
        @pytest.mark.smoke
        def test_filters_not_included(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.filters_not_included()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Название фильтра выделено жирным шрифтом')
        @pytest.mark.smoke
        def test_text_highlighted_bold(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.text_highlighted_bold()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Фильтр цена открыт по умолчанию')
        @pytest.mark.smoke
        def test_filter_price_open(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.filter_price_open()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Фильтр можно свернуть')
        @pytest.mark.smoke
        def test_you_can_close_the_filter(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.you_can_close_the_filter()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Фильтр можно развернуть')
        @pytest.mark.smoke
        def test_you_can_open_the_filter(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.you_can_open_the_filter()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должен быть toggle_switch "В наличии"')
        @pytest.mark.smoke
        def test_should_be_toggle_switch_available(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_toggle_switch_available()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должен быть toggle_switch "По акции"')
        @pytest.mark.smoke
        def test_should_be_toggle_switch_discounted(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_toggle_switch_discounted()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Все фильтры можно развернуть')
        @pytest.mark.smoke
        def test_all_filters_expand_and_collapse(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.all_filters_expand_and_collapse()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Все фильтры можно свернуть')
        @pytest.mark.smoke
        def test_all_filters_expand_and_collapse(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.all_filters_expand_and_collapse()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Есть фильтры "Диапазоны"')
        @pytest.mark.smoke
        def test_should_be_ranges(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_ranges()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Есть фильтры "Чекбоксы"')
        @pytest.mark.smoke
        def test_should_be_checkboxes(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_checkboxes()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('У диапазона есть поле "От"')
        @pytest.mark.smoke
        def test_should_be_input_min(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_input_min()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('У диапазона есть поле "До"')
        @pytest.mark.smoke
        def test_should_be_input_max(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_input_max()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В поле "От" предзаполнено минимальное значение')
        @pytest.mark.smoke
        def test_the_field_must_have_a_min_value(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.the_field_must_have_a_min_value()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В поле "До" предзаполнено минимальное значение')
        @pytest.mark.smoke
        def test_the_field_must_have_a_max_value(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.the_field_must_have_a_max_value()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Поле "От" кликабельно')
        @pytest.mark.smoke
        def test_field_from_clickable(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.field_from_clickable()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Поле "До" кликабельно')
        @pytest.mark.smoke
        def test_field_to_clickable(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.field_to_clickable()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В поле от можно ввести данные')
        @pytest.mark.smoke
        def test_data_can_be_entered_in_the_fields_min(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.data_can_be_entered_in_the_fields_min()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В поле до можно ввести данные')
        @pytest.mark.smoke
        def test_data_can_be_entered_in_the_fields_max(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.data_can_be_entered_in_the_fields_max()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Поле "От" принимает только целые числа')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num',
                                  [pytest.param('Роман', marks=pytest.mark.xfail),
                                  pytest.param('Roman', marks=pytest.mark.xfail),
                                  pytest.param('!@#$', marks=pytest.mark.xfail),
                                  pytest.param('12.34', marks=pytest.mark.xfail)])
        def test_field_min_accept_only_integers(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.field_min_accept_only_integers(num)

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Поле "До" принимает только целые числа')
        @pytest.mark.smoke
        @pytest.mark.parametrize('num',
                                  [pytest.param('Роман', marks=pytest.mark.xfail),
                                  pytest.param('Roman', marks=pytest.mark.xfail),
                                  pytest.param('!@#$', marks=pytest.mark.xfail),
                                  pytest.param('12.34', marks=pytest.mark.xfail)])
        def test_field_max_accept_only_integers(self, browser, num):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.field_max_accept_only_integers(num)

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('У диапазона есть точки минимум и максимум')
        @pytest.mark.smoke
        def test_there_is_a_slider_with_dots(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.there_is_a_slider_with_dots()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Точка минимум смещается при заполнении поля "От"')
        @pytest.mark.smoke
        def test_dot_min_shift_when_field_filled(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.dot_min_shift_when_field_filled()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Точка максимум смещается при заполнении поля "До"')
        @pytest.mark.smoke
        def test_dot_max_shift_when_field_filled(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.dot_max_shift_when_field_filled()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('При включении фильтра, появляется облако тегов')
        @pytest.mark.smoke
        def test_should_be_teg_cloud(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_teg_cloud()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Появляется облако "Очистить все"')
        @pytest.mark.smoke
        def test_should_be_teg_remove_all(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_teg_remove_all()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В облаке фильтра, есть кнопка закрытия')
        @pytest.mark.smoke
        def test_should_be_close_cloud_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_close_cloud_button()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Облако фильтра закрывается, фильтр отключается')
        @pytest.mark.smoke
        def test_close_cloud(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.close_cloud()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Можно выбрать чек-бокс')
        @pytest.mark.smoke
        def test_checkboxes_work(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.checkboxes_work()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Появляется облако "Очистить все" при выборе чек-бокса')
        @pytest.mark.smoke
        def test_there_is_a_tag_close_all(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.there_is_a_tag_close_all()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В облаке фильтра, есть кнопка закрытия')
        @pytest.mark.smoke
        def test_should_be_close_cloud_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_close_cloud_button()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Чекбокс отключается закрытием облака фильтра')
        @pytest.mark.smoke
        def test_close_cloud_checkbox(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.close_cloud_checkbox()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Фильтр отключается кликом в выбранный чекбокс')
        @pytest.mark.smoke
        def test_checkbox_can_be_disabled(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.checkboxes_work()
            page.checkbox_can_be_disabled()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('В листинге есть блок сортировки')
        @pytest.mark.asd
        def test_should_be_sorted(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_sorted()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Список сортировки открывается кликом в поле')
        @pytest.mark.asd
        def test_sort_list_expands(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_list_expands()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Сравнение вариантов сортировки')
        @pytest.mark.asd
        def test_list_items_sorted(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_items_sorted()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Список сортировки сворачивается выбором значения')
        @pytest.mark.asd
        def test_list_is_collapsed_by_selecting_a_value(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_is_collapsed_by_selecting_a_value()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Список сортировок сворачивается, кликом за пределами списка')
        @pytest.mark.asd
        def test_list_is_collapsing(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.list_is_collapsing()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Сортировка по цене сначала дешевые')
        @pytest.mark.asd
        def test_sort_first_cheap(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.sort_first_cheap()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Сортировка по цене сначала дорогие')
        @pytest.mark.asd
        def test_sort_first_expensive(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_first_expensive()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Сортировка по размеру скидки')
        @pytest.mark.asd
        def test_sort_first_discount_size(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.sort_first_discount_size()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должны быть плитки товаров')
        @pytest.mark.asd
        def test_should_be_items(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_items()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должны быть фотографии товаров')
        @pytest.mark.asd
        def test_should_be_photo_items(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_photo_items()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должны быть названия товаров')
        @pytest.mark.asd
        def test_should_be_title_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_title_item()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должны быть цены товаров')
        @pytest.mark.asd
        def test_should_be_price_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_price_item()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должна быть кнопка добавления в корзину')
        @pytest.mark.asd
        def test_should_be_add_cart_btn(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_add_cart_btn()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должна быть кнопка добавления в избранное')
        @pytest.mark.asd
        def test_should_be_add_favourites_btn(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_add_favourites_btn()


        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должен быть блок оценки товара')
        @pytest.mark.asd
        def test_should_be_reviews_rating_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_reviews_rating_item()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Должен быть блок комментариев')
        @pytest.mark.asd
        def test_should_be_comments_block(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_comments_block()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Плитка товара кликабельна')
        @pytest.mark.asd1
        def test_tiles_are_clickable_and_relevant(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.tiles_are_clickable_and_relevant()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Фотографии в плитке переключаются')
        @pytest.mark.asd1
        def test_photos_in_tiles_switch(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.photos_in_tiles_switch_listing()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('У плиток есть точки индикаторы фотографий')
        @pytest.mark.asd1
        def test_should_be_photo_switching_points(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_photo_switching_points()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Количество точек равно количеству фотографий')
        @pytest.mark.asd1
        def test_number_dots_is_equal_to_number_of_photos(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.number_dots_is_equal_to_number_of_photos_listing()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кнопка добавления в корзину подсвечивается при фокусе')
        @pytest.mark.asd1
        def test_button_add_cart_lights_up_when_focused(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.button_add_cart_lights_up_when_focused()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кнопка добавления в корзину кликабельна')
        @pytest.mark.asd1
        def test_click_the_add_to_cart_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.click_the_add_to_cart_button()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Появляется счетчик товаров в иконке корзины')
        @pytest.mark.smoke
        def test_counter_on_cart_icon(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.counter_on_cart_icon()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Добавленный в корзину товар, отображается на странице корзины')
        @pytest.mark.asd1
        def test_added_product_is_displayed_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.added_product_is_displayed_on_the_shopping_cart_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кнопки "+" и "-" не закрываются при обновлении страницы')
        @pytest.mark.asd1
        def test_quantity_change_buttons_not_closing(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.quantity_change_buttons_not_closing()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кликом в кнопку "+" добавляет 1 товар в корзину')
        @pytest.mark.asd1
        def test_click_on_the_plus_button_adds_one_item(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.click_on_the_plus_button_adds_one_item()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кликом в кнопку "-" удаляет 1 товар из корзины')
        @pytest.mark.asd1
        def test_clicking_the_minus_button_takes_away_one_product(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_the_minus_button_takes_away_one_product()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кликом в плитку, открывается релевантная карточка товара')
        @pytest.mark.asd1
        def test_clicking_on_product_in_slider_section_leads_to_product_card(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_on_product_in_slider_section_leads_to_product_card()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Количество добавленного товара в карточке товара соответствует количеству товара в плитке')
        @pytest.mark.asd1
        def test_quantity_of_added_product_in_product_card_corresponds_to_quantity_in_slider(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.quantity_of_added_product_in_product_card_corresponds_to_quantity_in_slider()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Повторный клик в кнопку "-" скрывает кнопки "+" и "-"')
        @pytest.mark.asd1
        def test_clicking_the_minus_button_removes_the_product(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.clicking_the_minus_button_removes_the_product()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар не отображается на странице корзины')
        @pytest.mark.asd1
        def test_product_is_not_on_the_shopping_cart_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_not_on_the_shopping_cart_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар добавляется в избранное')
        @pytest.mark.xfail
        @pytest.mark.asd1
        def test_should_be_added_to_favorites(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_added_to_favorites()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар отображается на странице избранное')
        @pytest.mark.asd1
        def test_should_be_on_the_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_on_the_wishlist_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар не удаляется из избранного при обновлении страницы')
        @pytest.mark.xfail
        @pytest.mark.asd1
        def test_not_deleted_after_page_refresh(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.not_deleted_after_page_refresh()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар удаляется из избранного')
        @pytest.mark.asd1
        def test_should_be_removed_from_favorites(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_removed_from_favorites()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар не отображается на странице избранное')
        @pytest.mark.smoke
        def test_should_be_removed_from_wishlist_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.should_be_removed_from_wishlist_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кнопка "К сравнению" есть в плитках товаров')
        @pytest.mark.asd1
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_compare_button()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Кнопка "К сравнению" кликабельна, появляется счетчик товаров')
        @pytest.mark.asd1
        @pytest.mark.xfail
        def test_click_on_compare_button(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.click_on_compare_button()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Добавленный товар к сравнению, отображается на странице сравнение')
        @pytest.mark.asd1
        def test_product_is_displayed_on_the_comparison_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_displayed_on_the_comparison_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар удаляется из сравнения, кнопка серая, счетчик закрылся')
        @pytest.mark.asd1
        def test_product_is_removed_from_comparison(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_removed_from_comparison()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Товар удаляется со страницы сравнение')
        @pytest.mark.asd1
        def test_product_is_not_displayed_on_the_comparison_page(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.product_is_not_displayed_on_the_comparison_page()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('При фокусе на плитку товара, отображаются все характеристики')
        @pytest.mark.asd1
        def test_should_be_window_with_characteristics(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.open()
            page.go_to_listing()
            page.should_be_window_with_characteristics()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Отображается не более 10 характеристик')
        @pytest.mark.asd1
        def test_maximum_10_characteristics(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.maximum_10_characteristics()

        @allure.feature('Листинг')
        @allure.story('Тело')
        @allure.title('Характеристики релевантны характеристикам из карточки товара')
        @pytest.mark.asd1
        def test_characteristics_are_relevant_to_the_product_card(self, browser):
            link = 'https://stroylandiya.ru/catalog/'
            page = CatalogPage(browser, link)
            page.characteristics_are_relevant_to_the_product_card()

    @pytest.mark.product_page
    class TestProductPage:

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Поиск кнопки город')
        @pytest.mark.smoke
        def test_should_be_city_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.open()
            page.go_to_product_page()
            page.should_be_city_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Кнопка магазины')
        @pytest.mark.smoke
        def test_should_be_shops_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_shops_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка статус заказа')
        @pytest.mark.smoke
        def test_should_be_status_order_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_status_order_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('В хедере есть номер телефона')
        @pytest.mark.smoke
        def test_should_be_phone_number(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_phone_number()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('В хедере есть логотип')
        @pytest.mark.smoke
        def test_should_be_logo(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_logo()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Каталог"')
        @pytest.mark.smoke
        def test_should_be_catalog_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_catalog_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть поле поиска')
        @pytest.mark.smoke
        def test_should_be_search_field(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_search_field()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Войти"')
        @pytest.mark.smoke
        def test_should_be_enter_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_enter_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Сравнение"')
        @pytest.mark.smoke
        def test_should_be_compare_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_compare_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть кнопка "Избранное"')
        @pytest.mark.smoke
        def test_should_be_favourites_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_favourites_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('В хедере есть кнопка "Корзина"')
        @pytest.mark.smoke
        def test_should_be_basket_button(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_basket_button()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Над хедером есть баннер')
        @pytest.mark.smoke
        def test_should_be_header_banner(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            page.should_be_header_banner()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть заголовок')
        @pytest.mark.smoke
        def test_should_be_product_title(self, browser):
            link = 'https://stroylandiya.ru/'
            page = ProductPage(browser, link)
            page.should_be_product_title()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('На странице есть хлебные крошки')
        @pytest.mark.smoke
        def test_should_be_bread_crumb(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.should_be_bread_crumb()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Хлебные крошки кликабельны')
        @pytest.mark.smoke
        def test_breadcrumbs_are_clickable(self, browser):
            link = 'https://stroylandiya.ru/'
            page = CatalogPage(browser, link)
            page.breadcrumbs_are_clickable()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Должен быть рейтинг товара')
        @pytest.mark.smoke
        def test_should_be_product_rating(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            product_page = ProductPage(browser, link)
            page.open()
            page.go_to_product_page()
            product_page.should_be_product_rating()

        @allure.feature('Карточка товара')
        @allure.story('Хедер')
        @allure.title('Должно быть количество отзывов')
        @pytest.mark.smoke
        def test_should_be_reviews(self, browser):
            link = 'https://stroylandiya.ru/'
            page = MainPage(browser, link)
            product_page = ProductPage(browser, link)
            product_page.should_be_reviews()
