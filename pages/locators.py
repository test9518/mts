from selenium.webdriver.common.by import By


class MainPageLocators:
    MAIN_BANNER = (By.CSS_SELECTOR, '.main__banners-main')
    ITEMS_BANNER = (By.CSS_SELECTOR, '.main__banners-items')
    BODY = (By.TAG_NAME, 'body')
    ORDER_STATUS_BUTTON = (By.CSS_SELECTOR, '.button-header')
    GIFT_BUTTON = (By.CSS_SELECTOR, 'button#popmechanic-button-59314')
    GET_GIFT = (By.CSS_SELECTOR, 'div[class="popmechanic-main popmechanic-screen popmechanic-screen_active"]')
    GIFT_CLOSE = (By.CSS_SELECTOR, 'button[class="popmechanic-button popmechanic-button-2"]')
    GEO = (By.CSS_SELECTOR, '#city_locate')
    CLOSE_GEO = (By.CSS_SELECTOR, '.cities-locate .popup__close-x')
    POPULAR_CATEGORIES = (By.CSS_SELECTOR, '.main__categories .categories__container')
    HIT_SALES = (By.CSS_SELECTOR, '.hits')
    POPULAR_BRANDS = (By.CSS_SELECTOR, '.popular-brands__container')
    LOWER_BANNER = (By.CSS_SELECTOR, '.promos')
    EXPERT_ADVICES = (By.CSS_SELECTOR, '.expert-advices')


class MainPageHeaderLocators:
    PHONE_NUMBER = (By.CSS_SELECTOR, '.header-phone')
    CITY_BUTTON = (By.CSS_SELECTOR, '.city-choose-wrapper')
    SHOPS_BUTTON = (By.CSS_SELECTOR, '.shop_link')
    ORDER_STATUS_BUTTON = (By.CSS_SELECTOR, '.button-header')
    PHONE = (By.CSS_SELECTOR, '.header-phone')
    LOGO = (By.CSS_SELECTOR, '.header-v2-logo')
    CATALOG_BUTTON = (By.CSS_SELECTOR, '.header-v2-catalog-button')
    CATALOG_BUTTON_HOVER = (By.CSS_SELECTOR, '.header-v2-catalog:hover')
    CATALOG_BODY = (By.CSS_SELECTOR, '.fb-header-catalog-menu_opened')
    CATALOG_CATEGORIES = (By.CSS_SELECTOR, '.fb-header-catalog-menu__parent-link')
    SEARCH_INPUT = (By.CSS_SELECTOR, '.search-input.js-digi-search-input')
    EMPTY_FIELD = (By.CSS_SELECTOR, '.search-input-digi-reset-btn.js-digi-reset-btn-desktop.hide')
    CLEAR_SEARCH_INPUT_BTN = (By.CSS_SELECTOR, '.search-input-digi-reset-btn')
    ACTIONS_BUTTON = (By.CSS_SELECTOR, '.header-v2-action')
    ACTIONS_BUTTON_FOCUS = (By.CSS_SELECTOR, '.header-v2-action:hover span')
    ENTER_BUTTON = (By.CSS_SELECTOR, '.header-v2-enter')
    COMPARE_BUTTON = (By.CSS_SELECTOR, '.header-v2-compare')
    FAVOURITES_BUTTON = (By.CSS_SELECTOR, '.header-v2-favourites')
    FAVOURITES_COUNT = (By.CSS_SELECTOR, '.header-v2-favourites span.count')
    BASKET_BUTTON = (By.CSS_SELECTOR, 'span.header-v2-basket')
    CATALOG_BUTTON_ICON_FIRST = (By.CSS_SELECTOR, '.header-v2-catalog .menu_button_icon_wrapper .menu_icon .first')
    CATALOG_BUTTON_ICON_THIRD = (By.CSS_SELECTOR, '.header-v2-catalog .menu_button_icon_wrapper .menu_icon .third')
    FIELD_SEARCH = (By.CSS_SELECTOR, '.search-input-div')
    SEARCH_BORDER = (By.CSS_SELECTOR, '.logo_and_menu-row .header-v2-search .search-input-div .search-input ')
    SEARCH_BORDER_FOCUS = \
        (By.CSS_SELECTOR, '.logo_and_menu-row .header-v2-search .search-input-div:hover .search-input')
    SEARCH_ICON = (By.CSS_SELECTOR, '.js-digi-search-loupe-svg')
    SEARCH_ICON_FOCUS = (By.CSS_SELECTOR, '.logo_and_menu-row .search-wrapper:hover .js-digi-search-loupe-svg')
    SEARCH_POPUP = (By.CSS_SELECTOR, '.search-results')
    CART_COUNTER = (By.CSS_SELECTOR, '.header-v2-basket span.count')
    SEARCH_BUTTON = (By.CSS_SELECTOR, '.search-input-digi-btn')
    SEARCH_BUTTON_FOCUS = (By.CSS_SELECTOR, '.search-input-digi-btn:hover ')
    HEADER_BANNER = (By.CSS_SELECTOR, '.banner.CROP')
    COMPARE_COUNTER = (By.CSS_SELECTOR, '.fb-sticky-menu__badge_type_compare.count')


class CityPopupLocators:
    CITY_POPUP_BODY = (By.CSS_SELECTOR, '#cities .cities__body')
    CITY_POPUP_BACKGROUND = (By.CSS_SELECTOR, '#cities.popup.open')
    CITY_HEADER_TITLE = (By.CSS_SELECTOR, '#cities .cities__header-title')
    CITY_POPUP_CLOSE_BUTTON = (By.CSS_SELECTOR, '#cities .popup__close-x')
    CITY_INPUT = (By.CSS_SELECTOR, '#cities .cities__form-input-container')
    CITY_INPUT_ARIA = (By.CSS_SELECTOR, '#cities .cities__form-input')
    CITY_INPUT_BORDER = (By.CSS_SELECTOR, '#cities .cities__form-input')
    CITY_IMPUT_BORDER_FOCUS = (By.CSS_SELECTOR, '#cities .cities__form-input:focus')
    CITY_INPUT_PLACEHOLDER = (By.CSS_SELECTOR, '#cities .cities__form-label')
    CITY_INPUT_PLACEHOLDER_FOCUS = (By.CSS_SELECTOR, '#cities .cities__form-input:focus ~ .cities__form-label')
    CITIES_RESULT = (By.CSS_SELECTOR, '#cities .cities__results')
    CITIES_RESULT_ITEMS = (By.CSS_SELECTOR, '#cities .cities__results-item')
    CITIES_RESULT_HINTS = (By.CSS_SELECTOR, '.cities__results.js-cities-list > .cities__results-item.js-city-btn')


class AuthPopupLocators:
    AUTH_POPUP_BODY = (By.CSS_SELECTOR, 'div.auth-popup__body')
    AUTH_POPUP_BACKGROUND = (By.CSS_SELECTOR, '.fancybox-container.fancybox-is-open, #fancybox-container-1')
    AUTH_TITLE = (By.CSS_SELECTOR, '.auth-popup__title')
    BUTTON_GET_CODE = (By.CSS_SELECTOR, 'button.auth-popup-mobile__form-submit')
    LOGIN_BY_EMAIL_BUTTON = (By.CSS_SELECTOR, '.auth-popup-mobile__link-to-email')
    AUTH_POPUP_CLOSE_BUTTON = (By.CSS_SELECTOR, '.auth-popup__body a.popup__close-x')
    PHONE_INPUT = (By.CSS_SELECTOR, '.auth-popup .auth-popup-mobile__input')
    AUTH_INPUT_PLACEHOLDER = (By.CSS_SELECTOR, '.auth-popup-mobile__input-label')
    AUTH_INPUT_PLACEHOLDER_FOCUS = \
        (By.CSS_SELECTOR, '.auth-popup .auth-popup-mobile__input:focus ~ .auth-popup-mobile__input-label')
    AUTH_INPUT_BORDER = (By.CSS_SELECTOR, '.auth-popup .auth-popup-mobile__input')
    AUTH_INPUT_BORDER_FOCUS = \
        (By.CSS_SELECTOR, '.auth-popup .auth-popup-mobile__input:focus, .auth-popup .auth-popup-mobile__input:active')
    AUTH_ERROR_PLACEHOLDER = (By.CSS_SELECTOR, '.auth-popup .auth-popup-input.error + .auth-popup-label')
    AUTH_ERROR_BORDER = (By.CSS_SELECTOR, '.auth-popup .auth-popup-input.error')
    AUTH_ERROR_MESSAGE = (By.CSS_SELECTOR, '.auth-popup .error#phone')
    AUTH_YANDEX_CAPTCHA = (By.CSS_SELECTOR, '.CheckboxCaptcha  .CheckboxCaptcha-Button')
    AUTH_CAPTCHA_CHECKBOX = (By.CSS_SELECTOR, '.CheckboxCaptcha-Anchor')
    AUTH_CHECKBOX_CHECKED = (By.CSS_SELECTOR, '.CheckboxCaptcha-Checkbox')
    ENTER_CODE_BODY = (By.CSS_SELECTOR, '.auth-popup__body')
    ENTER_CODE_TITLE = (By.CSS_SELECTOR, '.fancybox-container .auth-popup-mobile__call-header')
    ENTER_CODE_EMAIL_LOGIN = (By.CSS_SELECTOR, '.auth-popup-mobile__call-login-email')
    ENTER_CODE_TIMER = (By.CSS_SELECTOR, '.auth-popup-mobile__call-form-repeat-btn')
    ENTER_CODE_CHANGE_BUTTON = (By.CSS_SELECTOR, '.auth-popup-mobile__call-change-number-link')
    ENTER_CODE_FIELD = (By.CSS_SELECTOR, '.auth-popup .auth-popup-mobile__call-form-input')
    ENTER_CODE_PHONE_NUM = (By.CSS_SELECTOR, '.auth-popup-mobile__call-phone-number')
    ENTER_CODE_ERROR = (By.CSS_SELECTOR, '.auth-popup .error')
    ENTER_CODE_INCORRECT_CODE = (By.CSS_SELECTOR, '.auth-popup .error#lastDigits2')
    ENTER_CODE_FOUR_CHARACTERS = (By.CSS_SELECTOR, '.auth-popup .error#lastDigits')
    TIME_IS_UP_TITLE = (By.CSS_SELECTOR, '.fancybox-container .auth-popup-mobile__call-header')
    TIME_IS_UP_GET_CODE = (By.CSS_SELECTOR, '.fancybox-container .auth-popup__submit-btn--active')
    TIME_IS_UP_EMAIL_LOGIN = \
        (By.CSS_SELECTOR, '.auth-popup-mobile__call-login-email, .auth-popup-mobile__call-nocall-link')
    EMAIL_INPUT = (By.CSS_SELECTOR, 'auth-popup-input auth-popup-email__input ')
    NEW_USER_CHECKBOX = (By.CSS_SELECTOR, 'input.auth-popup__agreement-input')
    NEW_USER_AGREEMENT = (By.CSS_SELECTOR, 'a.auth-popup__agreement-label-link.js-agreement')
    NEW_USER_POLICY = (By.CSS_SELECTOR, 'a.auth-popup__agreement-label-link.js-politics')
    NEW_USER_EMAIL_INPUT = (By.CSS_SELECTOR, '.auth-popup .auth-popup-email__input')
    BACK_LINK = \
        (By.CSS_SELECTOR,
         '.auth-popup__body.auth-popup-mobile__body.auth-popup__agreement-popup a.auth-popup__header-back-link')
    BACK_LINK_POLICY = (By.CSS_SELECTOR, '.auth-popup__header-back-link')
    CLOSE_BUTTON = (By.CSS_SELECTOR, '.popup__close-svg use')
    NEW_USER_POPUP_BODY = (By.CSS_SELECTOR, '#auth-form-new .auth-popup__body')
    AGREEMENT_TITLE = \
        (By.CSS_SELECTOR,
         'div.auth-popup__body.auth-popup-mobile__body .auth-popup__agreement-popup div.auth-popup__politics h2')
    POLICY_TITLE = (By.CSS_SELECTOR, 'h2.auth-popup-mobile__call-header')
    REGISTRATION_BUTTON = (By.CSS_SELECTOR, '.fancybox-stage .auth-popup-mobile__form-submit-container')
    REGISTRATION_BUTTON_INACTIV = (By.CSS_SELECTOR, '.auth-popup__confirm-registation--inactive')
    NEW_USER_ERROR_BORDER = (By.CSS_SELECTOR, '.auth-popup .js-confirm-input.error')
    NEW_USER_ERROR_MSG = (By.CSS_SELECTOR, '.auth-popup-mobile__call span.error#lastDigits')
    ASD = (By.CSS_SELECTOR, '.auth-popup input[type="checkbox"]')


class MainPageSearchPopupLocators:
    FREQUENTLY_SEARCHED = (By.CSS_SELECTOR, '.search-results__popular-searches')
    POPULAR_GOODS = (By.CSS_SELECTOR, '.search-results__items')
    POPULAR_GOODS_PRODUCTS = (By.CSS_SELECTOR, '.search-results__item.js-article-item')
    ITEM_TITLE = (By.CSS_SELECTOR, '.search-results__item-title')
    ITEM_PRICE = (By.CSS_SELECTOR, '.search-results__item-price--current')
    ITEM_PRICE_NEW = (By.CSS_SELECTOR, 'p.search-results__item-price--new')
    ITEM_PRICE_OLD = (By.CSS_SELECTOR, 'p.search-results__item-price--old')
    ITEM_PHOTO = (By.CSS_SELECTOR, '.search-results__item-img')
    ADD_CART_BUTTON = (By.CSS_SELECTOR, '.search-results__background .cart')
    ADD_CART_BUTTON_FOCUS = (By.CSS_SELECTOR, '.search-results__background .cart:hover')
    ADD_CART_MINUS = \
        (By.CSS_SELECTOR, '.cart__amount-button.cart__amount-decrement--search-result.btn_reset.product_card_qty--btn')
    ADD_CART_PLUS = \
        (By.CSS_SELECTOR,
         '.cart__amount-button.cart__amount-increment--search-result.btn_reset.product_card_qty--btn.js_plus-item')
    ADD_CART_NUMBER = (By.CSS_SELECTOR, '.product_card_qty--input.cart__amount-value.js-digi-amount-input')
    SEARCH_HISTORY = (By.CSS_SELECTOR, '.search-results__last-searches')
    SEARCH_HISTORY_ITEMS = (By.CSS_SELECTOR, '.search-results__last-searches .search-results__value-text')
    FREQUENTLY_SEARCHED_ITEMS = (By.CSS_SELECTOR, '.search-results__popular-searches .js-highlight-text')
    SEARCH_HISTORY_ITEMS_FOCUS = (By.CSS_SELECTOR, '.search-results__value-text:hover')
    DELETE_SEARCH_HISTORY_ITEM = (By.CSS_SELECTOR, '.search-results__last-searches-item .search-results__close-icon')
    SEARCH_CATEGORIES = (By.CSS_SELECTOR, '.search-results__categories')
    SEARCH_CATEGORIES_ITEMS = \
        (By.CSS_SELECTOR, '.search-results__value-text.search-results__popular-searches-text.js-search-hint')
    SHOW_ALL_RESULTS = (By.CSS_SELECTOR, '.search-results__all-results-btn')


class MainBannerLocators:
    ACTIVE_SLIDE = (By.CSS_SELECTOR, '.main__banners-main .swiper-slide.swiper-slide-active')
    MAIN_NAVIGATION = (By.CSS_SELECTOR, '.main__banners-main .main__banners-main-navigation')
    PREV_BUTTON = (By.CSS_SELECTOR, '.main__banners-main .main__banners-main-navigation-prev')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.main__banners-main-navigation-next')
    CURRENT_VALUE = (By.CSS_SELECTOR, '.swiper-pagination-current.main__banners-main-info-current-value')
    TOTAL_VALUE = (By.CSS_SELECTOR, '.swiper-pagination-total.main__banners-main-info-total-value')
    SLIDES = (By.CSS_SELECTOR, '.main__banners-main .swiper-slide')


class ItemsOfDayLocators:
    ITEM_OF_DAY = (By.CSS_SELECTOR, '.main__banners-items')
    ITEMS = (By.CSS_SELECTOR, '.main__banners-items .item-of-day')
    ITEM_TITLE = (By.CSS_SELECTOR, '.item-of-day__name')
    ACTIV_SLIDE = (By.CSS_SELECTOR, '.main__banners-items .swiper-slide.swiper-slide-active .item-of-day')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.main__banners-items .main__banners-items-navigation-next')
    PREV_BUTTON = (By.CSS_SELECTOR, '.main__banners-items .main__banners-items-navigation-prev')
    BLOCK_TITLE = (By.CSS_SELECTOR, '.item-of-day__title')
    BLOCK_TIMER = (By.CSS_SELECTOR, '.item-of-day__counter')
    BLOCK_PROMO = (By.CSS_SELECTOR, '.main__banners-items .fb-product-card__badge-new')
    ITEMS_PHOTO = (By.CSS_SELECTOR, '.item-of-day__img')
    ITEMS_PRICE = (By.CSS_SELECTOR, '.item-of-day__price')
    BLACK_PRICE = (By.CSS_SELECTOR, '.main__banners-items .item-of-day__price-current')
    ITEM_PRICE_NEW = (By.CSS_SELECTOR, '.main__banners-items .item-of-day__price-new')
    ADD_CART_BTN = (By.CSS_SELECTOR, '.main__banners-items .cart__container .cart')
    ADD_CART_MINUS = (By.CSS_SELECTOR, '.main__banners-items .cart__amount-button.js_minus-item')
    ADD_CART_NUMBER = (By.CSS_SELECTOR, '.main__banners-items .cart__container .product_card_qty--input')
    ADD_CART_PLUS = (By.CSS_SELECTOR, '.main__banners-items .cart__amount-button.js_plus-item')


class PopularCategoriesLocators:
    ITEMS = (By.CSS_SELECTOR, '.categories__item')
    SLIDE_TITLE = (By.CSS_SELECTOR, '.categories__item .categories__item-title-link')
    ACTIV_SLIDE = (By.CSS_SELECTOR, '.main__categories .swiper-slide-active')
    ACTIV_SLIDE_TITLE = (By.CSS_SELECTOR, '.main__categories .swiper-slide-active .categories__item-title-link')
    ACTIV_SLIDE_IMG = \
        (By.CSS_SELECTOR, '.categories__container .swiper-wrapper .categories__item--info .categories__item-img')
    NEXT_BUTTON = (By.CSS_SELECTOR,
                   '.js-swiper__categories-next.categories__slider-navigation-link.categories__slider-navigation-next')
    PREV_BUTTON = (By.CSS_SELECTOR,
                   '.js-swiper__categories-prev.categories__slider-navigation-link.categories__slider-navigation-prev')


class HitsSalesLocators:
    ITEMS = (By.CSS_SELECTOR, '#main .hits__item')
    ACTIV_SLIDE = (By.CSS_SELECTOR, '.hits .swiper-slide.swiper-slide-active')
    ITEMS_TITLE = (By.CSS_SELECTOR, '.hits__item-title')
    ITEMS_PHOTO = (By.CSS_SELECTOR, '.custom-hit-bullet.js-custom-hit-bullet')
    ITEMS_PRICE = (By.CSS_SELECTOR, '.hits__item-price')
    BLACK_PRICE = (By.CSS_SELECTOR, '.hits__item-price-current')
    RED_PRICE = (By.CSS_SELECTOR, '.hits__item-price-new')
    ADD_CART_BTN = (By.CSS_SELECTOR, '.main__hits-container.hits .cart__container .cart')
    FAVOURITE_BTN = (By.CSS_SELECTOR, '.main__hits-container.hits .hits__item-favourite')
    REVIEWS_RATING = (By.CSS_SELECTOR, '.hits__item-reviews-rating')
    COMMENTS_BLOCK = (By.CSS_SELECTOR, '.hits__item-reviews-comments')
    COMMENTS_BTN = (By.CSS_SELECTOR,'.hits__item-reviews-comments-svg')
    COMMENTS_VALUE = (By.CSS_SELECTOR, '.hits__item-reviews-comments .hits__item-reviews-rating-value ')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.hits__slider-navigation-next')
    PREV_BUTTON = (By.CSS_SELECTOR, '.hits__slider-navigation-prev')
    ACTIV_SLIDE_TITLE = (By.CSS_SELECTOR, '.swiper-slide.swiper-slide-active .hits__item-title')
    ADD_CART_MINUS = (By.CSS_SELECTOR, '.hits .product_card_qty--btn.js_minus-item')
    ADD_CART_NUMBER = (By.CSS_SELECTOR, '.hits .cart__container .product_card_qty--input')
    ADD_CART_PLUS = (By.CSS_SELECTOR, '.hits .product_card_qty--btn.js_plus-item')
    PHOTO_SWITCHING_BLOCKS = (By.CSS_SELECTOR, '.custom-hit-bullet.js-custom-hit-bullet')
    ACTIV_PHOTO = (By.CSS_SELECTOR, '.hits__item-img .swiper-slide.swiper-slide-active .hit__item-img')
    PHOTO_SWITCHING_POINTS = (By.CSS_SELECTOR, '.hits .hits__slider-pagination.hit-clickable.swiper-pagination-bullets')
    PHOTO_DOTS = \
        (By.CSS_SELECTOR,
         '.hits .hits__slider-pagination.hit-clickable.swiper-pagination-bullets .swiper-pagination-bullet')
    HITS_ALL_BTN = (By.CSS_SELECTOR, '.hits__all-hits-link')
    ITEM_ADDED_TO_FAVORITES = (By.CSS_SELECTOR, '.hits__item-favourite.favorite-indicator.active')


class PopularBrandsLocators:
    ITEMS = (By.CSS_SELECTOR, '.popular-brands__brand')
    BRANDS_TITLE = (By.CSS_SELECTOR, '.popular-brands__brand-text')
    BRANDS_LOGO = (By.CSS_SELECTOR, '.popular-brands__brand-img')
    ALL_BRANDS_BTN = (By.CSS_SELECTOR, '.popular-brands__all-brands-link')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.popular-brands__slider-navigation-next')
    PREV_BUTTON = (By.CSS_SELECTOR, '.popular-brands__slider-navigation-prev')
    ACTIV_SLIDE = (By.CSS_SELECTOR, '.popular-brands .swiper-slide.swiper-slide-active')
    ACTIV_SLIDE_TITLE = (By.CSS_SELECTOR,
                         '.popular-brands .swiper-slide.swiper-slide-active .popular-brands__brand-text')


class BasketPageLocators:
    TITLE = (By.CSS_SELECTOR, '.title-v1 h1')
    ITEM = (By.CSS_SELECTOR, '.basket-item')
    ITEM_TITLE = (By.CSS_SELECTOR, '.basket-item__title')
    ITEM_PRICE = (By.CSS_SELECTOR, '.basket-price__current')


class ProductPageLocators:
    PRODUCT_TITLE = (By.CSS_SELECTOR, 'h1.dcol-8')
    PRODUCT_COUNT_INPUT = (By.CSS_SELECTOR, '.p_product_block.-shoping .product_card_qty--input')
    PRODUCT_PRICE = (By.CSS_SELECTOR, '.p_product_price .h1')
    REVIEWS_BTN_ACTIV = (By.CSS_SELECTOR, 'dcol-0 p_product_info--item_wrapper js-reviews js-btns active')
    REVIEWS_TAB_ACTIV = (By.CSS_SELECTOR, '.p_product_reviews.active')
    CHARACTERISTICS = (By.CSS_SELECTOR, '.p_product_params--list.text .mb-16')
    PRODUCT_RATING = (By.CSS_SELECTOR, 'div.container-fluid>div>div.dcol-0.p_product_review--wrapper .cform_rating')
    REVIEWS = (By.CSS_SELECTOR, 'main>div.container-fluid div.dcol-0.text.-sm')
    COMPARE_BTN = (By.CSS_SELECTOR, '.dcol-0.mb-8 .compare-indicator')


class SearchListingPage:
    SEARCH_KEY = (By.CSS_SELECTOR, '.results__title-article.js-results-title')


class ActionsPage:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')


class ComparePage:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')
    ITEM_BLOCK = (By.CSS_SELECTOR, 'tr .item_block')


class FavouritesPage:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')


class CatalogPageLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')


class BrandPageLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')


class LowerBannerLocators:
    SLIDES = (By.CSS_SELECTOR, '.promos .swiper-slide')
    MAIN_NAVIGATION = (By.CSS_SELECTOR, '.promos__slider-navigation')
    PREV_BUTTON = (By.CSS_SELECTOR, '.promos__slider-navigation-prev')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.promos__slider-navigation-next')
    CURRENT_VALUE = (By.CSS_SELECTOR, '.promos .swiper-pagination-current.promos__slider-current-value')
    TOTAL_VALUE = (By.CSS_SELECTOR, '.promos .swiper-pagination-total.promos__slider-total-value')
    ACTIVE_SLIDE = (By.CSS_SELECTOR, '.promos .swiper-slide.swiper-slide-active')


class ExpertAdvicesLocators:
    SLIDES = (By.CSS_SELECTOR, '.expert-advices .swiper-slide')
    SLIDES_IMG = (By.CSS_SELECTOR, '.expert-advices__card-img')
    SLIDE_CATEGORY = (By.CSS_SELECTOR, '.expert-advices__card-description-category')
    SLIDE_TITLE = (By.CSS_SELECTOR, '.expert-advices__card-description-title')
    ALL_ADVICES = (By.CSS_SELECTOR, '.expert-advices__all-advices-link')
    NEXT_BUTTON = (By.CSS_SELECTOR, '.expert-advices__slider-navigation-next')
    PREV_BUTTON = (By.CSS_SELECTOR, '.expert-advices__slider-navigation-prev')
    ACTIV_SLIDE = (By.CSS_SELECTOR, '.expert-advices .swiper-slide.swiper-slide-active')
    ACTIV_SLIDE_TITLE = \
        (By.CSS_SELECTOR, '.expert-advices .swiper-slide.swiper-slide-active .expert-advices__card-description-title')


class BlogPageLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')


class CatalogPopupLocators:
    CATEGORIES = (By.CSS_SELECTOR, '.fb-header-catalog-menu__parent-link')
    CATEGORY_SELECTED = \
        (By.CSS_SELECTOR, '.fb-header-catalog-menu__parent-link.fb-header-catalog-menu__parent-menu_selected')
    CATEGORIES_SECOND_LVL = (By.CSS_SELECTOR, '.fb-catalog-header-group-link__header')
    CATALOG_THIRD_LEVEL = (By.CSS_SELECTOR, '.fb-catalog-header-group-link__link')


class WishlistPageLocators:
    TITLE = (By.CSS_SELECTOR, 'h1#pagetitle')
    ITEMS = (By.CSS_SELECTOR, '.bascket_block_items__item')
    ITEM_TITLE = (By.CSS_SELECTOR, '.fb-product-card__title-inner')
    ITEM_PRICE = (By.CSS_SELECTOR, '.basket_left_block_name__sum__block span')


class AllBrandsPageLocators:
    TITLE = (By.CSS_SELECTOR, 'h1#pagetitle')


class FirstLvlCatalogLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')
    SECOND_LVL_CATEGORIES = (By.CSS_SELECTOR, '.fb-category-group-link__header')
    THIRD_LVL_CATEGORIES = (By.CSS_SELECTOR, '.fb-category-group-link__link')
    CATEGORIES_PHOTO = (By.CSS_SELECTOR, '.fb-category-group-link__image')
    CATEGORIES_GROUP = (By.CSS_SELECTOR, '.fb-catalog-page__content-column')
    CATEGORIES_THIRD_LVL = (By.CSS_SELECTOR, '.fb-category-group-link__link')
    SHOW_MORE_LINK = (By.CSS_SELECTOR, '.fb-category-group-link__show-more')


class SecondLvlCatalogLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')
    BACK_BTN = (By.CSS_SELECTOR, '.breadcrumbs a')
    CATEGORIES_BLOCK = (By.CSS_SELECTOR, '.fb-catalog-deep-page__category-children-row')
    CATEGORIES = (By.CSS_SELECTOR, '.fb-catalog-deep-page__category-children-column')
    BANNER_UPPER_CATEGORIES = (By.CSS_SELECTOR, '.fb-catalog-listing-page__slider')
    CATALOG_ITEM_NAME = (By.CSS_SELECTOR, '.fb-category-image-link__name')
    PRICE_BLOCK = (By.CSS_SELECTOR, '.fb-product-card__price')
    PRICE = (By.CSS_SELECTOR, '.fb-product-card__price-value')
    DISCOUNT = (By.CSS_SELECTOR, '.d-inline-block.product_card_badge.str-shield-fix.-red')
    ITEMS = (By.CSS_SELECTOR, '.fb-product-card.fb-product-card-2')
    ITEMS_PHOTO = (By.CSS_SELECTOR, '.fb-product-card__image-wrapper')
    ITEMS_TITLE = (By.CSS_SELECTOR, '.fb-product-card__title-inner')
    ADD_CART_BTN = (By.CSS_SELECTOR, '.fb-product-card.fb-product-card-2 .fb-product-card__basket-button')
    ADD_CART_BTN_HOVER = (By.CSS_SELECTOR, '.fb-product-card__basket-button:hover')
    FAVOURITE_BTN = \
        (By.CSS_SELECTOR, '.fb-product-card__wrapper .fb-product-card__favorite-button.favorite-indicator')
    REVIEWS_RATING = (By.CSS_SELECTOR, '.fb-product-card__info-item')
    COMMENTS_BLOCK = (By.CSS_SELECTOR, '.fb-product-card__info-item-icon.fb-product-card__info-item-icon--comment')
    ADD_CART_MINUS = (By.CSS_SELECTOR, '.fb-product-card__basket-action.fb-product-card__basket-action_type_remove')
    ADD_CART_PLUS = (By.CSS_SELECTOR, '.fb-product-card__basket-action.fb-product-card__basket-action_type_add')
    ADD_CART_NUMBER = (By.CSS_SELECTOR, '.fb-product-card__basket-value')
    ITEM_ADDED_TO_FAVORITES = \
        (By.CSS_SELECTOR, '.fb-product-card__favorite-button.favorite-indicator.active')
    CATEGORY_LINK = (By.CSS_SELECTOR, '.fb-collapse-block__header-inner .fb-collapse-block__title')
    SUBCATEGORY_LINK = (By.CSS_SELECTOR, '.fb-catalog-listing-page__column-filter .fb-collapse-items__item')
    SUBCATEGORY_HIDDEN_LINK = (By.CSS_SELECTOR, '.fb-collapse-items__item.fb-collapse-items__item--hidden')
    SHOW_ALL_CATEGORIES = (By.CSS_SELECTOR, '.fb-collapse-items__toggle.fb-collapse-items__toggle-visible')
    LINKS_BLOCK = (By.CSS_SELECTOR, '.fb-filter__row')
    BLOCK_ITEMS = (By.CSS_SELECTOR, '.fb-filter__row .fb-collapse-items')
    CATEGORY_TITLE = (By.CSS_SELECTOR, '.fb-collapse-block__header-inner a')
    COLLAPSE_BUTTON = \
        (By.CSS_SELECTOR,
         '.fb-collapse-items__toggle.fb-collapse-items__toggle-visible.fb-collapse-items__toggle--opened')
    SHOW_ALL_BTN = (By.CSS_SELECTOR, '.fb-collapse-items__toggle.fb-collapse-items__toggle-visible')
    PHOTO_SWITCHING_BLOCKS = (By.CSS_SELECTOR, '.custom-swiper--item')
    ACTIV_PHOTO = (By.CSS_SELECTOR, '.fb-product-card__wrapper .swiper-wrapper')
    PHOTO_SWITCHING_POINTS = (By. CSS_SELECTOR, '.swiper-pagination-bullets')
    SORT = (By.CSS_SELECTOR, '.fb-catalog-listing-page__sort')
    PHOTO_DOTS = (By.CSS_SELECTOR, '.fb-product-card__image-slider-bullet')
    COMPARE_BTN = (By.CSS_SELECTOR, '.d-none.d-lg-flex.compare-indicator')
    COMPARE_BTN_ACTIVE = (By.CSS_SELECTOR, '.d-none.d-lg-flex.compare-indicator.active')
    TOOLTIP = (By.CSS_SELECTOR, '.fb-product-card_tooltip_prop')
    TOOLTIP_ITEMS = (By.CSS_SELECTOR, '.fb-product-card_tooltip_prop .tooltip_prop_item')


class BitrixAuthLocators:
    EMAIL_INPUT = (By.CSS_SELECTOR, '.login-input')
    PASSWORD_INPUT = (By.CSS_SELECTOR, '#authorize_password .login-input')
    SUBMIT_BTN = (By.CSS_SELECTOR, '.login-btn-green')


class ListingLocators:
    TITLE = (By.CSS_SELECTOR, '#pagetitle')
    BREAD_CRUMB = (By.CSS_SELECTOR, '.fb-breadcrumb')
    BREAD_CRUMB_ITEM = (By.CSS_SELECTOR, '.fb-breadcrumb__link')
    QUANTITY = (By.CSS_SELECTOR, '.page-top-main #listing-count-elem-main')
    BACK_BTN = (By.CSS_SELECTOR, '.fb-filter-category__back')
    CATEGORY_BTN = (By.CSS_SELECTOR, '.fb-filter-category__list-item.fb-filter-category__list-item--active')
    FILTERS = (By.CSS_SELECTOR, '.fb-filter__row')
    REMOVE_ALL = (By.CSS_SELECTOR, '#fb-filter-chips-remove-all')
    FILTER_BLOCK = (By.CSS_SELECTOR, '.fb-filter__row')
    FILTER_TITLE = (By.CSS_SELECTOR, '.fb-filter__row .fb-collapse-block__title')
    CLOSE_FILTERS = (By.CSS_SELECTOR, '.fb-collapse-block')
    OPEN_FILTER = (By.CSS_SELECTOR, '.fb-collapse-block.fb-collapse-block--open')
    TOGGLE_SWITCHES = (By.CSS_SELECTOR, '.fb-catalog-listing-page__filter .fb-switch')
    RANGES = (By.CSS_SELECTOR, '.fb-catalog-listing-page__column-filter .fb-slider')
    FIELD_VALUES = (By.CSS_SELECTOR, '.fb-slider__slider.noUi-target.noUi-ltr.noUi-horizontal.noUi-txt-dir-ltr')
    CHECKBOXES = (By.CSS_SELECTOR, '.fb-catalog-listing-page__column-filter .fb-checkbox')
    INPUT_MIN = (By.CSS_SELECTOR, '.fb-input.fb-input-number.fb-slider-input__from .fb-input__input input')
    INPUT_MAX = (By.CSS_SELECTOR, '.fb-input.fb-input-number.fb-slider-input__to .fb-input__input input')
    INPUT_NOW = (By.CSS_SELECTOR, '.noUi-handle.noUi-handle-lower')
    INPUT_MIN_FOCUS = (By.CSS_SELECTOR, '.fb-input.fb-input-number.fb-slider-input__from.fb-input--focus')
    INPUT_MAX_FOCUS = (By.CSS_SELECTOR, '.fb-input.fb-input-number.fb-slider-input__to.fb-input--focus')
    MIN_DOT = (By.CSS_SELECTOR, '.noUi-handle.noUi-handle-lower .noUi-touch-area')
    MAX_DOT = (By.CSS_SELECTOR, '.noUi-handle.noUi-handle-upper .noUi-touch-area')
    TRANSFORM_DOTS = (By.CSS_SELECTOR, '.fb-slider-input .noUi-origin')
    TEG_CLOUD = (By.CSS_SELECTOR, '.fb-filter-chips__list')
    REMOVE_ALL_CLOUD = (By.CSS_SELECTOR, '.fb-filter-chips-remove-all')
    CLOUD_TEXT = (By.CSS_SELECTOR, '.fb-chip__text')
    CLOSE_CLOUD = (By.CSS_SELECTOR, '.fb-chip__close')
    SORTING = (By.CSS_SELECTOR, '.fb-catalog-listing-page__sort .fb-select')
    SORTING_VALUE = (By.CSS_SELECTOR, '.fb-select__option')
    SORTING_VALUE_SELECTED = (By.CSS_SELECTOR, '.fb-select__option.fb-select__option--selected')
    OPEN_SORTING = (By.CSS_SELECTOR, '.fb-select.fb-select--open')
    PRICE_BLOCK = (By.CSS_SELECTOR, '.fb-product-card__price')
    PRICE = (By.CSS_SELECTOR, '.fb-product-card__price-value')
    DISCOUNT = (By.CSS_SELECTOR, '.d-inline-block.product_card_badge.str-shield-fix.-red')
    ITEMS = (By.CSS_SELECTOR, '.fb-product-card.fb-product-card-2')
    ITEMS_PHOTO = (By.CSS_SELECTOR, '.fb-product-card__image-wrapper')
    ITEMS_TITLE = (By.CSS_SELECTOR, '.fb-product-card__title-inner')
    ADD_CART_BTN = (By.CSS_SELECTOR, '.fb-product-card.fb-product-card-2 .fb-product-card__basket-button')
    FAVOURITE_BTN = \
        (By.CSS_SELECTOR, '.fb-product-card__favorite-button-icon.fb-product-card__favorite-button-icon_not-filled')
    REVIEWS_RATING = (By.CSS_SELECTOR, '.fb-product-card__info-item')
    COMMENTS_BLOCK = (By.CSS_SELECTOR, '.fb-product-card__info-item-icon.fb-product-card__info-item-icon--comment')
    PHOTO_SWITCHING_BLOCKS = (By.CSS_SELECTOR, '.fb-product-card__image-slider-item.swiper-slide')
    ACTIV_PHOTO = (By.CSS_SELECTOR, '.fb-product-card__image-slider-item.swiper-slide.swiper-slide-active img')





