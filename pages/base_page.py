import time
from selenium.common.exceptions import NoSuchElementException
from random import randint
from selenium.common.exceptions import JavascriptException
from selenium.webdriver.common.actions.wheel_input import ScrollOrigin
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.wait import TimeoutException
import allure
from allure_commons.types import AttachmentType
import requests
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from .locators import MainPageLocators
from .locators import BitrixAuthLocators
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    def __init__(self, browser, url, timeout=10):
        self.browser = browser
        self.url = url
        # self.browser.implicitly_wait(timeout)
        WebDriverWait(browser, timeout)

    def close_gift(self):
        gift = self.find_element(*MainPageLocators.GET_GIFT)
        if not gift:
            return
        elif gift.is_displayed():
            self.find_element(*MainPageLocators.GIFT_CLOSE).click()

    def open(self):
        self.browser.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
            'source': '''
                delete window.cdc_adoQpoasnfa76pfcZLmcfl_Array;
                delete window.cdc_adoQpoasnfa76pfcZLmcfl_Promise;
                delete window.cdc_adoQpoasnfa76pfcZLmcfl_Symbol;
          '''
        })
        self.browser.get(self.url)
        if self.element_is_displayed(*MainPageLocators.GEO, False):
            self.find_element(*MainPageLocators.CLOSE_GEO).click()

    @allure.step("Поиск элемента {what} с помощью метода {how}")
    def is_element_present(self, how, what, check_visibility=True):
        if not check_visibility:
            try:
                self.find_element(how, what)
            except NoSuchElementException:
                self.allure_find_element('Элемент не найден')
                return False
            return True
        try:
            element = self.find_element(how, what)
            self.browser.execute_script(
                f'document.querySelector("{what}").style.border=\"3px solid rgba(255, 0, 0, 1)\";')
            self.allure_find_element('Элемент найден и отображается на странице')
            self.browser.execute_script(
                f'document.querySelector("{what}").style.border=\"0px solid rgba(255, 0, 0, 1)\";')
        except (NoSuchElementException, JavascriptException):
            self.allure_find_element('Элемент не найден')
            return False
        return True

    def auth(self):
        link = 'https://stroylandiya.ru/bitrix/admin/index.php#authorize'
        self.browser.get(link)
        email_input = self.find_element(*BitrixAuthLocators.EMAIL_INPUT)
        email_input.send_keys('denis.prokhorenko@stroylandiya.ru')
        password_input = self.find_element(*BitrixAuthLocators.PASSWORD_INPUT)
        password_input.send_keys('Simple123$%^')
        self.find_element(*BitrixAuthLocators.SUBMIT_BTN).click()
        time.sleep(2)


    # def is_element_present(self, how, what, check_visibility=True):
    #     if not check_visibility:
    #         try:
    #             self.find_element(how, what)
    #         except NoSuchElementException:
    #             self.allure_find_element('Элемент не найден')
    #             return False
    #         return True
    #     try:
    #         element = WebDriverWait(self.browser, 10).until(
    #             lambda driver: self.find_element(how, what) if self.find_element(how, what).is_displayed() else False)
    #         self.browser.execute_script(
    #             f'document.querySelector("{what}").style.border=\"3px solid rgba(255, 0, 0, 1)\";')
    #         self.allure_find_element('Элемент найден и отображается на странице')
    #         self.browser.execute_script(
    #             f'document.querySelector("{what}").style.border=\"0px solid rgba(255, 0, 0, 1)\";')
    #     except (NoSuchElementException, JavascriptException, TimeoutException):
    #         self.allure_find_element('Элемент не найден')
    #         return False
    #     return True


    def get_random_element(self, how, what, first=0, last=1):
        try:
            elements = self.find_elements(how, what)
            element = elements[randint(first, len(elements) - last)]
        except NoSuchElementException:
            self.allure_find_element(f"Элементы '{what}' не найдены в DOM")
            return False
        self.allure_find_element(f"Выбран случайный элемент '{what}'")
        return element

    def get_style_value(self, how, what, key):
        try:
            style_value = self.find_element(how, what).value_of_css_property(f'{key}')
            allure.attach(f"Style Value of element '{what}' with key '{key}': {style_value}")
        except NoSuchElementException:
            allure.attach(f"Element '{what}' not found")
            return False
        return style_value

    def get_element_text(self, how, what):
        try:
            text = self.find_element(how, what).text
            allure.attach(f"Text of element '{what}': {text}")
        except NoSuchElementException:
            allure.attach(f"Element '{what}' not found")
            return False
        return text

    def is_disappeared(self, how, what, timeout=4, screen=True):
        if not screen:
            try:
                WebDriverWait(self.browser, timeout). \
                    until_not(EC.visibility_of_element_located((how, what)))
            except TimeoutException:
                return False
            return True
        else:
            try:
                WebDriverWait(self.browser, timeout). \
                    until_not(EC.visibility_of_element_located((how, what)))
                self.allure_find_element('Элемент закрылся')
            except TimeoutException:
                self.allure_find_element('Элемент не закрылся')
                return False
            return True

    def allure_find_element(self, name):
        with allure.step(name):
            allure.attach(self.browser.current_url, name='url', attachment_type=AttachmentType.TEXT)
            allure.attach(self.browser.get_screenshot_as_png(), name='Скрин', attachment_type=AttachmentType.PNG)

    def allure_before_after(self, step):
        if step == 'before':
            with allure.step('До'):
                allure.attach(self.browser.current_url, name='url', attachment_type=AttachmentType.TEXT)
                allure.attach(self.browser.get_screenshot_as_png(), name='Скрин', attachment_type=AttachmentType.PNG)
        if step == 'after':
            with allure.step('После'):
                allure.attach(self.browser.current_url, name='url', attachment_type=AttachmentType.TEXT)
                allure.attach(self.browser.get_screenshot_as_png(), name='Скрин', attachment_type=AttachmentType.PNG)

    def move_to(self, element):
        action = ActionChains(self.browser)
        action.move_to_element(element).perform()

    def scroll_to(self, element):
        action = ActionChains(self.browser)
        action.scroll_to_element(element).perform()

    def scroll_from_origin(self, element, x=0, y=100):
        action = ActionChains(self.browser)
        scroll_origin = ScrollOrigin.from_element(element, 0, 0)
        action.scroll_from_origin(scroll_origin, x, y).perform()

    def scroll_to_block(self, how, what, num=100):
        block = self.find_element(how, what)
        self.scroll_from_origin(block, 0, num)
        time.sleep(0.4)

    def move_by_offset(self, x, y):
        action = ActionChains(self.browser)
        action.move_by_offset(x, y).perform()

    def action_click(self):
        action = ActionChains(self.browser)
        action.click().perform()

    def find_element(self, how, what, screen=True):
        try:
            element = self.browser.find_element(how, what)
        except NoSuchElementException:
            self.allure_find_element('Элемент не найден')
            return False
        return element

    def find_elements(self, how, what):
        try:
            elements = self.browser.find_elements(how, what)
        except NoSuchElementException:
            self.allure_find_element('Элемент не найден')
            return False
        return elements

    def element_is_displayed(self, how, what, screen=True):
        if not screen:
            try:
                element = self.find_element(how, what)
                if not element.is_displayed():
                    return False
                else:
                    return True
            except NoSuchElementException:
                return False
        try:
            element = self.find_element(how, what)
            if not element.is_displayed():
                self.allure_find_element('Элемент не отображается на странице')
                return False
            else:
                self.allure_find_element('Элемент отображается на странице')
                return True
        except NoSuchElementException:
            self.allure_find_element('Элемент не найден')
            return False

    def get_element_attribute(self, how, what, attribute):
        try:
            value = self.find_element(how, what).get_attribute(attribute)
        except NoSuchElementException:
            return False
        return value

    def wait_clickable(self, how, what):
        WebDriverWait(self.browser, 30).until(EC.element_to_be_clickable((how, what))).click()

    def wait_visibility_of(self, how, what):
        time.sleep(1)
        element = WebDriverWait(self.browser, 30).until(EC.visibility_of_element_located((how, what)))
        return element

