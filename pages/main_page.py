import pytest
from .base_page import BasePage
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from .locators import *
import time
from selenium.webdriver.common.keys import Keys


class MainPage(BasePage):

    def should_be_city_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.CITY_BUTTON), \
            'На странице нет кнопки город'
        # assert self.element_is_displayed(*MainPageHeaderLocators.CITY_BUTTON), \
        #     'Кнопка города не отображается на странице'

    def go_to_city_popup(self):
        button = self.find_element(*MainPageHeaderLocators.CITY_BUTTON)
        button.click()
        assert self.is_element_present(*CityPopupLocators.CITY_POPUP_BODY), \
            'Попап город не открылся'
        # assert self.element_is_displayed(*CityPopupLocators.CITY_POPUP_BODY), \
        #     'Попап города не отображается на странице'

    def go_to_product_page(self):
        self.find_element(*ItemsOfDayLocators.ITEM_TITLE).click()

    def background_should_be_blurred_city(self):
        # time.sleep(2)
        background = self.get_style_value(*CityPopupLocators.CITY_POPUP_BACKGROUND, 'backdrop-filter')
        self.allure_before_after('after')
        assert 'blur' in background, f'К фону не применен блюр, {background}'
        blur_value = (int(''.join(filter(str.isdigit, background))))
        assert blur_value > 0, f'Значение фильтра равно либо меньше 0, {background}'

    def should_be_title_city(self):
        assert self.is_element_present(*CityPopupLocators.CITY_HEADER_TITLE), \
            'В попапе нет заголовка'
        # assert self.element_is_displayed(*CityPopupLocators.CITY_HEADER_TITLE), \
        #     'Заголовок не отображается на странице'
        title = self.get_element_text(*CityPopupLocators.CITY_HEADER_TITLE)
        assert title == 'Выберите город', f'Заголовок не соответствует требованиям, {title}'

    def should_be_input_field(self):
        assert self.is_element_present(*CityPopupLocators.CITY_INPUT), \
            'В попапе нет поля ввода города'

    def city_should_be_placeholder(self):
        assert self.is_element_present(*CityPopupLocators.CITY_INPUT_PLACEHOLDER), \
            'Плейсхолдер в поле ввода города не найден'
        placeholder = self.get_element_text(*CityPopupLocators.CITY_INPUT_PLACEHOLDER)
        assert placeholder == "Ваш город...", f'Текст плейсхолдера не соответствует требованиям {placeholder}'

    def city_placeholder_focus(self):
        self.is_element_present(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, False)
        self.allure_before_after('before')
        top_value_after = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, 'top')
        font_size_value_after = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, 'font-size')
        self.find_element(*CityPopupLocators.CITY_INPUT).click()
        self.allure_before_after('after')
        top_value_before = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER_FOCUS, 'top')
        font_size_value_before = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER_FOCUS, 'font-size')
        assert (int(''.join(filter(str.isdigit, top_value_after)))) > \
               (int(''.join(filter(str.isdigit, top_value_before)))), 'Плейсхолдер не смещается вверх'
        assert (int(''.join(filter(str.isdigit, font_size_value_after)))) > \
               (int(''.join(filter(str.isdigit, font_size_value_before)))), 'Плейсхолдер не уменьшается'

    def city_frame_should_change_color(self):
        self.find_element(*CityPopupLocators.CITY_HEADER_TITLE).click()
        self.is_element_present(*CityPopupLocators.CITY_INPUT_BORDER, False)
        self.allure_before_after('before')
        color_after = self.get_style_value(*CityPopupLocators.CITY_INPUT_BORDER, 'border')
        self.find_element(*CityPopupLocators.CITY_INPUT).click()
        self.is_element_present(*CityPopupLocators.CITY_IMPUT_BORDER_FOCUS, False)
        self.allure_before_after('after')
        color_befor = self.get_style_value(*CityPopupLocators.CITY_IMPUT_BORDER_FOCUS, 'border')
        assert color_after != color_befor, 'Рамка поля не подсвечивается'

    def city_placeholder_defocus(self):
        self.is_element_present(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, False)
        self.allure_before_after('before')
        top_value_after = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER_FOCUS, 'top')
        font_size_value_after = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER_FOCUS, 'font-size')
        self.find_element(*CityPopupLocators.CITY_HEADER_TITLE).click()
        self.allure_before_after('after')
        top_value_before = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, 'top')
        font_size_value_before = self.get_style_value(*CityPopupLocators.CITY_INPUT_PLACEHOLDER, 'font-size')
        assert (int(''.join(filter(str.isdigit, top_value_after)))) < \
               (int(''.join(filter(str.isdigit, top_value_before)))), \
            'Плейсхолдер не смещается в центр поля'
        assert (int(''.join(filter(str.isdigit, font_size_value_after)))) < \
               (int(''.join(filter(str.isdigit, font_size_value_before)))), \
            'Плейсхолдер не увеличивается'

    def city_field_is_not_highlighted_when_focus_is_removed(self):
        self.find_element(*CityPopupLocators.CITY_INPUT).click()
        self.is_element_present(*CityPopupLocators.CITY_IMPUT_BORDER_FOCUS, False)
        self.allure_before_after('before')
        color_after = self.get_style_value(*CityPopupLocators.CITY_IMPUT_BORDER_FOCUS, 'border')
        self.find_element(*CityPopupLocators.CITY_HEADER_TITLE).click()
        self.is_element_present(*CityPopupLocators.CITY_INPUT_BORDER, False)
        self.allure_before_after('after')
        color_before = self.get_style_value(*CityPopupLocators.CITY_INPUT_BORDER, 'border')
        assert color_after != color_before, \
            'Рамка поля подсвечивается'

    def there_is_a_list_of_available_cities(self):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        time.sleep(0.5)
        assert self.is_element_present(*CityPopupLocators.CITIES_RESULT), \
            'В попапе нет списка доступных городов'

    def the_list_of_cities_scrolls(self):
        self.allure_before_after('before')
        cities = self.find_elements(*CityPopupLocators.CITIES_RESULT_ITEMS)
        coordinates_first_hints_before = cities[0].rect.get('y')
        self.scroll_to(cities[-1])
        time.sleep(0.3)
        coordinates_first_hints_after = cities[0].rect.get('y')
        self.allure_before_after('after')
        assert coordinates_first_hints_before != coordinates_first_hints_after, \
            'Список доступных городов  не скроллится'

    def text_color_inlist_of_cities_changes_on_focus(self):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        time.sleep(0.5)
        self.allure_before_after('before')
        self.find_elements(*CityPopupLocators.CITIES_RESULT_ITEMS)
        city = self.get_random_element(*CityPopupLocators.CITIES_RESULT_ITEMS, 1)
        color_before = city.value_of_css_property('color')
        self.scroll_to(city)
        time.sleep(0.5)
        self.move_to(city)
        time.sleep(1)
        color_after = city.value_of_css_property('color')
        self.allure_before_after('after')
        assert color_before != color_after, \
            'Текст города не изменил цвет при фокусе на него'

    def the_city_is_changed_by_clicking_on_any_city_from_the_list(self):
        time.sleep(1)
        city_name = self.get_element_text(*MainPageHeaderLocators.CITY_BUTTON)
        self.allure_before_after('before')
        self.go_to_city_popup()
        selected_city = self.get_random_element(*CityPopupLocators.CITIES_RESULT_ITEMS, 1)
        selected_city_name = selected_city.text
        while selected_city_name == city_name:
            selected_city = self.get_random_element(*CityPopupLocators.CITIES_RESULT_ITEMS, 1)
        self.scroll_to(selected_city)
        time.sleep(0.5)
        selected_city.click()
        time.sleep(3)
        self.allure_before_after('after')
        assert self.is_disappeared(*CityPopupLocators.CITY_POPUP_BACKGROUND), \
            'Попап выбора города не закрылся кликом в город из списка'
        new_city_name = self.get_element_text(*MainPageHeaderLocators.CITY_BUTTON)
        assert city_name != new_city_name, 'Город не изменился'
        assert selected_city_name == new_city_name, \
            f'Новый город не соответствует выбранному {city_name} {selected_city_name} {new_city_name}'

    def can_enter_text_in_the_field(self, text):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        self.allure_before_after('before')
        try:
            field.send_keys(text)
            result = True
        except:
            self.allure_before_after('after')
            result = False
        self.allure_before_after('after')
        assert result == True, \
            f'В поле нельзя ввести текст {text}'

    def text_can_be_removed_from_the_field(self):
        # button = self.find_element(*MainPageHeaderLocators.CITY_BUTTON)
        # button.click()
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        field.send_keys('Уфа')
        self.allure_before_after('before')
        try:
            for delete in range(3):
                field.send_keys(Keys.BACKSPACE)
                time.sleep(0.2)
            result = True
            self.allure_before_after('after')
        except:
            result = False
            self.allure_before_after('after')
        assert result == True, \
            'Из поля нельзя удалить текст'

    def hints_with_cities_should_appear(self, text):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        self.allure_before_after('before')
        field.send_keys(text)
        time.sleep(0.5)
        assert self.is_element_present(*CityPopupLocators.CITIES_RESULT_HINTS), \
            'Подсказки не появляются'

    def hints_must_be_relevant(self, text):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        self.allure_before_after('before')
        try:
            field.send_keys(text)
            time.sleep(2)
            hints = self.find_elements(*CityPopupLocators.CITIES_RESULT_ITEMS)
        except:
            self.allure_before_after('after')
        self.allure_before_after('after')
        hints_list = [h.text for h in hints if h.is_displayed()]
        invalid_hint = next((h for h in hints_list if text not in h), None)
        assert invalid_hint is None, \
            f'В подсказке "{invalid_hint}" нет введенных символов "{text}"'

    def focus_on_tooltip_change_text_color(self):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        field.send_keys('Уфа')
        time.sleep(0.5)
        self.allure_before_after('before')
        self.find_elements(*CityPopupLocators.CITIES_RESULT_ITEMS)
        hint = self.get_random_element(*CityPopupLocators.CITIES_RESULT_HINTS, 1)
        color_before = hint.value_of_css_property('color')
        self.scroll_to(hint)
        time.sleep(0.5)
        self.move_to(hint)
        time.sleep(1)
        color_after = hint.value_of_css_property('color')
        self.allure_before_after('after')
        assert color_before != color_after, 'Текст подсказки не изменил цвет при фокусе на него'

    def the_hint_list_scrolls(self):
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        field.send_keys('Оренбург')
        time.sleep(0.5)
        self.allure_before_after('before')
        hints = self.find_elements(*CityPopupLocators.CITIES_RESULT_HINTS)
        coordinates_first_hints_before = hints[0].rect.get('y')
        self.scroll_to(hints[-1])
        time.sleep(0.3)
        coordinates_first_hints_after = hints[0].rect.get('y')
        self.allure_before_after('after')
        self.scroll_to(hints[-0])
        time.sleep(1)
        field.clear()
        assert coordinates_first_hints_before != coordinates_first_hints_after, 'Список подсказок не скроллится'

    def when_click_on_the_hint_the_popup_closes_the_city_corresponds_to_the_selected_one(self):
        city_name = self.get_element_text(*MainPageHeaderLocators.CITY_BUTTON)
        self.allure_before_after('before')
        field = self.find_element(*CityPopupLocators.CITY_INPUT_ARIA)
        field.clear()
        field.send_keys('Оренбург')
        time.sleep(1)
        city = self.find_elements(*CityPopupLocators.CITIES_RESULT_HINTS)[0]
        self.scroll_to(city)
        city.click()
        time.sleep(3)
        self.allure_before_after('after')
        assert self.is_disappeared(*CityPopupLocators.CITY_POPUP_BACKGROUND), \
            'Попап выбора города не закрылся кликом в город из списка подсказок'
        new_city_name = self.get_element_text(*MainPageHeaderLocators.CITY_BUTTON)
        assert city_name != new_city_name, 'Город не изменился'

    def should_be_close_button(self):
        assert self.is_element_present(*CityPopupLocators.CITY_POPUP_CLOSE_BUTTON), \
            'В попапе нет кнопки закрытия "Х"'

    def popup_should_close_click_in_x(self):
        close_button = self.find_element(*CityPopupLocators.CITY_POPUP_CLOSE_BUTTON)
        self.allure_before_after('before')
        close_button.click()
        assert self.is_disappeared(*CityPopupLocators.CITY_POPUP_BACKGROUND), \
            'Попап выбора города не закрылся кликом в "Х"'

    def popup_should_be_closed_by_clicking_outside_the_window(self):
        button = self.find_element(*MainPageHeaderLocators.CITY_BUTTON)
        button.click()
        self.allure_before_after('before')
        back = self.find_element(*CityPopupLocators.CITY_POPUP_BACKGROUND)
        self.move_to(button)
        self.move_by_offset(0, 400)
        self.action_click()
        self.allure_before_after('after')
        assert self.is_disappeared(*CityPopupLocators.CITY_POPUP_BACKGROUND), \
            'Попап выбора города не закрылся кликом за пределами окна'

    def should_be_shops_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.SHOPS_BUTTON), \
            'На странице нет кнопки "Магазины"'

    def clicking_on_the_shops_button_leads_to_the_shops_page(self):
        shops_button = self.find_element(*MainPageHeaderLocators.SHOPS_BUTTON)
        self.allure_before_after('before')
        shops_button.click()
        url = self.browser.current_url
        self.allure_before_after('after')
        assert 'shops' in url, 'Адрес траницы не содержит "shops"'

    def should_be_status_order_button(self):
        assert self.is_element_present(*MainPageLocators.ORDER_STATUS_BUTTON)

    def go_to_popur_authorization(self):
        button = self.find_element(*MainPageHeaderLocators.ORDER_STATUS_BUTTON)
        button.click()
        assert self.is_element_present(*AuthPopupLocators.AUTH_POPUP_BODY), \
            'Попап авторизации не открылся'

    def should_be_title_auth(self):
        assert self.is_element_present(*AuthPopupLocators.AUTH_TITLE)
        title = self.get_element_text(*AuthPopupLocators.AUTH_TITLE)
        assert title == 'Войдите или зарегистрируйтесь, чтобы продолжить'

    def background_should_be_blurred_auth(self):
        background = self.get_style_value(*AuthPopupLocators.AUTH_POPUP_BACKGROUND, 'backdrop-filter')
        self.allure_before_after('after')
        assert 'blur' in background, f'К фону не применен блюр, {background}'
        blur_value = (int(''.join(filter(str.isdigit, background))))
        assert blur_value > 0, f'Значение фильтра равно либо меньше 0, {background}'

    def should_be_button_get_code(self):
        assert self.is_element_present(*AuthPopupLocators.BUTTON_GET_CODE)

    def button_get_code_is_disabled(self):
        button = self.find_element(*AuthPopupLocators.BUTTON_GET_CODE)
        self.is_element_present(*AuthPopupLocators.BUTTON_GET_CODE)
        assert button.is_enabled() == False, 'Кнопка получить код активна при незаполненных полях'

    def should_be_button_login_by_email(self):
        assert self.is_element_present(*AuthPopupLocators.LOGIN_BY_EMAIL_BUTTON, ), \
            'В попапе нет кнопки "Войти по Email"'

    def button_login_by_email_is_enabled(self):
        button = self.find_element(*AuthPopupLocators.LOGIN_BY_EMAIL_BUTTON)
        self.is_element_present(*AuthPopupLocators.LOGIN_BY_EMAIL_BUTTON)
        assert button.is_enabled() == True, 'Кнопка "Войти по Email" не активна'

    def should_be_close_button_auth(self):
        assert self.is_element_present(*AuthPopupLocators.AUTH_POPUP_CLOSE_BUTTON), \
            'В попапе нет кнопки закрытия "Х"'

    def popup_should_close_click_in_x_auth(self):
        close_button = self.find_element(*AuthPopupLocators.AUTH_POPUP_CLOSE_BUTTON)
        self.allure_before_after('before')
        close_button.click()
        assert self.is_disappeared(*AuthPopupLocators.AUTH_POPUP_BACKGROUND), \
            'Попап авторизации не закрылся кликом в "Х"'

    def popup_auth_should_be_closed_by_clicking_outside_the_window(self):
        button_auth = self.find_element(*MainPageHeaderLocators.ORDER_STATUS_BUTTON)
        back = self.find_element(*AuthPopupLocators.AUTH_POPUP_BACKGROUND)
        self.move_to(button_auth)
        self.move_by_offset(0, 400)
        self.action_click()
        assert self.is_disappeared(*AuthPopupLocators.AUTH_POPUP_BODY), \
            'Попап авторизации не закрылся кликом за пределами окна'

    def should_be_a_field_for_entering_phone_number(self):
        assert self.is_element_present(*AuthPopupLocators.PHONE_INPUT), \
            'В попапе нет поля для ввода номера телефона'

    def auth_should_be_placeholder(self):
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        assert self.is_element_present(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER), \
            'Плейсхолдер в поле ввода номера телефона не найден'
        placeholder = self.get_element_text(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER)
        assert placeholder == "Телефон", f'Текст плейсхолдера не соответствует требованиям {placeholder}'

    def auth_placeholder_focus(self):
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER, False)
        self.allure_before_after('before')
        top_value_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER, 'top')
        font_size_value_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER, 'font-size')
        self.find_element(*AuthPopupLocators.PHONE_INPUT).click()
        self.allure_before_after('after')
        top_value_before = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER_FOCUS, 'top')
        font_size_value_before = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER_FOCUS, 'font-size')
        assert (int(''.join(filter(str.isdigit, top_value_after)))) > \
               (int(''.join(filter(str.isdigit, top_value_before)))), 'Плейсхолдер не смещается вверх'
        assert (int(''.join(filter(str.isdigit, font_size_value_after)))) > \
               (int(''.join(filter(str.isdigit, font_size_value_before)))), 'Плейсхолдер не уменьшается'

    def auth_frame_should_change_color(self):
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_BORDER, False)
        self.allure_before_after('before')
        color_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_BORDER, 'border')
        self.find_element(*AuthPopupLocators.PHONE_INPUT).click()
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_BORDER_FOCUS, False)
        self.allure_before_after('after')
        color_befor = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_BORDER_FOCUS, 'border')
        assert color_after != color_befor, 'Рамка поля не подсвечивается'

    def auth_placeholder_defocus(self):
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER_FOCUS, False)
        self.allure_before_after('before')
        top_value_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER_FOCUS, 'top')
        font_size_value_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER_FOCUS, 'font-size')
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        self.allure_before_after('after')
        top_value_before = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER, 'top')
        font_size_value_before = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_PLACEHOLDER, 'font-size')
        assert (int(''.join(filter(str.isdigit, top_value_after)))) < \
               (int(''.join(filter(str.isdigit, top_value_before)))), 'Плейсхолдер не смещается вверх'
        assert (int(''.join(filter(str.isdigit, font_size_value_after)))) < \
               (int(''.join(filter(str.isdigit, font_size_value_before)))), 'Плейсхолдер не уменьшается'

    def auth_field_is_not_highlighted_when_focus_is_removed(self):
        self.find_element(*AuthPopupLocators.PHONE_INPUT).click()
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_BORDER_FOCUS, False)
        self.allure_before_after('before')
        color_after = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_BORDER_FOCUS, 'border')
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        self.is_element_present(*AuthPopupLocators.AUTH_INPUT_BORDER, False)
        self.allure_before_after('after')
        color_before = self.get_style_value(*AuthPopupLocators.AUTH_INPUT_BORDER, 'border')
        assert color_after != color_before, 'Рамка поля подсвечивается'

    def when_enter_any_number_plus_seven_is_automatically_substituted(self, text):
        phone_input = self.find_element(*AuthPopupLocators.PHONE_INPUT)
        phone_input.clear()
        self.allure_before_after('before')
        phone_input.click()
        phone_input.send_keys(text)
        time.sleep(0.5)
        self.allure_before_after('after')
        value = self.find_element(*AuthPopupLocators.PHONE_INPUT).get_attribute('value')
        assert str(value) == '+7(' or '+', 'Автоматическая подстановка +7 не работает'

    def second_character_after_the_plus_is_changed_to_seven(self, text):
        phone_input = self.find_element(*AuthPopupLocators.PHONE_INPUT)
        phone_input.clear()
        self.allure_before_after('before')
        phone_input.click()
        phone_input.send_keys(f'+{text}')
        time.sleep(0.5)
        self.allure_before_after('after')
        value = self.find_element(*AuthPopupLocators.PHONE_INPUT).get_attribute('value')
        assert value == f'+7 ({text}', \
            f'Автоматическая подстановка +7 не работает, при вводе + и любой цифры кроме 7 {value}'

    def the_field_accepts_exactly_12_characters_only_numbers_positive(self, num):
        phone_input = self.find_element(*AuthPopupLocators.PHONE_INPUT)
        phone_input.clear()
        self.allure_before_after('before')
        phone_input.click()
        phone_input.send_keys(f'{num}')
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        value = self.find_element(*AuthPopupLocators.PHONE_INPUT).get_attribute('value')
        value_int = (int(''.join(filter(str.isdigit, value))))
        assert f'+{str(value_int)}' == f'+71234567890', \
            f'В поле отображается другой номер, отображется - +{value_int}\nВвели - {num}'
        assert len(str(value_int)) == 11, f'Длина принятого номера не равна 12 символам'
        assert self.is_disappeared(*AuthPopupLocators.AUTH_ERROR_BORDER, 4, False), 'Рамка поля подсвечена красным'
        assert self.is_disappeared(*AuthPopupLocators.AUTH_ERROR_PLACEHOLDER, 4, False), 'Плейсхолдер подсвечен красным'
        assert self.element_is_displayed(*AuthPopupLocators.AUTH_ERROR_MESSAGE) == False, \
            'Появилось сообщение об ошибке'

    def the_field_accepts_exactly_12_characters_only_numbers_negative(self, num):
        phone_input = self.find_element(*AuthPopupLocators.PHONE_INPUT)
        phone_input.clear()
        self.allure_before_after('before')
        phone_input.click()
        phone_input.send_keys(f'{num}')
        self.find_element(*AuthPopupLocators.AUTH_TITLE).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        value = self.find_element(*AuthPopupLocators.PHONE_INPUT).get_attribute('value')
        value_int = (int(''.join(filter(str.isdigit, value))))
        assert self.is_element_present(*AuthPopupLocators.AUTH_ERROR_BORDER, False), \
            'Рамка поля не подсвечена красным'
        assert self.is_element_present(*AuthPopupLocators.AUTH_ERROR_PLACEHOLDER, False), \
            'Плейсхолдер не подсвечен красным'
        assert self.element_is_displayed(*AuthPopupLocators.AUTH_ERROR_MESSAGE) == True, \
            'Не появилось сообщение об ошибке'

    def auth_should_be_yandex_captcha(self):
        self.browser.switch_to.frame(self.find_element(By.CSS_SELECTOR, 'iframe[frameborder="0"]'))
        assert self.is_element_present(*AuthPopupLocators.AUTH_YANDEX_CAPTCHA), \
            'В попапе авторизации нет капчи'

    def auth_should_be_captcha_checkbox(self):
        assert self.is_element_present(*AuthPopupLocators.AUTH_CAPTCHA_CHECKBOX), 'В блоке капчи нет чек-бокса'

    def auth_can_click_on_the_captcha_checkbox(self):
        self.allure_before_after('before')
        checkbox_before = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        self.find_element(*AuthPopupLocators.AUTH_CAPTCHA_CHECKBOX).click()
        time.sleep(10)
        self.allure_before_after('after')
        checkbox_after = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        assert checkbox_after != checkbox_before, 'Чекбокс не кликабелен'

    def auth_can_not_uncheck_the_checkbox(self):
        self.allure_before_after('before')
        checkbox_before = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        self.find_element(*AuthPopupLocators.AUTH_CAPTCHA_CHECKBOX).click()
        time.sleep(10)
        self.allure_before_after('after')
        checkbox_after = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        assert checkbox_after == checkbox_before, \
            'С чекбокса можно снять отметку'

    def button_get_code_is_enabled(self):
        self.browser.switch_to.parent_frame()
        button = self.find_element(*AuthPopupLocators.BUTTON_GET_CODE)
        self.is_element_present(*AuthPopupLocators.BUTTON_GET_CODE)
        assert button.is_enabled() == True, \
            'Кнопка получить код не активна при отмеченном чекбоксе'

    def when_reopening_the_popup_the_checkbox_is_not_checked(self):
        self.browser.switch_to.frame(self.find_element(By.CSS_SELECTOR, 'iframe[frameborder="0"]'))
        checkbox_before = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        assert checkbox_before == 'false', \
            'Чекбокс остается отмеченным после закрытия попапа'
        self.find_element(*AuthPopupLocators.AUTH_CAPTCHA_CHECKBOX).click()
        time.sleep(10)
        self.allure_before_after('after')
        checkbox_after = self.get_element_attribute(*AuthPopupLocators.AUTH_CHECKBOX_CHECKED, 'data-checked')
        assert checkbox_after != checkbox_before, \
            'Чекбокс не кликабелен, при повторном открытии попапа'

    def when_click_get_code_without_phone_number_an_error_appears(self):
        self.allure_before_after('before')
        self.find_element(*AuthPopupLocators.BUTTON_GET_CODE).click()
        time.sleep(1)
        self.allure_before_after('after')
        assert self.is_element_present(*AuthPopupLocators.AUTH_ERROR_BORDER), \
            'Рамка поля не подсвечена красным'
        assert self.is_element_present(*AuthPopupLocators.AUTH_ERROR_PLACEHOLDER), \
            'Плейсхолдер не подсвечен красным'
        assert self.element_is_displayed(*AuthPopupLocators.AUTH_ERROR_MESSAGE) == True, \
            'Не появилось сообщение об ошибке'

    def when_click_get_the_code_popup_opens_enter_the_code(self, num):
        self.find_element(*AuthPopupLocators.PHONE_INPUT).send_keys(num)
        time.sleep(0.5)
        self.allure_before_after('before')
        self.find_element(*AuthPopupLocators.BUTTON_GET_CODE).click()
        time.sleep(0.5)
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_BODY), \
            'Попап введите код не открывается'

    def should_be_title_enter_code(self):
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_TITLE), \
            'В попапе нет заголовка'

    def should_be_code_entry_field(self):
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_FIELD), \
            "В попапе нет поля ввода кода"

    def should_be_timer(self):
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_TIMER), \
            "В попапе нет таймера повторного запроса кода"

    def resend_code_button_is_disabled(self):
        assert self.find_element(*AuthPopupLocators.ENTER_CODE_TIMER).is_enabled() == False, \
            "Кнопка запроса повторного кода активна"

    def should_be_phone_number_auth(self):
        time.sleep(2)
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_PHONE_NUM), \
            'В попапе нет ранее введенного номера'

    def should_be_button_email_login(self):
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_EMAIL_LOGIN), \
            'В попапе нет кнопки войти по Email'
        assert self.find_element(*AuthPopupLocators.ENTER_CODE_EMAIL_LOGIN).is_enabled() == True, \
            'Кнопка войти по Email не активна'

    def code_entry_field_accepts_four_characters(self, code):
        input_code = self.find_element(*AuthPopupLocators.ENTER_CODE_FIELD)
        input_code.send_keys(code)
        self.find_element(*AuthPopupLocators.ENTER_CODE_TITLE).click()
        time.sleep(0.5)
        assert self.is_element_present(*AuthPopupLocators.ENTER_CODE_ERROR), \
            'Не появилось сообщение об ошибке'

    def clicking_on_the_change_button_returns_to_entering_the_number(self):
        self.find_element(*AuthPopupLocators.ENTER_CODE_CHANGE_BUTTON).click()
        assert self.is_element_present(*AuthPopupLocators.PHONE_INPUT), \
            'Не вернулся к вводу номера телефона'
        number = self.get_element_attribute(*AuthPopupLocators.PHONE_INPUT, 'value')
        assert (int(''.join(filter(str.isdigit, number)))) == 79080000909, \
            'Номер не соответствует введенному'

    def clicking_on_the_login_by_email_button_opens_an_popup_with_an_email_input_field(self):
        time.sleep(2)
        self.find_element(*AuthPopupLocators.ENTER_CODE_EMAIL_LOGIN).click()
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_EMAIL_INPUT), \
            'В попапе нет поля ввода Email'

    def new_user_has_the_consent_checkbox(self):
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_CHECKBOX), \
            'В попапе нет чек-бокса'
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_AGREEMENT, False), \
            'В попапе нет кнопки "Согласие"'
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_POLICY, False), \
            'В попапе нет кнопки "Политика"'

    def clicking_on_the_consent_opens_the_data_processing_agreement(self):
        self.find_element(*AuthPopupLocators.NEW_USER_AGREEMENT).click()
        assert self.is_element_present(*AuthPopupLocators.AGREEMENT_TITLE, False), \
            'Попап соглашение на обработку данных не открылся'
        title = self.get_element_text(*AuthPopupLocators.AGREEMENT_TITLE)
        assert title == 'Согласие на обработку персональных данных', \
            'Заголовок не соответствует макетам'

    def should_be_back_link(self):
        assert self.is_element_present(*AuthPopupLocators.BACK_LINK), \
            'В соглашении на обработку данных нет кнопки назад'

    # def should_be_close_button(self):
    #     assert self.is_element_present(*AuthPopupLocators.AUTH_POPUP_CLOSE_BUTTON, False), \
    #         'В попапе нет кнопки закрыть'

    def back_button_returns_to_the_code_entry_popup(self):
        self.find_element(*AuthPopupLocators.BACK_LINK).click()
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_POPUP_BODY), \
            'Клик в кнопку "Назад" не возвращает в попап ввода кода'

    def click_in_policy_opens_the_data_processing_policy(self):
        self.find_element(*AuthPopupLocators.NEW_USER_POLICY).click()
        assert self.is_element_present(*AuthPopupLocators.POLICY_TITLE), \
            'Клик в кнопку назад не возвращает в попап ввода кода'

    def policy_should_be_back_link(self):
        assert self.is_element_present(*AuthPopupLocators.BACK_LINK_POLICY), \
            "В политике обработке данных есть кнопка назад"

    def policy_back_button_returns_to_the_code_entry_popup(self):
        self.find_element(*AuthPopupLocators.BACK_LINK_POLICY).click()
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_POPUP_BODY), \
            'Клик в кнопку назад, не возвращает в попап ввода кода'

    def should_be_registration_button(self):
        assert self.is_element_present(*AuthPopupLocators.REGISTRATION_BUTTON), \
            'В попапе нет кнопки регистрация'

    def error_when_clicking_in_the_checkbox_without_code(self):
        box = self.find_element(*AuthPopupLocators.NEW_USER_CHECKBOX)
        self.allure_before_after('before')
        self.browser.execute_script("arguments[0].click();", box)
        time.sleep(1)
        self.allure_before_after('after')
        border = self.get_style_value(*AuthPopupLocators.NEW_USER_ERROR_BORDER, 'border')
        button = self.find_element(*AuthPopupLocators.REGISTRATION_BUTTON_INACTIV)
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_ERROR_BORDER, False), \
            'Рамка поля не подсвечена'
        assert border == '2px solid rgb(255, 0, 0)', \
            'Цвет рамки не красный'
        assert self.is_element_present(*AuthPopupLocators.NEW_USER_ERROR_MSG), \
            'Не появилось сообщение об ошибке'
        assert button.is_enabled() == False, \
            'Кнопка "Зарегистрироваться" активна'

    # !!!!!!!!!!!!!!!!ПРОДОЛЖЕНИЕ АВТОРИЗАЦИИ!!!!!!!!!!!

    def should_be_phone_number(self):
        assert self.is_element_present(*MainPageHeaderLocators.PHONE_NUMBER), \
            "В хедере нет номера телефона"

    def should_be_logo(self):
        assert self.is_element_present(*MainPageHeaderLocators.LOGO), \
            'На странице нет логотипа'

    def click_on_the_logo_to_open_the_home_page(self, link):
        self.find_element(*MainPageHeaderLocators.LOGO).click()
        url = self.browser.current_url
        self.allure_before_after('after')
        assert url == link, \
            f"Клик в логотип открывает не главную страницу {url}"

    def should_be_catalog_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.CATALOG_BUTTON), \
            'На странице нет кнопки каталог'

    def go_to_catalog_popup(self):
        self.find_element(*MainPageHeaderLocators.CATALOG_BUTTON).click()
        time.sleep(0.3)
        assert self.is_element_present(*MainPageHeaderLocators.CATALOG_BODY), \
            'Каталог не открылся'
        assert self.is_element_present(*MainPageHeaderLocators.CATALOG_CATEGORIES), \
            'В каталоге нет категорий'

    def burger_changes_to_a_cross(self):
        first_before = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON_ICON_FIRST, 'transform')
        third_before = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON_ICON_THIRD, 'transform')
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.CATALOG_BUTTON).click()
        time.sleep(0.3)
        first_after = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON_ICON_FIRST, 'transform')
        third_after = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON_ICON_THIRD, 'transform')
        self.allure_before_after('after')
        assert first_before != first_after, \
            'Бургер не изменился на крестик'
        assert third_before != third_after, \
            'Бургер не изменился на крестик'

    def clicking_the_catalog_button_again_closes_the_catalog_window(self):
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.CATALOG_BUTTON).click()
        assert self.is_disappeared(*MainPageHeaderLocators.CATALOG_BODY), \
            'Окно каталога не закрылось'

    def catalog_button_is_highlighted_when_focused(self):
        button = self.find_element(*MainPageHeaderLocators.CATALOG_BUTTON)
        button_before = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON, 'background')
        self.allure_before_after('before')
        self.move_to(button)
        time.sleep(0.2)
        button_after = self.get_style_value(*MainPageHeaderLocators.CATALOG_BUTTON_HOVER, 'background')
        self.allure_before_after('after')
        assert button_before != button_after, \
            'Кнопка каталог не подсвечивается при фокусе'

    def should_be_search_field(self):
        self.is_element_present(*MainPageHeaderLocators.FIELD_SEARCH), \
            'На странице нет поля поиска'

    def search_frame_is_highlighted_when_focused(self):
        border_before = self.get_style_value(*MainPageHeaderLocators.SEARCH_BORDER, 'border')
        icon_before = self.get_style_value(*MainPageHeaderLocators.SEARCH_ICON, 'fill')
        search = self.find_element(*MainPageHeaderLocators.FIELD_SEARCH)
        self.allure_before_after('before')
        self.move_to(search)
        time.sleep(1)
        self.allure_before_after('after')
        border_after = self.get_style_value(*MainPageHeaderLocators.SEARCH_BORDER_FOCUS, 'border')
        icon_after = self.get_style_value(*MainPageHeaderLocators.SEARCH_ICON_FOCUS, 'fill')
        assert border_after != border_before, \
            'Рамка поиска не подсвечивается при фокусе на поле'
        assert icon_before != icon_after, \
            'Иконка поиска(лупа) не меняет цвет при фокусе на поле'

    def should_be_placeholder(self):
        self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        placeholder = self.get_element_attribute(*MainPageHeaderLocators.SEARCH_INPUT, 'placeholder')
        self.allure_find_element('Элемент найден')
        assert len(placeholder) > 0, 'Плейсхолдера нет в поле'
        assert placeholder == 'Найти дрель', f'Плейсхолдер не соответствует макетам {placeholder}'

    def go_to_search_popup(self):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.click()
        time.sleep(0.5)
        assert self.is_element_present(*MainPageHeaderLocators.SEARCH_POPUP), \
            'Попап поиска не открылся'

    def there_are_sections_in_popup_often_searched_and_popular_products(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.FREQUENTLY_SEARCHED), \
            'В попапе нет блока "Часто ищут"'
        assert self.is_element_present(*MainPageSearchPopupLocators.POPULAR_GOODS), \
            'В попапе нет блока популярные товары'

    def must_be_product(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.POPULAR_GOODS_PRODUCTS), \
            'В блоке популярные товары нет товаров'

    def should_be_item_title(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.ITEM_TITLE), \
            'У товаров нет названия'

    def should_be_item_price(self):
        price = self.find_element(*MainPageSearchPopupLocators.ITEM_PRICE)
        if not price:
            assert self.is_element_present(*MainPageSearchPopupLocators.ITEM_PRICE_NEW), \
                'У товара нет новой цена(красный ценник)'
            assert self.is_element_present(*MainPageSearchPopupLocators.ITEM_PRICE_OLD), \
                'У товара со скидкой, не указана старая цена'
        else:
            assert self.is_element_present(*MainPageSearchPopupLocators.ITEM_PRICE), \
                'У товаров нет цены'

    def should_be_photo(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.ITEM_PHOTO), \
            'У товара нет фотографии'

    def should_be_add_to_cart_button(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_BUTTON), \
            'У товаров нет кнопки добавить в корзину'

    def button_changes_color_on_focus(self):
        button_before = self.get_style_value(*MainPageSearchPopupLocators.ADD_CART_BUTTON, 'background')
        button = self.find_element(*MainPageSearchPopupLocators.ADD_CART_BUTTON)
        self.allure_before_after('before')
        self.move_to(button)
        time.sleep(1)
        self.allure_before_after('after')
        button_after = self.get_style_value(*MainPageSearchPopupLocators.ADD_CART_BUTTON_FOCUS, 'background')
        assert button_after != button_before, \
            'Кнопка добавления в корзину не подсвечивается при фокусе'

    def click_the_add_to_cart_button(self):
        add_cart_btn = self.find_element(*MainPageSearchPopupLocators.ADD_CART_BUTTON)
        add_cart_btn.click()
        time.sleep(1)
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def counter_on_cart_icon(self):
        assert self.is_element_present(*MainPageHeaderLocators.CART_COUNTER), \
            'Счетчик товаров в корзине не появился'
        count = self.get_element_text(*MainPageHeaderLocators.CART_COUNTER)
        assert count == '1', f'Количество товаров в корзине не равно 1 {count}'

    def added_product_is_displayed_on_the_shopping_cart_page(self):
        items = self.find_elements(*MainPageSearchPopupLocators.POPULAR_GOODS_PRODUCTS)
        item = items[0]
        title = item.find_element(*MainPageSearchPopupLocators.ITEM_TITLE).text
        try:
            price = item.find_element(*MainPageSearchPopupLocators.ITEM_PRICE)
        except NoSuchElementException:
            price = False
        if not price:
            price = item.find_element(*MainPageSearchPopupLocators.ITEM_PRICE_NEW).text.partition('/')[0]
        else:
            price = item.find_element(*MainPageSearchPopupLocators.ITEM_PRICE).text.partition('/')[0]
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_element_present(*BasketPageLocators.ITEM), \
            'В корзине нет товаров'
        assert self.is_element_present(*BasketPageLocators.ITEM_TITLE), \
            'У товара нет названия'
        assert self.is_element_present(*BasketPageLocators.ITEM_PRICE), \
            'У товара нет цены'
        basket_item_title = self.get_element_text(*BasketPageLocators.ITEM_TITLE)
        basket_item_price = self.get_element_text(*BasketPageLocators.ITEM_PRICE)
        assert basket_item_price == price, \
            f'Цена товара в корзине отличается от цены в попапе поиска, поиск - {price} корзина - {basket_item_price}'
        assert basket_item_title == title, \
            f'Название товара в корзине отличается от названия в поиске, ' \
            f'поиск - {title} корзина - {basket_item_title}'

    def quantity_change_buttons_not_closing(self):
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def click_on_the_plus_button_adds_one_item(self):
        self.allure_before_after('before')
        self.find_element(*MainPageSearchPopupLocators.ADD_CART_PLUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*MainPageSearchPopupLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 2, \
            'Счетчик товаров не изменился после клика в "+"'

    def clicking_the_minus_button_takes_away_one_product(self):
        self.allure_before_after('before')
        self.find_element(*MainPageSearchPopupLocators.ADD_CART_MINUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*MainPageSearchPopupLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 1, \
            'Счетчик товаров не изменился после клика в "-"'

    def clicking_the_minus_button_removes_the_product(self):
        self.allure_before_after('before')
        self.find_element(*MainPageSearchPopupLocators.ADD_CART_MINUS).click()
        time.sleep(1)
        self.allure_before_after('after')
        assert self.is_disappeared(*MainPageSearchPopupLocators.ADD_CART_MINUS), \
            'Кнопки "+" и "-" не пропали'
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_BUTTON), \
            'Кнопка добавить в корзину не появилась'

    def product_is_not_on_the_shopping_cart_page(self):
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_disappeared(*BasketPageLocators.ITEM), 'Товар не удалился со страницы корзина'

    def plus_and_minus_buttons_do_not_disappear_when_adding_new_product_to_cart(self):
        self.find_element(*MainPageSearchPopupLocators.ADD_CART_BUTTON).click()
        time.sleep(0.5)
        self.find_element(*MainPageSearchPopupLocators.ADD_CART_BUTTON).click()
        buttons = self.find_elements(*MainPageSearchPopupLocators.ADD_CART_MINUS)
        assert self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_MINUS), \
            'Кнопки "+" и "-" отсутствуют в попапе поиска'
        assert len(buttons) == 2, \
            'Кнопки "+" и "-" закрылись'

    def clicking_on_product_in_popular_products_section_leads_to_product_card(self):
        items = self.find_elements(*MainPageSearchPopupLocators.POPULAR_GOODS_PRODUCTS)
        item = items[0]
        item_title = item.find_element(*MainPageSearchPopupLocators.ITEM_TITLE).text
        try:
            item_price = item.find_element(*MainPageSearchPopupLocators.ITEM_PRICE).text
        except NoSuchElementException:
            item_price = item.find_element(*MainPageSearchPopupLocators.ITEM_PRICE_NEW).text
        item.find_element(*MainPageSearchPopupLocators.ITEM_TITLE).click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        product_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert item_title == product_title, \
            'Карточка товара не релевантна товары в поиске'
        assert item_price.partition('/')[0] == product_price.partition('/')[0], \
            f'Цена в карточке товара не соответствует цене в поиске карточка - {product_price} поиск - {item_price}'

    def quantity_of_added_product_in_product_card_corresponds_to_quantity_in_search(self):
        self.is_element_present(*ProductPageLocators.PRODUCT_COUNT_INPUT)
        count_product_page = self.get_element_attribute(*ProductPageLocators.PRODUCT_COUNT_INPUT, 'value')
        self.open()
        self.go_to_search_popup()
        self.is_element_present(*MainPageSearchPopupLocators.ADD_CART_NUMBER)
        count_search_popup = self.get_element_attribute(*MainPageSearchPopupLocators.ADD_CART_NUMBER, 'value')
        assert count_search_popup == count_product_page, \
            'Количество добавленных в поиске, товаров в корзину, не совпадает с количеством в карточке товара'
        minus_buttons = self.find_elements(*MainPageSearchPopupLocators.ADD_CART_MINUS)
        for m in minus_buttons:
            m.click()
            time.sleep(0.5)

    def should_be_previously_you_searched(self):
        search = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search.send_keys('ламинат')
        self.find_element(*MainPageHeaderLocators.SEARCH_BUTTON).click()
        self.open()
        self.go_to_search_popup()
        assert self.is_element_present(*MainPageSearchPopupLocators.SEARCH_HISTORY), \
            'Нет блока история поиска'
        assert self.is_element_present(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS), \
            'В истории поиска нет запросов'

    def click_to_search_history(self):
        search_item_text = self.get_element_text(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS)
        self.find_element(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS).click()
        time.sleep(1)
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        item_listing_page = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert search_item_text == item_listing_page, \
            'Поисковая выдача не релевантна запросу'

    def click_to_frequently_searched_item(self):
        frequently_searched_item = self.get_element_text(*MainPageSearchPopupLocators.FREQUENTLY_SEARCHED_ITEMS)
        self.find_element(*MainPageSearchPopupLocators.FREQUENTLY_SEARCHED_ITEMS).click()
        time.sleep(1)
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        item_listing_page = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert frequently_searched_item == item_listing_page, \
            'Поисковая выдача не релевантна запросу'

    def search_history_item_changes_color_when_focused(self):
        self.allure_before_after('before')
        item = self.find_element(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS)
        color_before = self.get_style_value(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS, 'color')
        self.move_to(item)
        time.sleep(1)
        self.allure_before_after('after')
        color_after = self.get_style_value(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS_FOCUS, 'color')
        assert color_before != color_after, \
            'Цвет пункта истории поиска, не меняется при фокусе'

    def each_search_history_item_has_a_delete_button(self):
        self.is_element_present(*MainPageSearchPopupLocators.DELETE_SEARCH_HISTORY_ITEM)
        items = len(self.find_elements(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS))
        delete_button = len(self.find_elements(*MainPageSearchPopupLocators.DELETE_SEARCH_HISTORY_ITEM))
        assert items == delete_button, \
            'Не у всех запросов есть кнопка удаления'

    def clicking_on_the_delete_button_removes_the_query_from_the_search_history(self):
        self.allure_before_after('before')
        count_item_before = len(self.find_elements(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS))
        self.find_element(*MainPageSearchPopupLocators.DELETE_SEARCH_HISTORY_ITEM).click()
        time.sleep(1)
        self.allure_before_after('after')
        try:
            count_item_after = len(self.find_elements(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS))
        except NoSuchElementException:
            count_item_after = 0
        assert count_item_before - 1 == count_item_after, \
            f'Запрос не удалился, запросов до {count_item_before} количество запросов после {count_item_after}'

    def if_delete_all_requests_the_section_is_not_displayed_in_popup(self):
        self.allure_before_after('before')
        count_item_before = len(self.find_elements(*MainPageSearchPopupLocators.SEARCH_HISTORY_ITEMS))
        for c in range(count_item_before):
            self.find_element(*MainPageSearchPopupLocators.DELETE_SEARCH_HISTORY_ITEM).click()
        self.allure_before_after('after')
        assert self.element_is_displayed(*MainPageSearchPopupLocators.SEARCH_HISTORY) == False, \
            'Раздел "Ранее вы искали" не пропадает при удалении всех запросов'

    def can_enter_text_in_the_search_field(self, text):
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.SEARCH_INPUT).send_keys(text)
        time.sleep(0.5)
        self.allure_before_after('after')
        assert self.is_element_present(*MainPageHeaderLocators.CLEAR_SEARCH_INPUT_BTN, False), \
            f'Не удалось ввести текст {text} в поле поиска'

    def often_searches_change_with_search_query(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        self.allure_before_after('before')
        search_input.send_keys(text)
        time.sleep(1)
        self.allure_before_after('after')
        items = self.find_elements(*MainPageSearchPopupLocators.FREQUENTLY_SEARCHED)
        for i in items:
            item = i.text
            assert text.casefold() in item.casefold(), \
                f'Выдача - {item} в блоке "Часто ищут" не релевантна запросу - {text}'

    def popular_products_change_with_search_query(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        self.allure_before_after('before')
        search_input.send_keys(text)
        time.sleep(2)
        self.allure_before_after('after')
        items = self.find_elements(*MainPageSearchPopupLocators.POPULAR_GOODS_PRODUCTS)
        for i in items:
            item = i.text
            assert text.casefold() in item.casefold(), \
                f'Выдача - {item} в блоке "Популярные товары" не релевантна запросу - {text}'

    def category_block_appears(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        self.allure_before_after('before')
        search_input.send_keys(text)
        time.sleep(1)
        assert self.is_element_present(*MainPageSearchPopupLocators.SEARCH_CATEGORIES), \
            'Блок категории не появился'
        assert self.is_element_present(*MainPageSearchPopupLocators.SHOW_ALL_RESULTS), \
            'Кнопка "Все результаты" не появилась'

    def output_in_category_block_is_relevant_to_query(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        self.allure_before_after('before')
        search_input.send_keys(text)
        time.sleep(1)
        self.allure_before_after('after')
        items = self.find_elements(*MainPageSearchPopupLocators.SEARCH_CATEGORIES_ITEMS)
        for i in items:
            item = i.text
            assert text.casefold() in item.casefold(), \
                f'Выдача - {item} в блоке "Категории" не релевантна запросу - {text}'

    def close_and_find_buttons_appear_in_the_search_bar(self):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        self.allure_before_after('before')
        search_input.send_keys('Ламинат')
        time.sleep(1)
        self.allure_before_after('after')
        assert self.is_element_present(*MainPageHeaderLocators.CLEAR_SEARCH_INPUT_BTN), \
            'Кнопка очистки поля поиска не появилась'
        assert self.is_element_present(*MainPageHeaderLocators.SEARCH_BUTTON), \
            'Кнопка "Найти" не появилась в строке поиска'

    def find_button_is_highlighted_when_focused(self):
        self.allure_before_after('before')
        button = self.find_element(*MainPageHeaderLocators.SEARCH_BUTTON)
        color_before = self.get_style_value(*MainPageHeaderLocators.SEARCH_BUTTON, 'background')
        self.move_to(button)
        time.sleep(1)
        self.allure_before_after('after')
        color_after = self.get_style_value(*MainPageHeaderLocators.SEARCH_BUTTON_FOCUS, 'background')
        assert color_before != color_after, \
            'Цвет кнопку "Найти" не меняется при фокусе на кнопку'

    def field_is_cleared_by_clicking_on_x_button(self):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.clear()
        search_input.send_keys('Ламинат')
        time.sleep(1)
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.CLEAR_SEARCH_INPUT_BTN).click()
        time.sleep(1)
        self.allure_before_after('after')
        assert self.element_is_displayed(*MainPageHeaderLocators.CLEAR_SEARCH_INPUT_BTN) is False, \
            'Кнопка очистки поля не исчезла'
        assert self.is_element_present(*MainPageHeaderLocators.EMPTY_FIELD, False), \
            'Поле не очищено после клика в кнопку "Х"'

    def clicking_on_find_button_opens_search_results(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.click()
        search_input.clear()
        search_input.send_keys(text)
        time.sleep(1)
        self.find_element(*MainPageHeaderLocators.SEARCH_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        search_result = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert search_result.casefold() == text.casefold(), \
            f'Поисковая выдача - {search_result} не релевантна запросу - {text}'

    def clicking_on_button_all_results_opens_search_results(self):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.click()
        search_input.clear()
        text = 'Ламинат'
        search_input.send_keys(text)
        time.sleep(2)
        self.find_element(*MainPageSearchPopupLocators.SHOW_ALL_RESULTS).click()
        time.sleep(2)
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        search_result = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert search_result.casefold() == text.casefold(), \
            f'Поисковая выдача - {search_result} не релевантна запросу - {text}'

    def clicking_on_any_category_opens_search_results(self):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.click()
        search_input.clear()
        search_input.send_keys('Ламинат')
        time.sleep(1)
        category = self.get_random_element(*MainPageSearchPopupLocators.SEARCH_CATEGORIES_ITEMS, 1)
        category_name = category.text
        category.click()
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        search_result = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert search_result.casefold() == category_name.casefold(), \
            f'Поисковая выдача - {search_result} не релевантна запросу - {category_name}'

    def processed_requests_with_a_typo(self, text):
        search_input = self.find_element(*MainPageHeaderLocators.SEARCH_INPUT)
        search_input.click()
        search_input.clear()
        search_input.send_keys(text)
        time.sleep(1)
        self.find_element(*MainPageHeaderLocators.SEARCH_BUTTON).click()
        self.is_element_present(*SearchListingPage.SEARCH_KEY)
        search_result = self.get_element_text(*SearchListingPage.SEARCH_KEY)
        assert search_result.casefold() == "Ламинат".casefold(), \
            f'Поисковая выдача - {search_result} не релевантна запросу - {text}'

    def popup_is_closed_by_clicking_outside_it(self):
        button = self.find_element(*MainPageHeaderLocators.CITY_BUTTON)
        self.move_to(button)
        self.move_by_offset(0, 600)
        self.action_click()
        assert self.element_is_displayed(*MainPageHeaderLocators.SEARCH_POPUP) == False, \
            'Попап поиска не закрылся'

    def should_be_actions_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.ACTIONS_BUTTON), \
            'В хедере нет кнопки акции'

    def click_in_button_actions_open_actions_page(self):
        button = self.find_element(*MainPageHeaderLocators.ACTIONS_BUTTON)
        button.click()
        url = self.browser.current_url
        assert 'actions' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*ActionsPage.TITLE), \
            'На странице нет заголовка'

    def should_be_enter_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.ENTER_BUTTON), \
            'В хедере нет кнопки вход'

    def click_in_button_enter_open_auth_popup(self):
        button = self.find_element(*MainPageHeaderLocators.ENTER_BUTTON)
        button.click()
        assert self.is_element_present(*AuthPopupLocators.AUTH_POPUP_BODY), \
            'Попап авторизации не открылся'

    def should_be_compare_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.COMPARE_BUTTON), \
            'В хедере нет кнопки "Сравнение"'

    def clicking_on_button_compare_opens_the_comparison_page(self):
        button = self.find_element(*MainPageHeaderLocators.COMPARE_BUTTON)
        button.click()
        time.sleep(1)
        url = self.browser.current_url
        assert 'compare' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*ComparePage.TITLE), \
            'На странице нет заголовка'

    def should_be_favourites_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.FAVOURITES_BUTTON), \
            'В хедере нет кнопки "Избранное"'

    def clicking_on_button_favourites_opens_page_waiting_products(self):
        button = self.find_element(*MainPageHeaderLocators.FAVOURITES_BUTTON)
        button.click()
        time.sleep(1)
        url = self.browser.current_url
        assert 'favorites' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*FavouritesPage.TITLE), \
            'На странице нет заголовка'

    def should_be_basket_button(self):
        assert self.is_element_present(*MainPageHeaderLocators.BASKET_BUTTON), \
            'В хедере нет кнопки "Корзина"'

    def clicking_on_button_basket_opens_basket_page(self):
        button = self.find_element(*MainPageHeaderLocators.BASKET_BUTTON)
        button.click()
        time.sleep(1)
        url = self.browser.current_url
        assert 'cart' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*BasketPageLocators.TITLE), \
            'На странице нет заголовка'

    def should_be_header_banner(self):
        assert self.is_element_present(*MainPageHeaderLocators.HEADER_BANNER), \
            'Над хедером нет баннера'

    def click_in_header_banner_open_action_page(self):
        button = self.find_element(*MainPageHeaderLocators.HEADER_BANNER)
        button.click()
        url = self.browser.current_url
        assert 'action' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*ActionsPage.TITLE), \
            'На странице нет заголовка'

    def should_be_main_banner(self):
        assert self.is_element_present(*MainPageLocators.MAIN_BANNER), \
            'На странице нет главного баннера'

    def should_be_main_banner_navigation(self):
        assert self.is_element_present(*MainBannerLocators.MAIN_NAVIGATION), \
            'У баннера нет блока управления слайдами'

    def should_be_prev_button_navigation(self):
        assert self.is_element_present(*MainBannerLocators.PREV_BUTTON), \
            'В блоке управления слайдами, нет кнопки переключения назад'

    def should_be_next_button_navigation(self):
        assert self.is_element_present(*MainBannerLocators.NEXT_BUTTON), \
            'В блоке управления слайдами, нет кнопки переключения вперед'

    def should_be_current_value_main_banner(self):
        assert self.is_element_present(*MainBannerLocators.CURRENT_VALUE), \
            'В блоке управления слайдами не отображается номер текущего слайда'

    def should_be_total_value_main_banner(self):
        assert self.is_element_present(*MainBannerLocators.TOTAL_VALUE), \
            'В блоке управления слайдами не отображается общее количество слайдов'

    def slides_can_be_switched_back(self):
        prev_button = self.find_element(*MainBannerLocators.PREV_BUTTON)
        idx_activ_slide_before = self.get_element_attribute(*MainBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_before = self.get_element_text(*MainBannerLocators.CURRENT_VALUE)
        self.allure_before_after('before')
        prev_button.click()
        time.sleep(0.3)
        idx_activ_slide_after = self.get_element_attribute(*MainBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_after = self.get_element_text(*MainBannerLocators.CURRENT_VALUE)
        self.allure_before_after('after')
        assert value_before != value_after, \
            'Номер активного слайда не изменился'
        assert idx_activ_slide_before != idx_activ_slide_after, \
            'Индекс активного слайда не изменился'

    def slides_can_be_switched_forward(self):
        prev_button = self.find_element(*MainBannerLocators.NEXT_BUTTON)
        time.sleep(1)
        idx_activ_slide_before = self.get_element_attribute(*MainBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_before = self.get_element_text(*MainBannerLocators.CURRENT_VALUE)
        self.allure_before_after('before')
        prev_button.click()
        time.sleep(0.3)
        idx_activ_slide_after = self.get_element_attribute(*MainBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_after = self.get_element_text(*MainBannerLocators.CURRENT_VALUE)
        self.allure_before_after('after')
        assert value_before != value_after, \
            'Номер активного слайда не изменился'
        assert idx_activ_slide_before != idx_activ_slide_after, \
            'Индекс активного слайда не изменился'

    def number_of_slides_is_equal_to_number_in_counter(self):
        count = []
        total_counter = int(self.get_element_text(*MainBannerLocators.TOTAL_VALUE))
        slides = self.find_elements(*MainBannerLocators.SLIDES)
        for s in slides:
            count.append(s.get_attribute("data-swiper-slide-index"))
        total_slides = len(set(count))
        assert total_counter == total_slides, f'{total_counter} {total_slides}'

    def slides_in_the_main_banner_are_clickable(self):
        active_slide = self.find_element(*MainBannerLocators.ACTIVE_SLIDE)
        # self.scroll_to(active_slide)
        active_slide.click()
        url = self.browser.current_url
        assert 'action' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*ActionsPage.TITLE), \
            'На странице нет заголовка'

    def should_be_goods_of_the_day(self):
        assert self.is_element_present(*MainPageLocators.ITEMS_BANNER), \
            'На странице нет блока "Товар дня"'

    def should_be_item_of_day_slides(self):
        assert self.is_element_present(*ItemsOfDayLocators.ITEM_OF_DAY), \
            'В блоке нет слайдов'

    def should_be_next_button_navigation_item_of_day(self):
        assert self.is_element_present(*ItemsOfDayLocators.NEXT_BUTTON), \
            'В блоке нет кнопки переключения слайда к следующему'

    def there_is_no_back_button_on_the_first_slide(self):
        self.allure_find_element('Скрин')
        assert False == self.find_element(*ItemsOfDayLocators.PREV_BUTTON).is_displayed(), \
            'На первом слайде есть кнопка переключения к предыдущему слайду'

    def the_slide_switches_to_the_next(self):
        self.is_element_present(*ItemsOfDayLocators.ACTIV_SLIDE)
        id_before = self.get_element_attribute(*ItemsOfDayLocators.ACTIV_SLIDE, 'data-id')
        self.find_element(*ItemsOfDayLocators.NEXT_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*ItemsOfDayLocators.ACTIV_SLIDE)
        id_after = self.get_element_attribute(*ItemsOfDayLocators.ACTIV_SLIDE, 'data-id')
        assert id_before != id_after, \
            'Слайд не переключился кликом в кнопку "следующий"'

    def back_button_appears_on_the_second_slide(self):
        self.is_element_present(*ItemsOfDayLocators.PREV_BUTTON)
        assert self.find_element(*ItemsOfDayLocators.PREV_BUTTON).is_displayed(), \
            'Кнопка переключения слайдов к предыдущему не появилась'

    def the_slide_switches_to_the_prev(self):
        self.is_element_present(*ItemsOfDayLocators.ACTIV_SLIDE)
        id_before = self.get_element_attribute(*ItemsOfDayLocators.ACTIV_SLIDE, 'data-id')
        self.find_element(*ItemsOfDayLocators.PREV_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*ItemsOfDayLocators.ACTIV_SLIDE)
        id_after = self.get_element_attribute(*ItemsOfDayLocators.ACTIV_SLIDE, 'data-id')
        assert id_before != id_after, \
            'Слайд не переключился кликом в кнопку "предыдущий"'

    def next_button_missing(self):
        items_count = len(self.find_elements(*ItemsOfDayLocators.ITEMS))
        self.allure_before_after('before')
        for i in range(items_count - 1):
            self.find_element(*ItemsOfDayLocators.NEXT_BUTTON).click()
            time.sleep(0.5)
        self.allure_before_after('after')
        assert False == self.find_element(*ItemsOfDayLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не пропала'

    def next_button_appears(self):
        items_count = len(self.find_elements(*ItemsOfDayLocators.ITEMS))
        self.allure_before_after('before')
        for i in range(items_count - 1):
            self.find_element(*ItemsOfDayLocators.PREV_BUTTON).click()
            time.sleep(0.5)
        self.is_element_present(*ItemsOfDayLocators.NEXT_BUTTON)
        assert True == self.find_element(*ItemsOfDayLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не появилась'

    def slides_products_of_the_day_are_clickable(self):
        slide = self.find_element(*ItemsOfDayLocators.ACTIV_SLIDE)
        self.is_element_present(*ItemsOfDayLocators.ITEM_TITLE)
        slide_name = self.get_element_text(*ItemsOfDayLocators.ITEM_TITLE)
        slide.click()
        time.sleep(1)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        assert slide_name == product_title, \
            'Карточка товара не релевантна слайду'

    def should_be_block_title(self):
        assert self.is_element_present(*ItemsOfDayLocators.BLOCK_TITLE), \
            'В блоке нет заголовка'

    def should_be_items_timer(self):
        assert self.is_element_present(*ItemsOfDayLocators.BLOCK_TIMER), \
            'У слайда нет таймера окончания акции'

    def should_be_a_badge(self):
        assert self.is_element_present(*ItemsOfDayLocators.BLOCK_PROMO), \
            'У слайда нет шильдиков'

    def should_be_photo_items(self):
        assert self.is_element_present(*ItemsOfDayLocators.ITEMS_PHOTO), \
            'У слайда нет фотографии'

    def should_be_name_item(self):
        assert self.is_element_present(*ItemsOfDayLocators.ITEM_TITLE), \
            'На слайде нет названия товара'

    def should_be_price(self):
        assert self.is_element_present(*ItemsOfDayLocators.ITEMS_PRICE), \
            'На слайде нет цены товара'

    def should_be_add_cart_btn(self):
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_BTN), \
            'На слайде нет цены товара'

    def button_add_cart_lights_up_when_focused(self):
        color_before = self.get_style_value(*ItemsOfDayLocators.ADD_CART_BTN, 'background').partition(')')[0]
        btn = self.find_element(*ItemsOfDayLocators.ADD_CART_BTN)
        self.allure_before_after('before')
        self.move_to(btn)
        time.sleep(0.5)
        self.allure_before_after('after')
        color_after = self.get_style_value(*ItemsOfDayLocators.ADD_CART_BTN, 'background').partition(')')[0]
        assert color_before != color_after

    def click_the_add_to_cart_button_items_of_day(self):
        add_cart_btn = self.find_element(*ItemsOfDayLocators.ADD_CART_BTN)
        add_cart_btn.click()
        time.sleep(1)
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def counter_on_cart_icon_items_of_day(self):
        assert self.is_element_present(*MainPageHeaderLocators.CART_COUNTER), \
            'Счетчик товаров в иконке корзины в хедере, не появился'
        count = self.get_element_text(*MainPageHeaderLocators.CART_COUNTER)
        assert count == '1', f'Количество товаров в корзине не равно 1 {count}'

    def added_product_is_displayed_on_the_shopping_cart_page_items_of_day(self):
        item = self.find_element(*ItemsOfDayLocators.ACTIV_SLIDE)
        title = item.find_element(*ItemsOfDayLocators.ITEM_TITLE).text
        try:
            price = item.find_element(*ItemsOfDayLocators.BLACK_PRICE)
        except NoSuchElementException:
            price = False
        if not price:
            price = item.find_element(*ItemsOfDayLocators.ITEM_PRICE_NEW).text.partition('/')[0]
        else:
            price = item.find_element(*ItemsOfDayLocators.BLACK_PRICE).text.partition('/')[0]
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_element_present(*BasketPageLocators.ITEM), \
            'В корзине нет товаров'
        assert self.is_element_present(*BasketPageLocators.ITEM_TITLE), \
            'У товара нет названия'
        assert self.is_element_present(*BasketPageLocators.ITEM_PRICE), \
            'У товара нет цены'
        basket_item_title = self.get_element_text(*BasketPageLocators.ITEM_TITLE)
        basket_item_price = self.get_element_text(*BasketPageLocators.ITEM_PRICE)
        assert basket_item_price == price, \
            f'Цена товара в корзине отличается от цены в слайде, слайд - {price} корзина - {basket_item_price}'
        assert basket_item_title == title, \
            f'Название товара в корзине отличается от названия в слайде, ' \
            f'слайд - {title} корзина - {basket_item_title}'

    def quantity_change_buttons_not_closing_items_of_day(self):
        time.sleep(5)
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def click_on_the_plus_button_adds_one_item_items_of_day(self):
        self.allure_before_after('before')
        self.find_element(*ItemsOfDayLocators.ADD_CART_PLUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*ItemsOfDayLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 2, \
            'Счетчик товаров не изменился после клика в "+"'

    def clicking_the_minus_button_takes_away_one_product_items_of_day(self):
        self.allure_before_after('before')
        self.find_element(*ItemsOfDayLocators.ADD_CART_MINUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*ItemsOfDayLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 1, \
            'Счетчик товаров не изменился после клика в "-"'

    def clicking_on_product_in_items_of_day_section_leads_to_product_card(self):
        item = self.find_element(*ItemsOfDayLocators.ACTIV_SLIDE)
        item_title = item.find_element(*ItemsOfDayLocators.ITEM_TITLE).text
        try:
            item_price = item.find_element(*ItemsOfDayLocators.BLACK_PRICE).text
        except NoSuchElementException:
            item_price = item.find_element(*ItemsOfDayLocators.ITEM_PRICE_NEW).text
        item.find_element(*ItemsOfDayLocators.ITEM_TITLE).click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        product_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert item_title == product_title, \
            'Карточка товара не релевантна товару дня'
        assert item_price.partition('/')[0] == product_price.partition('/')[0], \
            f'Цена в карточке товара не соответствует цене в блоке товар дня - {product_price} поиск - {item_price}'

    def quantity_of_added_product_in_product_card_corresponds_to_quantity_in_items_of_day(self):
        self.is_element_present(*ProductPageLocators.PRODUCT_COUNT_INPUT)
        count_product_page = self.get_element_attribute(*ProductPageLocators.PRODUCT_COUNT_INPUT, 'value')
        self.open()
        time.sleep(3)
        self.is_element_present(*ItemsOfDayLocators.ADD_CART_NUMBER)
        count_items_of_day = self.get_element_attribute(*ItemsOfDayLocators.ADD_CART_NUMBER, 'value')
        assert count_items_of_day == count_product_page, \
            'Количество добавленных в товаре дня, товаров в корзину, не совпадает с количеством в карточке товара'

    def clicking_the_minus_button_removes_the_product_items_of_day(self):
        self.allure_before_after('before')
        self.find_element(*ItemsOfDayLocators.ADD_CART_MINUS).click()
        assert self.is_disappeared(*ItemsOfDayLocators.ADD_CART_MINUS), \
            'Кнопки "+" и "-" не пропали'
        assert self.is_element_present(*ItemsOfDayLocators.ADD_CART_BTN), \
            'Кнопка добавить в корзину не появилась'

    def product_is_not_on_the_shopping_cart_page_items_of_day(self):
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_disappeared(*BasketPageLocators.ITEM), \
            'Товар не удалился со страницы корзина'

    def should_be_popular_categories(self):
        block = self.find_element(*MainPageLocators.POPULAR_CATEGORIES)
        self.scroll_from_origin(block, 0, 100)
        time.sleep(0.3)
        assert self.is_element_present(*MainPageLocators.POPULAR_CATEGORIES), \
            'На странице нет блока популярные категории'

    def should_be_popular_items(self):
        assert self.is_element_present(*PopularCategoriesLocators.ITEMS), \
            'В блоке популярные категории нет слайдов'

    def should_be_title_and_img_popular_items(self):
        assert self.is_element_present(*PopularCategoriesLocators.ACTIV_SLIDE_IMG), \
            'У активного слайда нет картинки'
        assert self.is_element_present(*PopularCategoriesLocators.SLIDE_TITLE), \
            'У активного слайда нет заголовка'
        other_slides = self.find_elements(*PopularCategoriesLocators.ITEMS)
        for o in other_slides[1:]:
            img_size = o.size
            for i in img_size.values():
                assert int(i) > 0, 'У слайда нет картинки'
            assert o.find_element(*PopularCategoriesLocators.SLIDE_TITLE)

    def slide_titles_change_color_on_focus(self):
        title = self.find_element(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE)
        color_before = self.get_style_value(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE, 'color')
        self.move_to(title)
        time.sleep(0.5)
        color_after = self.get_style_value(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE, 'color')
        assert color_before != color_after, \
            'Цвет заголовка в слайдах не меняется'

    def should_be_next_button_navigation_popular_items(self):
        assert self.is_element_present(*PopularCategoriesLocators.NEXT_BUTTON), \
            'В блоке нет кнопки переключения слайда к следующему'

    def there_is_no_back_button_on_the_first_slide_popular_items(self):
        self.allure_find_element('Скрин')
        assert False == self.find_element(*PopularCategoriesLocators.PREV_BUTTON).is_displayed(), \
            'На первом слайде есть кнопка переключения к предыдущему слайду'

    def the_slide_switches_to_the_next_popular_items(self):
        self.is_element_present(*PopularCategoriesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*PopularCategoriesLocators.NEXT_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*PopularCategoriesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "следующий"'

    def back_button_appears_on_the_second_slide_popular_items(self):
        self.is_element_present(*PopularCategoriesLocators.PREV_BUTTON)
        assert self.find_element(*PopularCategoriesLocators.PREV_BUTTON).is_displayed(), \
            'Кнопка переключения слайдов к предыдущему не появилась'

    def the_slide_switches_to_the_prev_popular_items(self):
        self.is_element_present(*PopularCategoriesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*PopularCategoriesLocators.PREV_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*PopularCategoriesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*PopularCategoriesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "предыдущий"'

    def next_button_missing_popular_items(self):
        self.is_element_present(*PopularCategoriesLocators.NEXT_BUTTON)
        self.find_element(*PopularCategoriesLocators.NEXT_BUTTON).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        assert False == self.find_element(*PopularCategoriesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не пропала'

    def next_button_appears_popular_items(self):
        self.allure_before_after('before')
        self.find_element(*PopularCategoriesLocators.PREV_BUTTON).click()
        time.sleep(0.5)
        self.is_element_present(*PopularCategoriesLocators.NEXT_BUTTON)
        assert True == self.find_element(*PopularCategoriesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не появилась'

    def slides_popular_categories_are_clickable(self):
        slide = self.find_element(*PopularCategoriesLocators.ITEMS)
        slide_name = self.get_element_text(*PopularCategoriesLocators.ITEMS)
        self.is_element_present(*PopularCategoriesLocators.ITEMS)
        slide.click()
        self.is_element_present(*CatalogPageLocators.TITLE)
        page_title = self.get_element_text(*CatalogPageLocators.TITLE)
        assert slide_name == page_title, \
            'Страница перехода не релевантна'

    def should_be_hits_sales(self):
        block = self.find_element(*MainPageLocators.HIT_SALES)
        self.scroll_from_origin(block, 0, 100)
        assert self.is_element_present(*MainPageLocators.HIT_SALES), \
            'На странице нет блока "Хиты продаж"'

    def should_be_hits_sales_items(self):
        assert self.is_element_present(*HitsSalesLocators.ITEMS), \
            'В блоке нет плиток товаров'

    def should_be_photo_items_sales_items(self):
        assert self.is_element_present(*HitsSalesLocators.ITEMS_PHOTO), \
            'У слайда нет фотографии'

    def should_be_name_item_sales_items(self):
        assert self.is_element_present(*HitsSalesLocators.ITEMS_TITLE), \
            'На слайде нет названия товара'

    def should_be_price_sales_items(self):
        assert self.is_element_present(*HitsSalesLocators.ITEMS_PRICE), \
            'На слайде нет цены товара'

    def should_be_add_cart_btn_sales_items(self):
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_BTN), \
            'На слайде нет цены товара'

    def should_be_add_favourites_button(self):
        assert self.is_element_present(*HitsSalesLocators.FAVOURITE_BTN), \
            'На слайде нет кнопки добавить в избранное'

    def should_be_reviews_rating(self):
        assert self.is_element_present(*HitsSalesLocators.REVIEWS_RATING), \
            'У товаров нет оценки'

    def should_be_comments_block(self):
        try:
            block = self.find_element(*HitsSalesLocators.COMMENTS_BLOCK)
        except NoSuchElementException:
            pytest.skip("нет товаров с комментариями")
        for i in range(3):
            if not block.is_displayed():
                self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
            else:
                break
        assert self.is_element_present(*HitsSalesLocators.COMMENTS_BLOCK), \
            'У товаров нет блока комментариев'

    def comment_button_is_clickable(self):
        self.find_element(*HitsSalesLocators.COMMENTS_BTN).click()
        url = self.browser.current_url
        assert 'reviews' in url, \
            'Неверный url после клика'
        assert self.is_element_present(*ProductPageLocators.REVIEWS_TAB_ACTIV), \
            'Таб "Отзывы" не активен'

    def slides_hits_sales_are_clickable(self):
        slide = self.find_element(*HitsSalesLocators.ITEMS)
        item = self.find_element(*HitsSalesLocators.ACTIV_SLIDE)
        slide_name = self.get_element_text(*HitsSalesLocators.ITEMS_TITLE)
        slide.click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        page_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        page_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert slide_name == page_title, \
            'Название товара не релевантно'

    def should_be_next_button_navigation_hits_sales(self):
        block = self.find_element(*MainPageLocators.HIT_SALES)
        self.scroll_from_origin(block, 0, 100)
        assert self.is_element_present(*HitsSalesLocators.NEXT_BUTTON), \
            'В блоке нет кнопки переключения слайда к следующему'

    def there_is_no_back_button_on_the_first_slide_hits_sales(self):
        self.allure_find_element('Скрин')
        assert False == self.find_element(*HitsSalesLocators.PREV_BUTTON).is_displayed(), \
            'На первом экране слайдов есть кнопка переключения к предыдущему слайду'

    def the_slide_switches_to_the_next_hits_sales(self):
        self.is_element_present(*HitsSalesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*HitsSalesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*HitsSalesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*HitsSalesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "следующий"'

    def back_button_appears_on_the_second_slide_hits_sales(self):
        self.is_element_present(*HitsSalesLocators.PREV_BUTTON)
        assert self.find_element(*HitsSalesLocators.PREV_BUTTON).is_displayed(), \
            'Кнопка переключения слайдов к предыдущему не появилась'

    def the_slide_switches_to_the_prev_hits_sales(self):
        self.is_element_present(*HitsSalesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*HitsSalesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*HitsSalesLocators.PREV_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*HitsSalesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*HitsSalesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "предыдущий"'

    def next_button_missing_hits_sales(self):
        items_is_displayed = 0
        self.is_element_present(*HitsSalesLocators.NEXT_BUTTON)
        items = self.find_elements(*HitsSalesLocators.ITEMS)
        items_count = len(items)
        for i in items:
            if i.is_displayed() == True:
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
            time.sleep(0.5)
        self.allure_before_after('after')
        assert False == self.find_element(*HitsSalesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не пропала'

    def next_button_appears_hits_sales(self):
        items_is_displayed = 0
        self.allure_before_after('before')
        items = self.find_elements(*HitsSalesLocators.ITEMS)
        items_count = len(items)
        for i in items:
            if i.is_displayed():
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*HitsSalesLocators.PREV_BUTTON).click()
            time.sleep(0.5)
        self.is_element_present(*HitsSalesLocators.NEXT_BUTTON)
        assert True == self.find_element(*HitsSalesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не появилась'

    def button_add_cart_lights_up_when_focused_hits_sales(self):
        color_before = self.get_style_value(*HitsSalesLocators.ADD_CART_BTN, 'background').partition(')')[0]
        btn = self.find_element(*HitsSalesLocators.ADD_CART_BTN)
        self.allure_before_after('before')
        self.move_to(btn)
        time.sleep(0.5)
        self.allure_before_after('after')
        color_after = self.get_style_value(*HitsSalesLocators.ADD_CART_BTN, 'background').partition(')')[0]
        assert color_before != color_after

    def click_the_add_to_cart_button_hits_sales(self):
        add_cart_btn = self.find_element(*HitsSalesLocators.ADD_CART_BTN)
        add_cart_btn.click()
        time.sleep(1)
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def counter_on_cart_icon_hits_sales(self):
        assert self.is_element_present(*MainPageHeaderLocators.CART_COUNTER), \
            'Счетчик товаров в иконке корзины в хедере, не появился'
        count = self.get_element_text(*MainPageHeaderLocators.CART_COUNTER)
        assert count == '1', f'Количество товаров в корзине не равно 1 {count}'

    def added_product_is_displayed_on_the_shopping_cart_page_hits_sales(self):
        item = self.find_element(*HitsSalesLocators.ACTIV_SLIDE)
        title = item.find_element(*HitsSalesLocators.ITEMS_TITLE).text
        try:
            price = item.find_element(*HitsSalesLocators.BLACK_PRICE)
        except NoSuchElementException:
            price = False
        if not price:
            price = item.find_element(*HitsSalesLocators.RED_PRICE).text.partition('/')[0]
        else:
            price = item.find_element(*HitsSalesLocators.BLACK_PRICE).text.partition('/')[0]
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_element_present(*BasketPageLocators.ITEM), \
            'В корзине нет товаров'
        assert self.is_element_present(*BasketPageLocators.ITEM_TITLE), \
            'У товара нет названия'
        assert self.is_element_present(*BasketPageLocators.ITEM_PRICE), \
            'У товара нет цены'
        basket_item_title = self.get_element_text(*BasketPageLocators.ITEM_TITLE)
        basket_item_price = self.get_element_text(*BasketPageLocators.ITEM_PRICE)
        assert basket_item_price == price, \
            f'Цена товара в корзине отличается от цены в слайде, слайд - {price} корзина - {basket_item_price}'
        assert basket_item_title == title, \
            f'Название товара в корзине отличается от названия в слайде, ' \
            f'слайд - {title} корзина - {basket_item_title}'

    def quantity_change_buttons_not_closing_hits_sales(self):
        block = self.find_element(*MainPageLocators.HIT_SALES)
        self.scroll_from_origin(block, 0, 100)
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def click_on_the_plus_button_adds_one_item_hits_sales(self):
        self.allure_before_after('before')
        self.find_element(*HitsSalesLocators.ADD_CART_PLUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*HitsSalesLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 2, \
            'Счетчик товаров не изменился после клика в "+"'

    def clicking_the_minus_button_takes_away_one_product_hits_sales(self):
        self.allure_before_after('before')
        self.find_element(*HitsSalesLocators.ADD_CART_MINUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_attribute(*HitsSalesLocators.ADD_CART_NUMBER, 'value')
        assert int(count_after) == 1, \
            'Счетчик товаров не изменился после клика в "-"'

    def clicking_on_product_in_hits_sales_section_leads_to_product_card(self):
        item = self.find_element(*HitsSalesLocators.ACTIV_SLIDE)
        item_title = item.find_element(*HitsSalesLocators.ITEMS_TITLE).text
        try:
            item_price = item.find_element(*HitsSalesLocators.BLACK_PRICE).text
        except NoSuchElementException:
            item_price = item.find_element(*HitsSalesLocators.RED_PRICE).text
        item.find_element(*HitsSalesLocators.ITEMS_TITLE).click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        product_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert item_title == product_title, \
            'Карточка товара не релевантна товару дня'
        assert item_price.partition('/')[0] == product_price.partition('/')[0], \
            f'Цена в карточке товара не соответствует цене в хиты продаж - {item_price} карточка - {product_price}'

    def quantity_of_added_product_in_product_card_corresponds_to_quantity_in_hits_sales(self):
        self.is_element_present(*ProductPageLocators.PRODUCT_COUNT_INPUT)
        count_product_page = self.get_element_attribute(*ProductPageLocators.PRODUCT_COUNT_INPUT, 'value')
        self.open()
        block = self.find_element(*MainPageLocators.HIT_SALES)
        self.scroll_from_origin(block, 0, 100)
        self.is_element_present(*HitsSalesLocators.ADD_CART_NUMBER)
        count_items_of_day = self.get_element_attribute(*HitsSalesLocators.ADD_CART_NUMBER, 'value')
        assert count_items_of_day == count_product_page, \
            'Количество добавленных в товаре дня, товаров в корзину, не совпадает с количеством в карточке товара'

    def clicking_the_minus_button_removes_the_product_hits_sales(self):
        self.allure_before_after('before')
        minus = self.find_element(*HitsSalesLocators.ADD_CART_MINUS).click()
        time.sleep(0.3)
        assert self.element_is_displayed(*HitsSalesLocators.ADD_CART_MINUS) == False, \
            'Кнопки "+" и "-" не пропали'
        assert self.is_element_present(*HitsSalesLocators.ADD_CART_BTN), \
            'Кнопка добавить в корзину не появилась'

    def product_is_not_on_the_shopping_cart_page_hits_sales(self):
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_disappeared(*BasketPageLocators.ITEM), \
            'Товар не удалился со страницы корзина'

    def photos_in_tiles_switch(self):
        block = self.find_element(*MainPageLocators.HIT_SALES)
        self.scroll_from_origin(block, 0, 100)
        time.sleep(0.5)
        slides = self.find_elements(*HitsSalesLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*HitsSalesLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        for i in range(3):
            if found_slide.is_displayed() == False:
                self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
                time.sleep(0.5)
            else:
                break
        activ_photo = found_slide.find_element(*HitsSalesLocators.ACTIV_PHOTO)
        first_photo_block = found_slide.find_elements(*HitsSalesLocators.PHOTO_SWITCHING_BLOCKS)[0]
        self.move_to(first_photo_block)
        time.sleep(1)
        self.allure_before_after('before')
        url_before = activ_photo.get_attribute('src')
        second_photo_block = found_slide.find_elements(*HitsSalesLocators.PHOTO_SWITCHING_BLOCKS)[-1]
        self.move_to(second_photo_block)
        time.sleep(1)
        activ_photo_after = found_slide.find_element(*HitsSalesLocators.ACTIV_PHOTO)
        self.allure_before_after('after')
        url_after = activ_photo_after.get_attribute('src')
        assert url_before != url_after, \
            'Фотографии в плитках не переключаются'

    def should_be_photo_switching_points(self):
        slides = self.find_elements(*HitsSalesLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*HitsSalesLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        for i in range(3):
            if found_slide.is_displayed() == False:
                self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
                time.sleep(0.5)
            else:
                break
        assert self.is_element_present(*HitsSalesLocators.PHOTO_SWITCHING_POINTS), \
            'У слайда нет точек переключения фотографий'

    def number_dots_is_equal_to_number_of_photos(self):
        slides = self.find_elements(*HitsSalesLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*HitsSalesLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        for i in range(3):
            if not found_slide.is_displayed():
                self.find_element(*HitsSalesLocators.NEXT_BUTTON).click()
                time.sleep(0.5)
            else:
                break
        dots_count = len(found_slide.find_elements(*HitsSalesLocators.PHOTO_DOTS))
        self.allure_find_element('Скрин')
        assert photo_count == dots_count, \
            'Количество точек не соответствует количеству фотографий'

    def should_be_hits_all_btn(self):
        assert self.is_element_present(*HitsSalesLocators.HITS_ALL_BTN), \
            'Нет кнопки "Смотреть все"'

    def button_text_is_highlighted_on_focus(self):
        btn = self.find_element(*HitsSalesLocators.HITS_ALL_BTN)
        color_before = self.get_style_value(*HitsSalesLocators.HITS_ALL_BTN, 'color')
        self.move_to(btn)
        time.sleep(1)
        color_after = self.get_style_value(*HitsSalesLocators.HITS_ALL_BTN, 'color')
        assert color_before != color_after, \
            'Текст кнопки не подсвечивается'

    def click_to_open_hits_page(self):
        btn = self.find_element(*HitsSalesLocators.HITS_ALL_BTN)
        btn.click()
        url = self.browser.current_url
        self.allure_find_element('скрин')
        assert 'hits' in url, 'страница перехода не релевантна'

    def should_be_added_to_favorites(self):
        self.scroll_to_block(*HitsSalesLocators.ITEMS)
        self.find_element(*HitsSalesLocators.FAVOURITE_BTN).click()
        assert self.is_element_present(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик товаров в избранном не появился'
        assert self.element_is_displayed(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик не отображается на странице'

    def should_be_on_the_wishlist_page(self):
        self.is_element_present(*HitsSalesLocators.ITEMS_TITLE)
        item_title = self.get_element_text(*HitsSalesLocators.ITEMS_TITLE)
        self.find_element(*MainPageHeaderLocators.FAVOURITES_BUTTON).click()
        page_title = self.get_element_text(*WishlistPageLocators.TITLE)
        self.is_element_present(*WishlistPageLocators.ITEM_TITLE)
        wishlist_item_title = self.get_element_text(*WishlistPageLocators.ITEM_TITLE)
        assert page_title == 'Избранное', \
            f'Заголовок страницы не совпадает с макетами {page_title}'
        assert item_title == wishlist_item_title, \
            'В избранном нет ранее добавленного товара'

    def not_deleted_after_page_refresh(self):
        self.scroll_to_block(*HitsSalesLocators.ITEMS)
        time.sleep(2)
        assert self.is_element_present(*HitsSalesLocators.ITEM_ADDED_TO_FAVORITES), \
            'Кнопка добавления в избранное не выбрана'

    def should_be_removed_from_favorites(self):
        block = self.find_element(*HitsSalesLocators.ITEMS)
        self.scroll_from_origin(block, 0, 100)
        time.sleep(1)
        self.find_element(*HitsSalesLocators.FAVOURITE_BTN).click()
        # assert self.is_disappeared(*MainPageHeaderLocators.FAVOURITES_COUNT), \
        #     'Счетчик товаров в избранном не пропал'
        time.sleep(3)
        assert not self.element_is_displayed(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик отображается на странице'

    def should_be_removed_from_wishlist_page(self):
        self.find_element(*MainPageHeaderLocators.FAVOURITES_BUTTON).click()
        page_title = self.get_element_text(*WishlistPageLocators.TITLE)
        assert self.is_disappeared(*WishlistPageLocators.ITEMS), \
            'товар не удален'
        assert page_title == 'Избранное', \
            f'Заголовок страницы не совпадает с макетами {page_title}'

    # !!!!!!!!!!!!!!!!

    def should_be_popular_brand(self):
        self.scroll_to_block(*MainPageLocators.POPULAR_BRANDS)
        assert self.is_element_present(*MainPageLocators.POPULAR_BRANDS), \
            'На странице нет блока "Популярные бренды"'

    def should_be_popular_brand_items(self):
        assert self.is_element_present(*PopularBrandsLocators.ITEMS), \
            'В блоке нет слайдов брендов'

    def should_be_logo_popular_brand(self):
        items_count = len(self.find_elements(*PopularBrandsLocators.ITEMS))
        items_logo_count = len(self.find_elements(*PopularBrandsLocators.BRANDS_LOGO))
        assert self.is_element_present(*PopularBrandsLocators.BRANDS_LOGO), \
            'Нет логотипа бренда'
        assert items_count == items_logo_count, \
            'Не у всех брендов есть логотип'

    def should_be_title_items_popular_brand(self):
        items_count = len(self.find_elements(*PopularBrandsLocators.ITEMS))
        items_logo_count = len(self.find_elements(*PopularBrandsLocators.BRANDS_TITLE))
        assert self.is_element_present(*PopularBrandsLocators.BRANDS_TITLE), \
            'Нет наименования бренда'
        assert items_count == items_logo_count, \
            'Не у всех брендов есть наименование'

    def text_color_changes_on_focus(self):
        color_before = self.get_style_value(*PopularBrandsLocators.BRANDS_TITLE, 'color')
        item = self.find_element(*PopularBrandsLocators.ITEMS)
        self.move_to(item)
        color_after = self.get_style_value(*PopularBrandsLocators.BRANDS_TITLE, 'color')
        assert color_before != color_after, \
            'Цвет текста не меняется при фокусе на слайд бренда'

    def should_be_all_brands_btn(self):
        assert self.is_element_present(*PopularBrandsLocators.ALL_BRANDS_BTN), \
            'Нет кнопки все бренды'

    def brand_page_opens(self):
        self.find_element(*PopularBrandsLocators.ALL_BRANDS_BTN).click()
        url = self.browser.current_url
        assert 'brands' in url, \
            'Неверный адрес страницы'
        assert self.is_element_present(*AllBrandsPageLocators.TITLE), \
            'На странице нет заголовка'
        page_title = self.get_element_text(*AllBrandsPageLocators.TITLE)
        assert page_title.lower() == 'Бренды'.lower(), \
            f'Неверный заголовок страницы {page_title}'

    def brand_page_is_relevant_to_slide(self):
        self.scroll_to_block(*MainPageLocators.POPULAR_BRANDS)
        time.sleep(0.4)
        self.is_element_present(*PopularBrandsLocators.BRANDS_TITLE)
        brand_title = self.get_element_text(*PopularBrandsLocators.BRANDS_TITLE)
        self.find_element(*PopularBrandsLocators.BRANDS_TITLE).click()
        self.is_element_present(*BrandPageLocators.TITLE)
        page_title = self.get_element_text(*BrandPageLocators.TITLE)
        assert brand_title.lower() == page_title.lower(), \
            'Страница перехода не релевантна слайду'

    def should_be_next_button_navigation_popular_brand(self):
        self.scroll_to_block(*MainPageLocators.POPULAR_BRANDS)
        time.sleep(0.4)
        assert self.is_element_present(*PopularBrandsLocators.NEXT_BUTTON), \
            'В блоке нет кнопки переключения слайда к следующему'

    def there_is_no_back_button_on_the_first_slide_popular_brand(self):
        self.allure_find_element('Скрин')
        assert False == self.find_element(*PopularBrandsLocators.PREV_BUTTON).is_displayed(), \
            'На первом экране слайдов есть кнопка переключения к предыдущему слайду'

    def the_slide_switches_to_the_next_popular_brand(self):
        self.is_element_present(*PopularBrandsLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*PopularBrandsLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*PopularBrandsLocators.NEXT_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*PopularBrandsLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*PopularBrandsLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "следующий"'

    def back_button_appears_on_the_second_slide_popular_brand(self):
        self.is_element_present(*PopularBrandsLocators.PREV_BUTTON)
        assert self.find_element(*PopularBrandsLocators.PREV_BUTTON).is_displayed(), \
            'Кнопка переключения слайдов к предыдущему не появилась'

    def the_slide_switches_to_the_prev_popular_brand(self):
        self.is_element_present(*PopularBrandsLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*PopularBrandsLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*PopularBrandsLocators.PREV_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*PopularBrandsLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*PopularBrandsLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "предыдущий"'

    def next_button_missing_popular_brand(self):
        items_is_displayed = 0
        self.is_element_present(*PopularBrandsLocators.NEXT_BUTTON)
        items = self.find_elements(*PopularBrandsLocators.ITEMS)
        items_count = len(items)
        for i in items:
            if i.is_displayed():
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*PopularBrandsLocators.NEXT_BUTTON).click()
            time.sleep(0.5)
        self.allure_before_after('after')
        assert False == self.find_element(*PopularBrandsLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не пропала'

    def next_button_appears_popular_brand(self):
        items_is_displayed = 0
        self.allure_before_after('before')
        items = self.find_elements(*PopularBrandsLocators.ITEMS)
        items_count = len(items)
        for i in items:
            if i.is_displayed():
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*PopularBrandsLocators.PREV_BUTTON).click()
            time.sleep(0.5)
        self.is_element_present(*PopularBrandsLocators.NEXT_BUTTON)
        assert True == self.find_element(*PopularBrandsLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не появилась'

    def slides_popular_brads_are_clickable(self):
        slide = self.find_element(*PopularBrandsLocators.ITEMS)
        slide_name = self.get_element_text(*PopularBrandsLocators.BRANDS_TITLE)
        self.is_element_present(*PopularBrandsLocators.BRANDS_TITLE)
        slide.click()
        self.is_element_present(*BrandPageLocators.TITLE)
        page_title = self.get_element_text(*BrandPageLocators.TITLE)
        assert slide_name == page_title, \
            'Страница перехода не релевантна'

    def should_be_lower_banner(self):
        banner = self.find_element(*MainPageLocators.LOWER_BANNER)
        self.scroll_from_origin(banner, 0, 100)
        assert self.is_element_present(*MainPageLocators.LOWER_BANNER), \
            'На странице нет нижнего баннера'

    def should_be_lower_banner_navigation(self):
        assert self.is_element_present(*LowerBannerLocators.MAIN_NAVIGATION), \
            'У баннера нет блока управления слайдами'

    def should_be_prev_button_navigation_lower_banner(self):
        assert self.is_element_present(*LowerBannerLocators.PREV_BUTTON), \
            'В блоке управления слайдами, нет кнопки переключения назад'

    def should_be_next_button_navigation_lower_banner(self):
        assert self.is_element_present(*LowerBannerLocators.NEXT_BUTTON), \
            'В блоке управления слайдами, нет кнопки переключения вперед'

    def should_be_current_value_lower_banner(self):
        assert self.is_element_present(*LowerBannerLocators.CURRENT_VALUE), \
            'В блоке управления слайдами не отображается номер текущего слайда'

    def should_be_total_value_lower_banner(self):
        assert self.is_element_present(*LowerBannerLocators.TOTAL_VALUE), \
            'В блоке управления слайдами не отображается общее количество слайдов'

    def slides_can_be_switched_back_lower_banner(self):
        prev_button = self.find_element(*LowerBannerLocators.PREV_BUTTON)
        idx_activ_slide_before = \
            self.get_element_attribute(*LowerBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_before = self.get_element_text(*LowerBannerLocators.CURRENT_VALUE)
        self.allure_before_after('before')
        prev_button.click()
        time.sleep(0.3)
        idx_activ_slide_after = self.get_element_attribute(*LowerBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_after = self.get_element_text(*LowerBannerLocators.CURRENT_VALUE)
        self.allure_before_after('after')
        assert value_before != value_after, \
            'Номер активного слайда не изменился'
        assert idx_activ_slide_before != idx_activ_slide_after, \
            'Индекс активного слайда не изменился'

    def slides_can_be_switched_forward_lower_banner(self):
        prev_button = self.find_element(*LowerBannerLocators.NEXT_BUTTON)
        time.sleep(1)
        idx_activ_slide_before = self.get_element_attribute(*LowerBannerLocators.ACTIVE_SLIDE,
                                                            'data-swiper-slide-index')
        value_before = self.get_element_text(*LowerBannerLocators.CURRENT_VALUE)
        self.allure_before_after('before')
        prev_button.click()
        time.sleep(0.3)
        idx_activ_slide_after = self.get_element_attribute(*LowerBannerLocators.ACTIVE_SLIDE, 'data-swiper-slide-index')
        value_after = self.get_element_text(*LowerBannerLocators.CURRENT_VALUE)
        self.allure_before_after('after')
        assert value_before != value_after, \
            'Номер активного слайда не изменился'
        assert idx_activ_slide_before != idx_activ_slide_after, \
            'Индекс активного слайда не изменился'

    def number_of_slides_is_equal_to_number_in_counter_lower_banner(self):
        count = []
        total_counter = int(self.get_element_text(*MainBannerLocators.TOTAL_VALUE))
        slides = self.find_elements(*MainBannerLocators.SLIDES)
        for s in slides:
            count.append(s.get_attribute("data-swiper-slide-index"))
        total_slides = len(set(count))
        assert total_counter == total_slides, f'{total_counter} {total_slides}'

    def slides_in_the_lower_banner_are_clickable(self):
        slides = self.find_elements(*LowerBannerLocators.SLIDES)
        for s in slides:
            if s.is_displayed():
                active_slide = s
                break
        active_slide.click()
        url = self.browser.current_url
        assert 'action' in url, \
            f'Неверная страница перехода {url}'
        assert self.is_element_present(*ActionsPage.TITLE), \
            'На странице нет заголовка'

    def should_be_block_expert_advices(self):
        self.scroll_to_block(*MainPageLocators.EXPERT_ADVICES)
        time.sleep(0.4)
        assert self.is_element_present(*MainPageLocators.EXPERT_ADVICES), \
            'На странице нет блока "Советы экспертов"'

    def should_be_expert_advices_items(self):
        assert self.is_element_present(*ExpertAdvicesLocators.SLIDES), \
            'В блоке нет слайдов'

    def should_be_photo_expert_advices(self):
        items_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDES))
        items_img_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDES_IMG))
        assert self.is_element_present(*ExpertAdvicesLocators.SLIDES_IMG), \
            'У слайдов нет фотографии'
        assert items_count == items_img_count, \
            'Не у всех слайдов есть фотографии'

    def should_be_category_slide(self):
        items_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDES))
        items_category_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDE_CATEGORY))
        assert self.is_element_present(*ExpertAdvicesLocators.SLIDE_CATEGORY), \
            'У слайдов нет категории'
        assert items_count == items_category_count, \
            'Не у всех слайдов есть категория'

    def should_be_title_slide(self):
        items_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDES))
        items_description_count = len(self.find_elements(*ExpertAdvicesLocators.SLIDE_TITLE))
        assert self.is_element_present(*ExpertAdvicesLocators.SLIDE_TITLE), \
            'У слайдов нет описания'
        assert items_count == items_description_count, \
            'Не у всех слайдов есть описание'

    def should_be_all_advices(self):
        assert self.is_element_present(*ExpertAdvicesLocators.ALL_ADVICES), \
            'Нет кнопки "Смотреть все"'

    def blog_page_opens(self):
        self.find_element(*ExpertAdvicesLocators.ALL_ADVICES).click()
        url = self.browser.current_url
        assert 'blog' in url, \
            'Неверный url перехода'
        self.is_element_present(*BlogPageLocators.TITLE)
        title = self.get_element_text(*BlogPageLocators.TITLE)
        assert title.lower() == 'блог'.lower(), \
            'Страница перехода не релевантна'

    def slides_expert_advices_are_clickable(self):
        slide = self.find_element(*ExpertAdvicesLocators.SLIDES)
        slide_name = self.get_element_text(*ExpertAdvicesLocators.SLIDE_TITLE)
        self.is_element_present(*ExpertAdvicesLocators.SLIDE_TITLE)
        slide.click()
        self.is_element_present(*BlogPageLocators.TITLE)
        page_title = self.get_element_text(*BlogPageLocators.TITLE)
        assert slide_name == page_title, \
            'Страница перехода не релевантна'

    def should_be_next_button_navigation_expert_advices(self):
        self.scroll_to_block(*ExpertAdvicesLocators.SLIDES)
        time.sleep(0.3)
        assert self.is_element_present(*ExpertAdvicesLocators.NEXT_BUTTON), \
            'В блоке нет кнопки переключения слайда к следующему'

    def there_is_no_back_button_on_the_first_slide_expert_advices(self):
        self.allure_find_element('Скрин')
        assert False == self.find_element(*ExpertAdvicesLocators.PREV_BUTTON).is_displayed(), \
            'На первом экране слайдов есть кнопка переключения к предыдущему слайду'

    def the_slide_switches_to_the_next_expert_advices(self):
        self.is_element_present(*ExpertAdvicesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*ExpertAdvicesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*ExpertAdvicesLocators.NEXT_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*ExpertAdvicesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*ExpertAdvicesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "следующий"'

    def back_button_appears_on_the_second_slide_expert_advices(self):
        self.is_element_present(*ExpertAdvicesLocators.PREV_BUTTON)
        assert self.find_element(*ExpertAdvicesLocators.PREV_BUTTON).is_displayed(), \
            'Кнопка переключения слайдов к предыдущему не появилась'

    def the_slide_switches_to_the_prev_expert_advices(self):
        self.is_element_present(*ExpertAdvicesLocators.ACTIV_SLIDE)
        title_before = self.get_element_text(*ExpertAdvicesLocators.ACTIV_SLIDE_TITLE)
        self.find_element(*ExpertAdvicesLocators.PREV_BUTTON).click()
        time.sleep(1)
        self.is_element_present(*ExpertAdvicesLocators.ACTIV_SLIDE)
        title_after = self.get_element_text(*ExpertAdvicesLocators.ACTIV_SLIDE_TITLE)
        assert title_before != title_after, \
            'Слайд не переключился кликом в кнопку "предыдущий"'

    def next_button_missing_expert_advices(self):
        items_is_displayed = 0
        self.is_element_present(*ExpertAdvicesLocators.NEXT_BUTTON)
        items = self.find_elements(*ExpertAdvicesLocators.SLIDES)
        items_count = len(items)
        for i in items:
            if i.is_displayed():
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*ExpertAdvicesLocators.NEXT_BUTTON).click()
            time.sleep(0.5)
        self.allure_before_after('after')
        assert False == self.find_element(*ExpertAdvicesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не пропала'

    def next_button_appears_expert_advices(self):
        items_is_displayed = 0
        self.allure_before_after('before')
        items = self.find_elements(*ExpertAdvicesLocators.SLIDES)
        items_count = len(items)
        for i in items:
            if i.is_displayed():
                items_is_displayed += 1
        clicks = int(items_count / items_is_displayed)
        for r in range(clicks):
            self.find_element(*ExpertAdvicesLocators.PREV_BUTTON).click()
            time.sleep(0.5)
        self.is_element_present(*ExpertAdvicesLocators.NEXT_BUTTON)
        assert True == self.find_element(*ExpertAdvicesLocators.NEXT_BUTTON).is_displayed(), \
            'Кнопка переключения к следующему слайду, не появилась'


class CatalogPage(BasePage):

    def on_the_left_side_of_the_first_level_category(self):
        assert self.is_element_present(*CatalogPopupLocators.CATEGORIES), \
            'В попапе каталога нет категории'

    def should_be_second_level_categories(self):
        assert self.is_element_present(*CatalogPopupLocators.CATEGORIES_SECOND_LVL), \
            'В попапе нет категорий второго уровня'

    def should_be_third_level_categories(self):
        assert self.is_element_present(*CatalogPopupLocators.CATALOG_THIRD_LEVEL), \
            'В попапе нет категорий второго уровня'

    def when_opening_window_top_category_is_selected(self):
        self.is_element_present(*CatalogPopupLocators.CATEGORY_SELECTED)
        category_id = self.get_element_attribute(*CatalogPopupLocators.CATEGORY_SELECTED, 'data-id')
        assert category_id == '0', \
            f'Выбрана не первая категория {category_id}'

    def categories_are_selected_by_hovering_over(self):
        self.is_element_present(*CatalogPopupLocators.CATEGORY_SELECTED)
        category_id_before = self.get_element_attribute(*CatalogPopupLocators.CATEGORY_SELECTED, 'data-id')
        category = self.get_random_element(*CatalogPopupLocators.CATEGORIES)
        self.scroll_to(category)
        self.move_to(category)
        time.sleep(1)
        self.is_element_present(*CatalogPopupLocators.CATEGORY_SELECTED)
        category_id_after = self.get_element_attribute(*CatalogPopupLocators.CATEGORY_SELECTED, 'data-id')
        assert category_id_after != category_id_before, \
            'Категория не выбралась наведением курсора'

    def go_to_directory_of_first_lvl(self):
        category = self.get_random_element(*CatalogPopupLocators.CATEGORIES)
        category_name = category.text
        move = self.find_element(*CatalogPopupLocators.CATEGORIES)
        self.move_to(move)
        self.scroll_from_origin(category, 0, 20)
        category.click()
        self.is_element_present(*CatalogPageLocators.TITLE)
        page_title = self.get_element_text(*CatalogPageLocators.TITLE)
        assert category_name == page_title, \
            'Клик в категорию открывает не релевантную страницу'

    def go_to_directory_of_second_lvl(self):
        category = self.get_random_element(*CatalogPopupLocators.CATEGORIES)
        move = self.find_element(*CatalogPopupLocators.CATEGORIES)
        self.move_to(move)
        self.scroll_from_origin(category, 0, 20)
        self.move_to(category)
        time.sleep(0.5)
        categories_second_lvl = self.find_elements(*CatalogPopupLocators.CATEGORIES_SECOND_LVL)
        for c in categories_second_lvl:
            if c.is_displayed():
                second_lvl_category = c
                break
        second_lvl_cat_name = second_lvl_category.text
        second_lvl_category.click()
        self.is_element_present(*CatalogPageLocators.TITLE)
        page_title = self.get_element_text(*CatalogPageLocators.TITLE)
        assert second_lvl_cat_name == page_title, \
            'Клик в категорию открывает не релевантную страницу'

    def go_to_directory_of_listing_products(self):
        num = 0
        category = self.get_random_element(*CatalogPopupLocators.CATEGORIES)
        move = self.find_element(*CatalogPopupLocators.CATEGORIES)
        self.move_to(move)
        self.scroll_from_origin(category, 0, 20)
        self.move_to(category)
        time.sleep(0.5)
        categories_third_lvl = self.find_elements(*CatalogPopupLocators.CATALOG_THIRD_LEVEL)
        for c in categories_third_lvl:
            if c.is_displayed():
                third_lvl_cat = c
                num = 1
                break
        if num == 0:
            self.go_to_directory_of_listing_products()
            return
        third_lvl_cat_name = third_lvl_cat.text
        third_lvl_cat.click()
        self.is_element_present(*CatalogPageLocators.TITLE)
        page_title = self.get_element_text(*CatalogPageLocators.TITLE)
        assert third_lvl_cat_name == page_title, \
            'Клик в категорию открывает не релевантную страницу'

    def should_be_catalog_title(self):
        assert self.is_element_present(*FirstLvlCatalogLocators.TITLE), \
            'На странице нет каталога'
        title = self.get_element_text(*FirstLvlCatalogLocators.TITLE)
        assert title.lower() == 'Каталог'.lower(), \
            f'Неверный заголовок страницы {title}'

    def should_be_second_lvl_categories(self):
        assert self.is_element_present(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES), \
            'В каталоге нет категорий второго уровня'

    def should_be_third_lvl_categories(self):
        assert self.is_element_present(*FirstLvlCatalogLocators.THIRD_LVL_CATEGORIES), \
            'В каталоге нет категорий третьего уровня'

    def should_be_category_photo(self):
        categories_count = len(self.find_elements(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES))
        assert self.is_element_present(*FirstLvlCatalogLocators.CATEGORIES_PHOTO), \
            'У категорий нет фотографий'
        photo_count = len(self.find_elements(*FirstLvlCatalogLocators.CATEGORIES_PHOTO))
        assert categories_count == photo_count, \
            'Не у всех категорий есть фото'

    def no_more_than_eight_categories(self):
        groups = self.find_elements(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        self.is_element_present(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        for g in groups:
            group_name = g.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES).text
            categories_count = len(g.find_elements(*FirstLvlCatalogLocators.CATEGORIES_THIRD_LVL))
            assert categories_count <= 8, \
                f'В группе {group_name} больше 8 категорий'

    def should_be_show_more_link(self):
        groups = self.find_elements(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        self.is_element_present(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        for g in groups:
            group_name = g.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES).text
            categories_count = len(g.find_elements(*FirstLvlCatalogLocators.CATEGORIES_THIRD_LVL))
            if categories_count >= 8:
                assert self.is_element_present(*FirstLvlCatalogLocators.SHOW_MORE_LINK, False), \
                    f'У блока {group_name}, нет кнопки "Показать все n"'
            else:
                assert self.is_disappeared(*FirstLvlCatalogLocators.SHOW_MORE_LINK), \
                    'Есть кнопка у группы с количеством категорий меньше 8'
        self.is_element_present(*FirstLvlCatalogLocators.SHOW_MORE_LINK)

    def clicking_on_show_all_opens_second_lvl_directory(self, num):
        time.sleep(2)
        category = self.get_random_element(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        category_name = category.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES).text
        show_all_link = category.find_element(*FirstLvlCatalogLocators.SHOW_MORE_LINK)
        show_all_link.click()
        self.is_element_present(*SecondLvlCatalogLocators.TITLE)
        page_title = self.get_element_text(*SecondLvlCatalogLocators.TITLE)
        assert category_name.lower() == page_title.lower(), \
            f'{num} - Страница перехода {page_title} не релевантна группе {category_name}'

    def opens_second_lvl_directory(self, num):
        time.sleep(2)
        category = self.get_random_element(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        category_name = category.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES).text
        block_title = category.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES)
        block_title.click()
        self.is_element_present(*SecondLvlCatalogLocators.TITLE)
        page_title = self.get_element_text(*SecondLvlCatalogLocators.TITLE)
        assert category_name.lower() == page_title.lower(), \
            f'{num} - Страница перехода {page_title} не релевантна группе {category_name}'

    def opens_third_lvl_directory(self, num):
        time.sleep(2)
        category = self.get_random_element(*FirstLvlCatalogLocators.CATEGORIES_THIRD_LVL)
        category_name = category.text
        category.click()
        self.is_element_present(*SecondLvlCatalogLocators.TITLE)
        page_title = self.get_element_text(*ListingLocators.TITLE)
        assert category_name.lower() == page_title.lower(), \
            f'{num} - Страница перехода {page_title} не релевантна категории {category_name}'

    def go_to_listing(self):
        category = self.get_random_element(*FirstLvlCatalogLocators.CATEGORIES_THIRD_LVL)
        category_name = category.text
        # self.scroll_from_origin(category, 10)
        category.click()
        time.sleep(5)

    def go_to_second_lvl_catalog(self):
        category = self.get_random_element(*FirstLvlCatalogLocators.CATEGORIES_GROUP)
        category_name = category.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES).text
        block_title = category.find_element(*FirstLvlCatalogLocators.SECOND_LVL_CATEGORIES)
        block_title.click()

    def should_be_second_lvl_catalog_title(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.TITLE)

    def should_be_back_button(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.BACK_BTN)

    def returns_to_catalog_page(self):
        self.find_element(*SecondLvlCatalogLocators.BACK_BTN).click()
        url = self.browser.current_url
        self.is_element_present(*FirstLvlCatalogLocators.TITLE)
        assert url == 'https://stroylandiya.ru/catalog/', \
            'Неверная страница перехода'

    def should_be_categories_block(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.CATEGORIES_BLOCK), \
            'На странице нет блока с категориями'

    def should_be_categories(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.CATEGORIES), \
            'На странице нет плиток с категориями'

    def could_be_a_banner_ad(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.BANNER_UPPER_CATEGORIES), \
            'На странице нет рекламного баннера'

    def tiles_are_clickable_and_relevant(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS)
        item = self.find_element(*SecondLvlCatalogLocators.ITEMS)
        item_title = self.get_element_text(*SecondLvlCatalogLocators.ITEMS_TITLE)
        item_price = self.get_element_text(*SecondLvlCatalogLocators.PRICE)
        item.find_element(*SecondLvlCatalogLocators.ITEMS_TITLE).click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        product_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert item_title == product_title, \
            'Карточка товара не релевантна плитке'
        assert item_price == product_price, \
            'Цена не релевантна'

    def photos_in_tiles_switch(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS, 150)
        slides = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*SecondLvlCatalogLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        activ_photo = found_slide.find_element(*SecondLvlCatalogLocators.ACTIV_PHOTO)
        first_photo_block = found_slide.find_elements(*SecondLvlCatalogLocators.PHOTO_SWITCHING_BLOCKS)[0]
        self.move_to(first_photo_block)
        time.sleep(1)
        self.allure_before_after('before')
        url_before = activ_photo.get_attribute('style')
        second_photo_block = found_slide.find_elements(*SecondLvlCatalogLocators.PHOTO_SWITCHING_BLOCKS)[-1]
        self.move_to(second_photo_block)
        time.sleep(1)
        activ_photo_after = found_slide.find_element(*SecondLvlCatalogLocators.ACTIV_PHOTO)
        self.allure_before_after('after')
        url_after = activ_photo_after.get_attribute('style')
        assert url_before != url_after, \
            'Фотографии в плитках не переключаются'

    def should_be_photo_switching_points(self):
        sort = self.find_element(*SecondLvlCatalogLocators.SORT)
        self.move_to(sort)
        slides = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*SecondLvlCatalogLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        assert self.is_element_present(*SecondLvlCatalogLocators.PHOTO_SWITCHING_POINTS), \
            'У слайда нет точек переключения фотографий'

    def number_dots_is_equal_to_number_of_photos(self):
        slides = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*SecondLvlCatalogLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        dots_count = len(found_slide.find_elements(*SecondLvlCatalogLocators.PHOTO_DOTS))
        self.allure_find_element('Скрин')
        assert photo_count == dots_count, \
            'Количество точек не соответствует количеству фотографий'

    def button_add_cart_lights_up_when_focused(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ADD_CART_BTN)
        color_before = self.get_style_value(*SecondLvlCatalogLocators.ADD_CART_BTN, 'background-color').partition(')')[
            0]
        btn = self.find_element(*SecondLvlCatalogLocators.ADD_CART_BTN)
        self.allure_before_after('before')
        self.move_to(btn)
        time.sleep(0.5)
        self.allure_before_after('after')
        color_after = \
            self.get_style_value(*SecondLvlCatalogLocators.ADD_CART_BTN_HOVER, 'background-color').partition(')')[0]
        assert color_before != color_after

    def click_the_add_to_cart_button(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS)
        add_cart_btn = self.find_element(*SecondLvlCatalogLocators.ADD_CART_BTN)
        add_cart_btn.click()
        time.sleep(1)
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def counter_on_cart_icon(self):
        assert self.is_element_present(*MainPageHeaderLocators.CART_COUNTER), \
            'Счетчик товаров в иконке корзины в хедере, не появился'
        count = self.get_element_text(*MainPageHeaderLocators.CART_COUNTER)
        assert count == '1', \
            f'Количество товаров в корзине не равно 1 {count}'

    def added_product_is_displayed_on_the_shopping_cart_page(self):
        item = self.find_element(*SecondLvlCatalogLocators.ITEMS)
        title = item.find_element(*SecondLvlCatalogLocators.ITEMS_TITLE).text
        price = item.find_element(*SecondLvlCatalogLocators.PRICE).text.partition('/')[0]
        self.allure_before_after('before')
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_element_present(*BasketPageLocators.ITEM), \
            'В корзине нет товаров'
        assert self.is_element_present(*BasketPageLocators.ITEM_TITLE), \
            'У товара нет названия'
        assert self.is_element_present(*BasketPageLocators.ITEM_PRICE), \
            'У товара нет цены'
        basket_item_title = self.get_element_text(*BasketPageLocators.ITEM_TITLE)
        basket_item_price = self.get_element_text(*BasketPageLocators.ITEM_PRICE)
        assert basket_item_price == price, \
            f'Цена товара в корзине отличается от цены в слайде, слайд - {price} корзина - {basket_item_price}'
        assert basket_item_title == title, \
            f'Название товара в корзине отличается от названия в слайде, ' \
            f'слайд - {title} корзина - {basket_item_title}'

    def quantity_change_buttons_not_closing(self):
        self.browser.back()
        time.sleep(3)
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS)
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_MINUS), \
            'Не появилась кнопка уменьшения количества "-"'
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_NUMBER), \
            'Не появился счетчик товара'
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_PLUS), \
            'Не появилась кнопка увеличения количества товара "+"'

    def click_on_the_plus_button_adds_one_item(self):
        self.allure_before_after('before')
        self.find_element(*SecondLvlCatalogLocators.ADD_CART_PLUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_text(*SecondLvlCatalogLocators.ADD_CART_NUMBER)
        assert int(count_after) == 2, \
            'Счетчик товаров не изменился после клика в "+"'

    def clicking_the_minus_button_takes_away_one_product(self):
        self.allure_before_after('before')
        self.find_element(*SecondLvlCatalogLocators.ADD_CART_MINUS).click()
        time.sleep(0.5)
        self.allure_before_after('after')
        count_after = self.get_element_text(*SecondLvlCatalogLocators.ADD_CART_NUMBER)
        assert int(count_after) == 1, \
            'Счетчик товаров не изменился после клика в "-"'

    def clicking_on_product_in_slider_section_leads_to_product_card(self):
        item = self.find_element(*SecondLvlCatalogLocators.ITEMS)
        item_title = item.find_element(*SecondLvlCatalogLocators.ITEMS_TITLE).text
        item_price = item.find_element(*SecondLvlCatalogLocators.PRICE).text
        item.find_element(*SecondLvlCatalogLocators.ITEMS_TITLE).click()
        self.is_element_present(*ProductPageLocators.PRODUCT_TITLE)
        self.is_element_present(*ProductPageLocators.PRODUCT_PRICE)
        product_title = self.get_element_text(*ProductPageLocators.PRODUCT_TITLE)
        product_price = self.get_element_text(*ProductPageLocators.PRODUCT_PRICE)
        assert item_title == product_title, \
            'Карточка товара не релевантна товару в слайдере'
        assert item_price.partition('/')[0] == product_price.partition('/')[0], \
            f'Цена в карточке товара не соответствует цене в слайдере - {item_price} карточка - {product_price}'

    def quantity_of_added_product_in_product_card_corresponds_to_quantity_in_slider(self):
        self.is_element_present(*ProductPageLocators.PRODUCT_COUNT_INPUT)
        count_product_page = self.get_element_attribute(*ProductPageLocators.PRODUCT_COUNT_INPUT, 'value')
        self.browser.back()
        time.sleep(0.4)
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS)
        self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_NUMBER)
        count_items_of_day = self.get_element_text(*SecondLvlCatalogLocators.ADD_CART_NUMBER)
        assert count_items_of_day == count_product_page, \
            'Количество добавленных в слайдере, товаров в корзину, не совпадает с количеством в карточке товара'

    def clicking_the_minus_button_removes_the_product(self):
        self.allure_before_after('before')
        minus = self.find_element(*SecondLvlCatalogLocators.ADD_CART_MINUS).click()
        time.sleep(1)
        assert not self.element_is_displayed(*SecondLvlCatalogLocators.ADD_CART_MINUS), \
            'Кнопки "+" и "-" не пропали'
        assert self.is_element_present(*SecondLvlCatalogLocators.ADD_CART_BTN), \
            'Кнопка добавить в корзину не появилась'

    def product_is_not_on_the_shopping_cart_page(self):
        self.find_element(*MainPageHeaderLocators.BASKET_BUTTON).click()
        assert self.is_disappeared(*BasketPageLocators.ITEM), \
            'Товар не удалился со страницы корзина'

    def should_be_added_to_favorites(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS, 20)
        btn = self.find_element(*SecondLvlCatalogLocators.FAVOURITE_BTN)
        self.move_to(btn)
        self.action_click()
        time.sleep(1)
        assert self.is_element_present(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик товаров в избранном не появился'
        assert self.element_is_displayed(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик не отображается на странице'

    def should_be_on_the_wishlist_page(self):
        self.is_element_present(*SecondLvlCatalogLocators.ITEMS_TITLE)
        item_title = self.get_element_text(*SecondLvlCatalogLocators.ITEMS_TITLE)
        self.find_element(*MainPageHeaderLocators.FAVOURITES_BUTTON).click()
        page_title = self.get_element_text(*WishlistPageLocators.TITLE)
        self.is_element_present(*WishlistPageLocators.ITEM_TITLE)
        wishlist_item_title = self.get_element_text(*WishlistPageLocators.ITEM_TITLE)
        assert page_title == 'Избранное', \
            f'Заголовок страницы не совпадает с макетами {page_title}'
        assert item_title == wishlist_item_title, \
            'В избранном нет ранее добавленного товара'

    def not_deleted_after_page_refresh(self):
        self.browser.back()
        time.sleep(2)
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS, 10)
        assert self.is_element_present(*SecondLvlCatalogLocators.ITEM_ADDED_TO_FAVORITES), \
            'Кнопка добавления в избранное не выбрана'

    def should_be_removed_from_favorites(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS)
        # self.find_element(*SecondLvlCatalogLocators.ITEM_ADDED_TO_FAVORITES).click()
        self.find_element(*SecondLvlCatalogLocators.FAVOURITE_BTN).click()
        time.sleep(1)
        assert not self.element_is_displayed(*MainPageHeaderLocators.FAVOURITES_COUNT), \
            'Счетчик отображается на странице'

    def should_be_removed_from_wishlist_page(self):
        self.find_element(*MainPageHeaderLocators.FAVOURITES_BUTTON).click()
        page_title = self.get_element_text(*WishlistPageLocators.TITLE)
        assert self.is_disappeared(*WishlistPageLocators.ITEMS), \
            'товар не удален'
        assert page_title == 'Избранное', \
            f'Заголовок страницы не совпадает с макетами {page_title}'

    def should_be_compare_button(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.COMPARE_BTN), \
            'Кнопка сравнения товаров не найдена'
        items = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        items_count = len(items)
        compare_btn = self.find_elements(*SecondLvlCatalogLocators.COMPARE_BTN)
        compare_btn_count = len(compare_btn)
        assert items_count == compare_btn_count, \
            'Количество кнопок сравнения товаров не совпадает с количеством товаров в каталоге'

    def click_on_compare_button(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.COMPARE_BTN, 10)
        self.find_element(*SecondLvlCatalogLocators.COMPARE_BTN).click()
        time.sleep(2)
        assert self.is_element_present(*SecondLvlCatalogLocators.COMPARE_BTN_ACTIVE), \
            'Кнопка сравнения товаров не активирована'
        assert self.is_element_present(*MainPageHeaderLocators.COMPARE_COUNTER), \
            'Не появился счетчик товаров в сравнении'
        compare_count = int(self.get_element_text(*MainPageHeaderLocators.COMPARE_COUNTER))
        assert compare_count == 1, \
            'Количество товаров сравнений не соответствует'

    def product_is_displayed_on_the_comparison_page(self):
        self.find_element(*MainPageHeaderLocators.COMPARE_BUTTON).click()
        assert self.is_element_present(*ComparePage.ITEM_BLOCK), \
            'Товар не отображается на странице сравнения'

    def product_is_removed_from_comparison(self):
        self.browser.back()
        time.sleep(2)
        self.find_element(*SecondLvlCatalogLocators.COMPARE_BTN).click()
        assert self.is_disappeared(*SecondLvlCatalogLocators.COMPARE_BTN_ACTIVE), \
            'Кнопка добавления к сравнению осталась активирована'
        assert self.is_disappeared(*MainPageHeaderLocators.COMPARE_COUNTER), \
            'Счетчик товаров в сравнении не закрылся'

    def product_is_not_displayed_on_the_comparison_page(self):
        self.find_element(*MainPageHeaderLocators.COMPARE_BUTTON).click()
        assert self.is_disappeared(*ComparePage.ITEM_BLOCK), \
            'Товар не удалился со страницы сравнения'

    def should_be_window_with_characteristics(self):
        item = self.find_element(*SecondLvlCatalogLocators.ITEMS)
        self.scroll_from_origin(item, 0, 50)
        self.move_to(item)
        time.sleep(0.3)
        assert self.is_element_present(*SecondLvlCatalogLocators.TOOLTIP), \
            'Окно с характеристиками товара не найдено'
        assert self.element_is_displayed(*SecondLvlCatalogLocators.TOOLTIP, False), \
            'Окно с характеристиками товара не отображается'

    def maximum_10_characteristics(self):
        tooltip_items_count = 0
        tooltip_items = self.find_elements(*SecondLvlCatalogLocators.TOOLTIP_ITEMS)
        for i in tooltip_items:
            if i.is_displayed():
                tooltip_items_count += 1
        assert tooltip_items_count <= 10, \
            'В окне больше 10 характеристик'

    def characteristics_are_relevant_to_the_product_card(self):
        tooltip_items_list = []
        product_characteristics_list = []
        self.is_element_present(*SecondLvlCatalogLocators.TOOLTIP)
        tooltip_items = self.find_elements(*SecondLvlCatalogLocators.TOOLTIP_ITEMS)
        for i in tooltip_items:
            if i.is_displayed():
                tooltip_items_list.append(i.text)
        self.find_element(*SecondLvlCatalogLocators.ITEMS_TITLE).click()
        self.is_element_present(*ProductPageLocators.CHARACTERISTICS)
        product_characteristics = self.find_elements(*ProductPageLocators.CHARACTERISTICS)
        for i in product_characteristics:
            if i.is_displayed():
                product_characteristics_list.append(i.text)
        assert tooltip_items_list == product_characteristics_list, \
            'Характеристики товара не соответствуют карточке товара'

    def relevant_listing_opens(self, num):
        time.sleep(1)
        item = self.get_random_element(*SecondLvlCatalogLocators.CATEGORIES)
        item_name = item.find_element(*SecondLvlCatalogLocators.CATALOG_ITEM_NAME).text
        item.click()
        self.is_element_present(*ListingLocators.TITLE)
        page_title = self.get_element_text(*ListingLocators.TITLE)
        assert item_name.lower() == page_title.lower(), \
            f'{num} - Листинг {page_title} не релевантен выбранной категории {item_name}'

    def should_be_categories_links(self):
        assert self.is_element_present(*SecondLvlCatalogLocators.CATEGORY_LINK), \
            'В фильтрах нет категорий'
        links_count = len(self.find_elements(*SecondLvlCatalogLocators.CATEGORY_LINK))
        categories_count = len(self.find_elements(*SecondLvlCatalogLocators.CATEGORIES))
        assert links_count == categories_count, \
            'В фильтрах не все категории'

    def click_on_category_link(self):
        category = self.get_random_element(*SecondLvlCatalogLocators.CATEGORY_LINK)
        self.scroll_from_origin(category, 0, 50)
        category_name = category.text
        category.click()
        time.sleep(2)
        self.is_element_present(*WishlistPageLocators.TITLE)
        page_title = self.get_element_text(*WishlistPageLocators.TITLE)
        assert category_name == page_title, \
            f'Страница перехода не релевантна {category_name} {page_title}'

    # def no_more_than_five_subcategories_are_displayed(self):
    #     self.is_element_present(*SecondLvlCatalogLocators.LINKS_BLOCK)
    #     blocks = self.find_elements(*SecondLvlCatalogLocators.LINKS_BLOCK)
    #     for b in blocks:
    #         # category_title = b.find_element(*SecondLvlCatalogLocators.CATEGORY_TITLE).text
    #         items_count = int(b.find_element(*SecondLvlCatalogLocators.BLOCK_ITEMS).get_attribute('data-show-count'))
    #         assert items_count <= 5, \
    #             f'В категории отображается больше 5 подкатегорий'

    def maybe_show_everything(self):
        self.is_element_present(*SecondLvlCatalogLocators.SHOW_ALL_BTN)
        blocks = self.find_elements(*SecondLvlCatalogLocators.LINKS_BLOCK)
        for b in blocks:
            try:
                b.find_element(*SecondLvlCatalogLocators.SUBCATEGORY_HIDDEN_LINK)
            except NoSuchElementException:
                continue
            else:
                block = b
                block_name = block.find_element(*SecondLvlCatalogLocators.CATEGORY_TITLE).text
                assert block.find_element(*SecondLvlCatalogLocators.SUBCATEGORY_HIDDEN_LINK), \
                    f'Есть кнопка "Смотреть все" в категории с количеством подкатегорий меньше 5 {block_name}'

    def expands_the_list_of_subcategories(self):
        self.is_element_present(*SecondLvlCatalogLocators.SHOW_ALL_CATEGORIES)
        btns = self.find_elements(*SecondLvlCatalogLocators.SHOW_ALL_CATEGORIES)
        if len(btns) == 0:
            self.open()
            self.go_to_second_lvl_catalog()
            self.expands_the_list_of_subcategories()
            return
        for b in btns:
            b.click()
            time.sleep(0.5)
        assert self.is_disappeared(*SecondLvlCatalogLocators.SUBCATEGORY_HIDDEN_LINK), \
            'Список подкатегорий не развернулся'
        self.scroll_to_block(*SecondLvlCatalogLocators.COLLAPSE_BUTTON, 10)
        assert self.is_element_present(*SecondLvlCatalogLocators.COLLAPSE_BUTTON), \
            'Кнопка свернуть не появилась'

    def the_list_is_collapsing(self):
        self.find_element(*SecondLvlCatalogLocators.COLLAPSE_BUTTON).click()
        time.sleep(0.5)
        assert self.is_element_present(*SecondLvlCatalogLocators.SUBCATEGORY_HIDDEN_LINK), \
            'Список категорий не свернулся'

    def should_be_listing_title(self):
        assert self.is_element_present(*ListingLocators.TITLE), \
            'На странице нет заголовка'

    def should_be_bread_crumb(self):
        assert self.is_element_present(*ListingLocators.BREAD_CRUMB), \
            'На странице нет хлебных крошек'

    def breadcrumbs_are_clickable(self):
        self.is_element_present(*ListingLocators.BREAD_CRUMB_ITEM)
        crumb = self.get_random_element(*ListingLocators.BREAD_CRUMB_ITEM)
        crumb_name = crumb.text
        crumb.click()
        time.sleep(1)
        self.is_element_present(*SecondLvlCatalogLocators.TITLE)
        page_title = self.get_element_text(*SecondLvlCatalogLocators.TITLE)
        assert crumb_name == page_title, \
            'Не релевантная страница перехода'

    def should_be_quantity(self):
        assert self.is_element_present(*ListingLocators.QUANTITY), \
            'Нет общего количества товаров'

    def should_be_back_btn(self):
        self.is_element_present(*ListingLocators.BACK_BTN)
        category = self.find_elements(*ListingLocators.BREAD_CRUMB_ITEM)[1]
        category_name = category.text
        back_btn = self.find_element(*ListingLocators.BACK_BTN)
        back_btn_name = back_btn.text
        assert category_name.lower() == back_btn_name.lower(), \
            f'Текст кнопки назад не верный, категория - {category_name} текст кнопки - {back_btn_name}'

    def back_button_clickable(self):
        self.is_element_present(*ListingLocators.BACK_BTN)
        back_btns = self.find_elements(*ListingLocators.BACK_BTN)
        btn_count = len(back_btns)
        if btn_count < 1:
            back_btn = back_btns[1]
        else:
            back_btn = back_btns[0]
        back_btn_name = back_btn.text
        back_btn.click()
        time.sleep(1)
        page_title = self.get_element_text(*SecondLvlCatalogLocators.TITLE)
        assert back_btn_name == page_title, \
            'Страница перехода не релевантна'

    def should_be_category_button(self):
        time.sleep(3)
        page_title = self.get_element_text(*ListingLocators.TITLE)
        assert self.is_element_present(*ListingLocators.CATEGORY_BTN), \
            'На странице нет кнопки выбранной категории'
        category_name = self.get_element_text(*ListingLocators.CATEGORY_BTN)
        assert page_title.lower() == category_name.lower(), \
            'Текст кнопка не соответствует выбранной категории'
        background = self.get_style_value(*ListingLocators.CATEGORY_BTN, 'background-color')
        assert background is not None, \
            'Фон кнопки не выделен'

    def category_button_is_clickable(self):
        url_before = self.browser.current_url
        self.allure_before_after('before')
        self.find_element(*ListingLocators.CATEGORY_BTN).click()
        self.allure_before_after('after')
        url_after = self.browser.current_url
        assert url_after == url_before, \
            'Кнопка категории ведет на другую страницу'

    def should_be_filters(self):
        assert self.is_element_present(*ListingLocators.FILTERS), \
            'На странице нет фильтров'

    def filters_not_included(self):
        assert self.is_disappeared(*ListingLocators.REMOVE_ALL), \
            'Есть включенные фильтры'

    def text_highlighted_bold(self):
        self.is_element_present(*ListingLocators.FILTER_TITLE)
        bold_value = self.get_style_value(*ListingLocators.FILTER_TITLE, 'font-weight')
        assert int(bold_value) == 700, 'Текст не bold'

    def filter_price_open(self):
        self.is_element_present(*ListingLocators.FILTER_BLOCK)
        price_filter = self.find_elements(*ListingLocators.FILTER_BLOCK)[0]
        assert price_filter.find_element(*ListingLocators.OPEN_FILTER) is not False, \
            'Фильтр цена не открыт'

    def you_can_close_the_filter(self):
        block_filter = self.find_elements(*ListingLocators.FILTERS)[0]
        price_filter = self.find_elements(*ListingLocators.FILTER_TITLE)[0]
        self.is_element_present(*ListingLocators.OPEN_FILTER)
        price_filter.click()
        time.sleep(0.3)
        self.allure_before_after('after')
        try:
            filter_after = block_filter.find_element(*ListingLocators.OPEN_FILTER)
        except NoSuchElementException:
            filter_after = False
        assert filter_after is False, \
            'Фильтр Цена не закрылся'

    def you_can_open_the_filter(self):
        block_filter = self.find_elements(*ListingLocators.FILTERS)[0]
        price_filter = self.find_elements(*ListingLocators.FILTER_TITLE)[0]
        self.allure_before_after('before')
        price_filter.click()
        time.sleep(1)
        self.is_element_present(*ListingLocators.OPEN_FILTER)
        try:
            filter_after = block_filter.find_element(*ListingLocators.OPEN_FILTER)
        except NoSuchElementException:
            filter_after = False
        assert filter_after is not False, \
            'Фильтр Цена не открылся'

    def should_be_toggle_switch_available(self):
        toggle_switches = self.find_elements(*ListingLocators.TOGGLE_SWITCHES)
        self.scroll_from_origin(toggle_switches[0])
        assert len(toggle_switches) == 2, \
            'Количество toggle switch не равно 2'
        assert toggle_switches[0].text.lower() == 'В наличии'.lower(), \
            'Название не соответствует шаблону "В наличии"'
        assert self.is_element_present(*ListingLocators.TOGGLE_SWITCHES), \
            'Нет toggle_switch в наличии'

    def should_be_toggle_switch_discounted(self):
        toggle_switches = self.find_elements(*ListingLocators.TOGGLE_SWITCHES)
        assert len(toggle_switches) == 2, \
            'Количество toggle switch не равно 2'
        assert toggle_switches[1].text.lower() == 'По акции'.lower(), \
            'Название не соответствует шаблону "По акции"'
        self.allure_find_element('Элемент найден')
        assert toggle_switches[1].is_displayed(), \
            'Нет toggle_switch в наличии'

    def all_filters_expand_and_collapse(self):
        filters_count = len(self.find_elements(*ListingLocators.FILTER_TITLE))
        filters = self.find_elements(*ListingLocators.FILTER_BLOCK)
        for f in filters:
            try:
                f.find_element(*ListingLocators.CLOSE_FILTERS)
                self.scroll_from_origin(f, y=10)
                f.find_element(*ListingLocators.FILTER_TITLE).click()
                time.sleep(1)
            except NoSuchElementException:
                self.scroll_from_origin(f, y=10)
                continue
        self.allure_before_after('after')
        open_filters_count = len(self.find_elements(*ListingLocators.OPEN_FILTER))
        assert filters_count - 3 == open_filters_count, \
            f'Не все фильтры открылись filters_count-{filters_count} open_filters_count-{open_filters_count}'

    def should_be_ranges(self):
        self.scroll_to_block(*ListingLocators.TITLE)
        # self.find_element(*ListingLocators.FILTER_TITLE).click()
        assert self.is_element_present(*ListingLocators.RANGES), \
            'В фильтрах нет диапазонов'

    def should_be_checkboxes(self):
        assert self.is_element_present(*ListingLocators.CHECKBOXES), \
            'В фильтрах нет чек-боксов'

    def should_be_input_min(self):
        time.sleep(0.3)
        assert self.is_element_present(*ListingLocators.INPUT_MIN), \
            'У диапазона нет поля "От"'

    def should_be_input_max(self):
        assert self.is_element_present(*ListingLocators.INPUT_MAX), \
            'У диапазона нет поля "До"'

    def the_field_must_have_a_min_value(self):
        min_price = self.get_element_attribute(*ListingLocators.FIELD_VALUES, 'data-min')
        self.allure_find_element('Скрин')
        assert min_price is not None, \
            'Нет значения в поле "От"'

    def the_field_must_have_a_max_value(self):
        min_price = self.get_element_attribute(*ListingLocators.FIELD_VALUES, 'data-max')
        self.allure_find_element('Скрин')
        assert min_price is not None, \
            'Нет значения в поле "До"'

    def field_from_clickable(self):
        self.find_element(*ListingLocators.INPUT_MIN).click()
        time.sleep(0.5)
        assert self.is_element_present(*ListingLocators.INPUT_MIN_FOCUS), \
            'Поле "От" кликабельно'

    def field_to_clickable(self):
        self.find_element(*ListingLocators.INPUT_MAX).click()
        time.sleep(0.5)
        assert self.is_element_present(*ListingLocators.INPUT_MAX_FOCUS), \
            'Поле "До" кликабельно'

    def data_can_be_entered_in_the_fields_min(self):
        min_field = self.find_element(*ListingLocators.INPUT_MIN)
        value_before = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuetext')
        self.allure_before_after('before')
        for i in range(len(value_before)):
            min_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        min_field.send_keys(f'{int(value_before)+10}')
        time.sleep(3)
        self.allure_before_after('after')
        value_after = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuetext')
        assert int(value_after) != int(value_before), \
            'Минимальная цена не изменилась'

    def data_can_be_entered_in_the_fields_max(self):
        self.browser.refresh()
        max_field = self.find_element(*ListingLocators.INPUT_MAX)
        value_before = float(self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax'))
        value_before = round(value_before)
        self.allure_before_after('before')
        for i in range(len(str(value_before))):
            max_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        max_field.send_keys(f'{int(value_before)-10}')
        time.sleep(3)
        self.allure_before_after('after')
        value_after = float(self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax'))
        value_after = round(value_after)
        assert int(value_after) != int(value_before), \
            'Максимальная цена не изменилась'

    def field_min_accept_only_integers(self, num):
        self.browser.refresh()
        min_field = self.find_element(*ListingLocators.INPUT_MIN)
        value_before = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuetext')
        self.allure_before_after('before')
        for i in range(len(value_before)):
            min_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        min_field.send_keys(num)
        time.sleep(3)
        value_after = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuetext')
        assert str(value_after) == str(num), \
            'Поле до, принимает другие типы данных'

    def field_max_accept_only_integers(self, num):
        self.browser.refresh()
        max_field = self.find_element(*ListingLocators.INPUT_MAX)
        value_before = float(self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax'))
        value_before = round(value_before)
        self.allure_before_after('before')
        for i in range(len(str(value_before))):
            max_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        max_field.send_keys(num)
        time.sleep(3)
        value_after = float(self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax'))
        value_after = round(value_after)
        assert str(value_after) == str(num), \
            'Поле до, принимает другие типы данных'

    def there_is_a_slider_with_dots(self):
        assert self.is_element_present(*ListingLocators.RANGES), \
            'Нет слайдера'
        assert self.is_element_present(*ListingLocators.MIN_DOT), \
            'Нет точки минимум'
        assert self.is_element_present(*ListingLocators.MAX_DOT), \
            'Нет точки максимум'

    def dot_min_shift_when_field_filled(self):
        min_field = self.find_element(*ListingLocators.INPUT_MIN)
        value_before = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuetext')
        position = self.find_elements(*ListingLocators.TRANSFORM_DOTS)[0]
        position_before = position.value_of_css_property('transform')
        self.allure_before_after('before')
        for i in range(len(value_before)):
            min_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        min_field.send_keys(f'{int(value_before)+10}')
        time.sleep(5)
        position_after = position.value_of_css_property('transform')
        self.allure_before_after('after')
        assert position_before != position_after, \
            'Точка не сместилась'

    def dot_max_shift_when_field_filled(self):
        max_field = self.find_element(*ListingLocators.INPUT_MAX)
        value_before = float(self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax'))
        position = self.find_elements(*ListingLocators.TRANSFORM_DOTS)[1]
        position_before = position.value_of_css_property('transform')
        value_before = round(value_before)
        self.allure_before_after('before')
        for i in range(len(str(value_before))):
            max_field.send_keys(Keys.BACKSPACE)
            time.sleep(0.2)
        time.sleep(1)
        max_field.send_keys(f'{int(value_before)-10}')
        time.sleep(5)
        position_after = position.value_of_css_property('transform')
        self.allure_before_after('after')
        assert position_before != position_after, \
            'Точка не сместилась'

    def should_be_teg_cloud(self):
        assert self.is_element_present(*ListingLocators.TEG_CLOUD), \
            'Облако тегов не появилось'

    def should_be_teg_remove_all(self):
        assert self.is_element_present(*ListingLocators.REMOVE_ALL_CLOUD), \
            'Не появилось облако "Очистить все"'

    def should_be_cloud_value(self):
        assert self.is_element_present(*ListingLocators.CLOUD_TEXT), \
            'В облаке нет значения фильтра'

    def should_be_close_cloud_button(self):
        assert self.is_element_present(*ListingLocators.CLOSE_CLOUD), \
            'В облаке нет кнопки удаления'

    def close_cloud(self):
        self.allure_before_after('before')
        value_before = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax')
        position = self.find_elements(*ListingLocators.TRANSFORM_DOTS)[1]
        position_before = position.value_of_css_property('transform')
        self.find_element(*ListingLocators.CLOSE_CLOUD).click()
        time.sleep(2)
        position = self.find_elements(*ListingLocators.TRANSFORM_DOTS)[1]
        position_after = position.value_of_css_property('transform')
        value_after = self.get_element_attribute(*ListingLocators.INPUT_NOW, 'aria-valuemax')
        assert not self.element_is_displayed(*ListingLocators.TEG_CLOUD), \
            'Облако фильтра не закрылось'
        assert position_before != position_after, \
            'Точка диапазона не сдвинулась'
        assert value_after != value_before, \
            'Значение в поле не изменилось'

    def checkboxes_work(self):
        try:
            remove_all_cloud = self.find_element(*ListingLocators.REMOVE_ALL_CLOUD)
            if remove_all_cloud == False:
                pass
            else:
                remove_all_cloud.click()
        except NoSuchElementException:
            pass
        self.wait_clickable(*ListingLocators.CHECKBOXES)
        box_name = self.get_element_text(*ListingLocators.CHECKBOXES)
        self.wait_visibility_of(*ListingLocators.CLOSE_CLOUD)
        cloud_name = self.get_element_text(*ListingLocators.CLOUD_TEXT).partition(': ')[2]
        assert self.is_element_present(*ListingLocators.TEG_CLOUD), \
            'Фильтр не применился, нет облака фильтра'
        assert cloud_name.lower() == box_name.lower(), \
            'Применился неверный фильтр'

    def there_is_a_tag_close_all(self):
        assert self.is_element_present(*ListingLocators.REMOVE_ALL_CLOUD), \
            'Не появилось облако "Закрыть все"'

    def close_cloud_checkbox(self):
        self.allure_before_after('before')
        self.find_element(*ListingLocators.CLOSE_CLOUD).click()
        time.sleep(2)
        assert not self.element_is_displayed(*ListingLocators.TEG_CLOUD), \
            'Облако фильтра не закрылось'

    def checkbox_can_be_disabled(self):
        self.wait_clickable(*ListingLocators.CHECKBOXES)
        assert not self.element_is_displayed(*ListingLocators.TEG_CLOUD), \
            'Облако фильтра не закрылось'

    def should_be_sorted(self):
        assert self.is_element_present(*ListingLocators.SORTING), \
            'На странице нет блока сортировки'

    def by_default_selected_by_popularity(self):
        default_value = self.get_element_attribute(*ListingLocators.SORTING_VALUE_SELECTED, 'data-value')
        assert default_value == 'popular', \
            'Сортировка по популярности, не выбрана'

    def sort_list_expands(self):
        self.scroll_to_block(*ListingLocators.SORTING)
        self.wait_clickable(*ListingLocators.SORTING)
        assert self.is_element_present(*ListingLocators.OPEN_SORTING), \
            'Список сортировок не раскрывается'

    def list_items_sorted(self):
        reference_list = ['Популярные', 'Сначала дешёвые', 'Сначала дорогие', 'По размеру скидки']
        items_list = []
        items = self.find_elements(*ListingLocators.SORTING_VALUE)
        self.allure_before_after('before')
        for i in items:
            item_value = i.text
            items_list.append(item_value)
        assert len(items) == 4, \
            'Количество сортировок неверное'
        assert reference_list == items_list, \
            'Не совпадают варианты сортировки'

    def list_is_collapsed_by_selecting_a_value(self):
        item = self.get_random_element(*ListingLocators.SORTING_VALUE, first=1)
        item_name = item.text
        item.click()
        time.sleep(0.5)
        assert self.is_disappeared(*ListingLocators.OPEN_SORTING), \
            'Список сортировок не свернулся'
        selected_value = self.get_element_text(*ListingLocators.SORTING)
        assert item_name == selected_value, \
            'Выбранная сортировка не включилась'

    def list_is_collapsing(self):
        self.wait_clickable(*ListingLocators.SORTING)
        self.is_element_present(*ListingLocators.OPEN_SORTING)
        self.scroll_to_block(*ListingLocators.TITLE, -100)
        self.wait_clickable(*ListingLocators.TITLE)
        assert self.is_disappeared(*ListingLocators.OPEN_SORTING)

    def sort_first_cheap(self):
        price_list = []
        self.scroll_to_block(*ListingLocators.SORTING)
        self.allure_before_after('before')
        self.wait_clickable(*ListingLocators.SORTING)
        time.sleep(0.5)
        self.is_element_present(*ListingLocators.OPEN_SORTING)
        sort_list = self.find_elements(*ListingLocators.SORTING_VALUE)
        for s in sort_list:
            if s.get_attribute('data-value') == 'cheap':
                cheap_sort = s
                break
            else:
                cheap_sort = None
        assert cheap_sort is not None, \
            'Сортировка по минимальной цене не найдена'
        cheap_sort.click()
        price = self.wait_visibility_of(*ListingLocators.PRICE_BLOCK)
        self.allure_before_after('after')
        prices = self.find_elements(*ListingLocators.PRICE)
        for p in prices:
            item_price = p.text.replace(' ', '')
            price_list.append(float(item_price.partition('₽')[0]))
        for idx in range(1, len(price_list)):
            assert price_list[idx - 1] <= price_list[idx], \
                f'Сортировка от дешевых не работает {price_list[idx - 1]} {price_list[idx]}'

    def sort_first_expensive(self):
        price_list = []
        self.scroll_to_block(*ListingLocators.SORTING, -10)
        self.allure_before_after('before')
        self.wait_clickable(*ListingLocators.SORTING)
        time.sleep(0.5)
        self.is_element_present(*ListingLocators.OPEN_SORTING)
        sort_list = self.find_elements(*ListingLocators.SORTING_VALUE)
        for s in sort_list:
            if s.get_attribute('data-value') == 'expensive':
                expensive_sort = s
                break
            else:
                expensive_sort = None
        assert expensive_sort is not None, \
            'Сортировка по максимальной цене не найдена'
        expensive_sort.click()
        price = self.wait_visibility_of(*ListingLocators.PRICE_BLOCK)
        self.allure_before_after('after')
        prices = self.find_elements(*ListingLocators.PRICE)
        for p in prices:
            item_price = p.text.replace(' ', '')
            price_list.append(float(item_price.partition('₽')[0]))
        for idx in range(1, len(price_list)):
            assert price_list[idx - 1] >= price_list[idx], \
                f'Сортировка от дорогих не работает {price_list[idx - 1]} {price_list[idx]}'

    def sort_first_discount_size(self):
        discount_list = []
        self.scroll_to_block(*ListingLocators.SORTING, -10)
        self.allure_before_after('before')
        self.wait_clickable(*ListingLocators.SORTING)
        time.sleep(0.5)
        self.is_element_present(*ListingLocators.OPEN_SORTING)
        sort_list = self.find_elements(*ListingLocators.SORTING_VALUE)
        for s in sort_list:
            if s.get_attribute('data-value') == 'discount_size':
                discount_size = s
                break
            else:
                discount_size = None
        assert discount_size is not None, \
            'Сортировка по размеру скидки не найдена'
        discount_size.click()
        self.wait_visibility_of(*ListingLocators.DISCOUNT)
        self.allure_before_after('after')
        discounts = self.find_elements(*ListingLocators.DISCOUNT)
        for d in discounts:
            item_discount = d.text.strip('-')
            discount_list.append(int(''.join(filter(str.isdigit, item_discount))))
        for idx in range(1, len(discount_list)):
            assert discount_list[idx - 1] >= discount_list[idx], \
                f'Сортировка размеру скидки не работает {discount_list[idx - 1]} {discount_list[idx]}'

    def should_be_items(self):
        self.scroll_to_block(*ListingLocators.ITEMS)
        assert self.is_element_present(*ListingLocators.ITEMS), \
            'В блоке нет плиток товаров'

    def should_be_photo_items(self):
        assert self.is_element_present(*ListingLocators.ITEMS_PHOTO), \
            'У слайда нет фотографии'

    def should_be_title_item(self):
        assert self.is_element_present(*ListingLocators.ITEMS_TITLE), \
            'На слайде нет названия товара'

    def should_be_price_item(self):
        assert self.is_element_present(*ListingLocators.PRICE_BLOCK), \
            'На слайде нет цены товара'

    def should_be_add_cart_btn(self):
        assert self.is_element_present(*ListingLocators.ADD_CART_BTN), \
            'На слайде нет цены товара'

    def should_be_add_favourites_btn(self):
        assert self.is_element_present(*ListingLocators.FAVOURITE_BTN), \
            'На слайде нет кнопки добавить в избранное'

    def should_be_reviews_rating_item(self):
        assert self.is_element_present(*ListingLocators.REVIEWS_RATING), \
            'У товаров нет оценки'

    def should_be_comments_block(self):
        assert self.is_element_present(*ListingLocators.COMMENTS_BLOCK), \
            'У товаров нет блока комментариев'

    def photos_in_tiles_switch_listing(self):
        self.scroll_to_block(*SecondLvlCatalogLocators.ITEMS, 150)
        slides = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*ListingLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        activ_photo = found_slide.find_element(*ListingLocators.ACTIV_PHOTO)
        first_photo_block = found_slide.find_elements(*ListingLocators.PHOTO_SWITCHING_BLOCKS)[0]
        self.move_to(first_photo_block)
        time.sleep(1)
        self.allure_before_after('before')
        url_before = activ_photo.get_attribute('src')
        second_photo_block = found_slide.find_elements(*ListingLocators.PHOTO_SWITCHING_BLOCKS)[-1]
        self.move_to(second_photo_block)
        time.sleep(1)
        activ_photo_after = found_slide.find_element(*ListingLocators.ACTIV_PHOTO)
        self.allure_before_after('after')
        url_after = activ_photo_after.get_attribute('src')
        assert url_before != url_after, \
            'Фотографии в плитках не переключаются'

    def number_dots_is_equal_to_number_of_photos_listing(self):
        slides = self.find_elements(*SecondLvlCatalogLocators.ITEMS)
        try:
            for slide in slides:
                photo_count = len(slide.find_elements(*ListingLocators.PHOTO_SWITCHING_BLOCKS))
                if photo_count > 1:
                    found_slide = slide
                    break
        except:
            pytest.skip('Нет плиток, где фотографий больше 1')
        dots_count = len(found_slide.find_elements(*SecondLvlCatalogLocators.PHOTO_DOTS))
        self.allure_find_element('Скрин')
        assert photo_count == dots_count, \
            'Количество точек не соответствует количеству фотографий'


class ProductPage(BasePage):
    def should_be_product_title(self):
        assert self.is_element_present(*ProductPageLocators.PRODUCT_TITLE), \
            'На странице товара нет заголовка'

    def should_be_product_rating(self):
        assert self.is_element_present(*ProductPageLocators.PRODUCT_RATING), \
            'На странице товара нет оценки'

    def should_be_reviews(self):
        assert self.is_element_present(*ProductPageLocators.REVIEWS), \
            'В карточке товара нет количества отзывов'

    def should_be_compare_button(self):
        assert self.is_element_present(*ProductPageLocators.COMPARE_BTN), \
            'На странице товара нет кнопки сравнения'




